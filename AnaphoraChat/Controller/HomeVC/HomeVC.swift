//
//  HomeVC.swift
//  AnaphoraChat
//
//  Created by subhajit halder on 20/01/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SocketIO

class HomeVC: UIViewController, UITableViewDataSource, UITableViewDelegate, GMSMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: VARS
    var locationManager = CLLocationManager()
    var lat = ""
    var lng = ""
    var url = URL(string: "\(kBaseUrl)")
    //var socket: SocketIOClient?
    //var socketIsConnected = Bool()
    var dataSource = NSArray()
    let delegate = UIApplication.shared.delegate as! AppDelegate
    //MARK: Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        
        UIApplication.shared.statusBarStyle = .lightContent
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor.black
        
    }
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
                let screen = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC
                screen?.isHomeSwiped = true
                screen?.userName = UserDefaults.standard.object(forKey: "userName") as! String
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromBottom
                view.window!.layer.add(transition, forKey: kCATransition)
                self.present(screen!, animated: false, completion: nil)
//                let ProfileVC = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//                self.navigationController?.pushViewController(ProfileVC, animated: true)
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    @IBAction func searchClicked(_ sender: Any) {
        let SearchVC = storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        self.navigationController?.pushViewController(SearchVC, animated: true)
    }
    @IBAction func notificationClicked(_ sender: Any) {
        let NotificationVC = storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(NotificationVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Initialize the location manager.
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        self.chatGroup_data()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        locationManager.requestWhenInUseAuthorization()
    }

    @IBAction func appIconClicked(_ sender: Any) {
        let chatTypeVC = storyboard?.instantiateViewController(withIdentifier: "ChatTypeVC") as! ChatTypeVC
        self.navigationController?.pushViewController(chatTypeVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func connectToSocket(){
        if !(delegate.socketIsConnected ?? false){
            let data_to_send = ["userName": "\(UserDefaults.standard.object(forKey: "userName") as! String)"]//["conversationGroupID": "\(self.chatModel.conversationGroupID)",
            
            delegate.socket = SocketIOClient(socketURL: (URL(string: "\(kBaseUrl)")! as NSURL) as URL, config: [.log(false), .compress, .connectParams(data_to_send)])
            
            delegate.socket!.on(clientEvent: .connect, callback: {(data, ack) in
                print("****** Socket Connected ******")
                
                self.delegate.socketIsConnected = true
            })
            delegate.socket?.on(clientEvent: .disconnect, callback: {(data, ack) in
                print("****** Socket Dis-Connected ******")
                self.delegate.socketIsConnected = false
            })
            delegate.socket?.disconnect()
            delegate.socket!.connect()
        }
    }
    
    //MARK: Table View Delegate & DataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count;
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeCell
        let model = self.dataSource[indexPath.row] as! ChatGroupModel
        print(model.type)
//        print(self.dataSource[indexPath.row])
        cell.refreshWithData(model: model){(object) in
            let model = object as! ChatGroupModel
//            print(model)
            let optionList = UIAlertController(title: "Options", message: nil, preferredStyle: .actionSheet)
            let delete = UIAlertAction(title: "Delete Chat Group", style: .default, handler: { action -> Void in
                self.deleteChatGroup(id: model.id)
                print("Delete")
            })
            optionList.addAction(delete)
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { action -> Void in
                print("Cancel")
            })
            optionList.addAction(cancel)
            self.present(optionList, animated: true, completion: nil)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath) as! HomeCell
        if cell.isSelected{
            let messageVc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            messageVc.chatModel = self.dataSource[indexPath.row] as! ChatGroupModel
            messageVc.isChatRoomClicked = false
            self.navigationController?.pushViewController(messageVc, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    //MARK:ACTIONS
    
    @IBAction func action_notifications(_ sender: Any) {
    }
    
    //MARK: API CALL
    func chatGroup_data() {
        self.startActivity()
        AppServiceManager.chatGroup(handler: {(object, message, error) in
            DispatchQueue.main.async(execute: { () -> Void in
                self.stopActivity()
                if error == nil {
                    if object != nil {
                        print(object)
                        self.dataSource = object as! NSArray
                        self.connectToSocket()
                        self.tableView.reloadData()
                    }else{
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    let string_as_error = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: string_as_error)
                }
            })
        })
    }
    
    func log_Data(){
        AppServiceManager.logUser(lat: self.lat, lng: self.lng) { (object, message, error) in
            print(object)
            print(message)
            print(error)
            if error == nil{
                if object != nil{
                }else{
                    self.showAlert(withTitle: "", andMessage: message)
                }
            }else{
                let string_as_error = error?.localizedDescription
                self.showAlert(withTitle: "", andMessage: string_as_error)
            }
        }
    }
    
    //Delete Chat Group
    func deleteChatGroup(id: String){
        self.startActivity()
        AppServiceManager.deleteChateGroup(id: id, handler: {(object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil{
                    if object != nil {
                        self.showAlert(withTitle: "", andMessage: message, handler: { (_,_) in
                            self.chatGroup_data()
                        })
                    }else{
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    let error_Api = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_Api)
                }
            })
        })
    }
   
    //MARK: Delegate
    
    //Location Delegate
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        //        currentCoordinates = location?.coordinate
        let lat = Double((location?.coordinate.latitude)!)
        let long = Double((location?.coordinate.longitude)!)
        locationManager.stopUpdatingLocation()
        retrivePlaceName(withCoordinates: (location?.coordinate)!)
        print("\(lat),\(long)")
        self.lat = "\(lat)"
        self.lng =  "\(long)"
        self.stopActivity()
        self.log_Data()
    }
    
    func retrivePlaceName(withCoordinates coordinate: CLLocationCoordinate2D) {
        GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: {(response, error) in
            var currentAddress = String()
            var addressObj = GMSAddress()
            if let tempaddressObj = response?.firstResult() {
                addressObj = tempaddressObj
            }
            let city = (addressObj.locality != nil) ? addressObj.locality : ((addressObj.subLocality != nil) ? addressObj.subLocality : addressObj.thoroughfare)
            let country = addressObj.country
            currentAddress = "\(city ?? "")" + "," + "\(country ?? "")"
            DispatchQueue.main.async(execute: { () -> Void in
                self.stopActivity()
            })
        })
    }

}
