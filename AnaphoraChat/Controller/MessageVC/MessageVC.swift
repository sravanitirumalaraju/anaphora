//
//  MessageVC.swift
//  AnaphoraChat
//
//  Created by subhajit halder on 20/01/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit
//import JSQMessagesViewController

class MessageVC: UIViewController {

    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var viewOnlineDisplay: UIView!
    
    
    
//    var messages = [JSQMessage]()
//    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
//    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.sender
//        self.senderId = "1000"
//        self.senderDisplayName = "Michel"
//        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
//        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
//        self.collectionView.backgroundColor = UIColor(colorLiteralRed: 0.925, green: 0.925, blue: 0.925, alpha: 1)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.view.bringSubview(toFront: viewNavBar)
//        self.view.bringSubview(toFront: imageViewUser)
//        self.view.bringSubview(toFront: viewOnlineDisplay)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        addMessage(withId: "foo", name: "Mr.Bolt", text: "I am so fast!")
//        // messages sent from local sender
//        addMessage(withId: senderId, name: "Me", text: "I bet I can run faster than you!")
//        addMessage(withId: senderId, name: "Me", text: "I like to run!")
//        // animates the receiving of a new message on the view
//        finishReceivingMessage()
    }
    
//    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
////        let itemRef = messageRef.childByAutoId() // 1
////        let messageItem = [ // 2
////            "senderId": senderId!,
////            "senderName": senderDisplayName!,
////            "text": text!,
////            ]
//        
////        itemRef.setValue(messageItem) // 3
//        addMessage(withId: senderId, name: "Me", text: text)
//        
//        JSQSystemSoundPlayer.jsq_playMessageSentSound() // 4
//        
//        finishSendingMessage() // 5
//    }
//
//    private func addMessage(withId id: String, name: String, text: String) {
//        if let message = JSQMessage(senderId: id, displayName: name, text: text) {
//            messages.append(message)
//        }
//    }
//    
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
//        return messages[indexPath.item]
//    }
//    
//    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return messages.count
//    }
//    
//    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
//        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
//        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.white)
//    }
//    
//    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
//        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
//        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.lightGray)
//    }
//
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
//        let message = messages[indexPath.item] // 1
//        if message.senderId == senderId { // 2
//            return outgoingBubbleImageView
//        } else { // 3
//            return incomingBubbleImageView
//        }
//    }
//    
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
//        return nil
//    }
//    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func action_ChatNow(_ sender: Any) {
        
        let chatTypeVC = storyboard?.instantiateViewController(withIdentifier: "ChatTypeVC") as! ChatTypeVC
        self.navigationController?.pushViewController(chatTypeVC, animated: true)
        
    }
    

}
