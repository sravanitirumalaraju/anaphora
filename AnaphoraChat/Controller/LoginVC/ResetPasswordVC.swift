//
//  ResetPasswordVC.swift
//  ACCO
//
//  Created by Tanmoy on 14/07/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController {
    
    
    //IBOUTLETS
    
    @IBOutlet weak var textFieldUsername: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    //Vars 
    
    var userName = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textFieldUsername.isEnabled = false

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        self.textFieldUsername?.text = userName
        
        let whitePlaceHolderColor = UIColor.white
        textFieldUsername.attributedPlaceholder = NSAttributedString(string: textFieldUsername.placeholder!, attributes: [NSForegroundColorAttributeName: whitePlaceHolderColor])
        textFieldPassword.attributedPlaceholder = NSAttributedString(string: textFieldPassword.placeholder!, attributes: [NSForegroundColorAttributeName: whitePlaceHolderColor])
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: DELEGATE
    
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
         let rect = textField.superview?.convert(textField.frame, to: self.view)
         scrollView.scrollContentForViewFrame(activeFrame: rect!, andParentView: self.view)
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //MARK: ACTIONS
    
    @IBAction func action_resetpass(_ sender: Any) {
        
        if textFieldUsername.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter username")
            
        }
        else if  textFieldPassword.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter password")
        }
        else {
            
            
            self.resetPassword()
    
        }
        
        
    }
    
    @IBAction func action_searchUname(_ sender: Any) {
        
        let searchUname = self.storyboard?.instantiateViewController(withIdentifier: "ResetSearchUserVC") as! ResetSearchUserVC
        
        self.present(searchUname, animated: false) { 
            
        }
        
    }
    
    @IBAction func action_back(_ sender: Any) {
        
       self.navigationController?.popViewController(animated: true)
        
    }
    
    
    //MARK: API Call
    
    func resetPassword(){
        
        self.startActivity()
        
        AppServiceManager.resetPassword(username: textFieldUsername.text?.trimString(), password: textFieldPassword.text?.trimString(), handler: {
            (object, message, error) in
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                
                if error == nil {
                    
                    if object != nil {
                        
                        self.showAlert(withTitle: "", andMessage: message!)
                        
                        let searchUname = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        
                        self.navigationController?.pushViewController(searchUname, animated: false)
                        
                    }
                    else {
                        
                        self.showAlert(withTitle: "", andMessage: message!)
                    }
                }
                else {
                    let errorStr = error.debugDescription
                    self.showAlert(withTitle: "", andMessage: errorStr)
                }
                
            })
            
        })
        
    }

}
