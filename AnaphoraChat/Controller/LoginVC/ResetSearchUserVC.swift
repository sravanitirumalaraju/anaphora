//
//  ResetSearchUserVC.swift
//  ACCO
//
//  Created by Tanmoy on 14/07/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class ResetSearchUserVC: UIViewController, UITextFieldDelegate {

    //MARK: OUTLETS
    @IBOutlet weak var text_searchUname: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let whitePlaceHolderColor = UIColor.white
        text_searchUname.attributedPlaceholder = NSAttributedString(string: text_searchUname.placeholder!, attributes: [NSForegroundColorAttributeName: whitePlaceHolderColor])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    
    
    
    //MARK: ACTIONS
    
    @IBAction func action_searchUserName(_ sender: Any) {
        
        if text_searchUname?.text?.trimString() == ""{
            
            self.showAlert(withTitle: "", andMessage: "Please enter a valid emailid")
        
        }else{
            
            self.search()
            
        }
        
        
    }
    
    @IBAction func action_back(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK: DELEGATE
    
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
       // let rect = textField.superview?.convert(textField.frame, to: self.view)
       // scrollView.scrollContentForViewFrame(activeFrame: rect!, andParentView: self.view)
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    
    
    //MARK: API CALL
    
    func search(){
        
        self.startActivity()
        
        AppServiceManager.searchUname(email:(text_searchUname.text?.trimString())!) { (object, message, error) in
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                
                if error == nil{
                    
                    if object != nil{
                        
                        let searchUname = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                        
                        searchUname.userName = object as! String
                        
                        self.navigationController?.pushViewController(searchUname, animated: false)
                        
                    }else{
                        
                        self.showAlert(withTitle: "", andMessage: message)
                        
                    }
                    
                }else{
                    
                    let error_as_string = error?.localizedDescription
                    
                    self.showAlert(withTitle: "", andMessage: error_as_string)
                    
                }
                
            })
            
            
        }
        
    }

    

}
