//
//  LoginVC.swift
//  AnaphoraChat
//
//  Created by subhajit halder on 20/01/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit

class LoginVC: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate, UINavigationControllerDelegate {

    
    //MARK: OUTLETS
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewSubScroll: UIView!
    
    @IBOutlet weak var textFieldUsername: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var switchRememberMe: UISwitch!
    
    
    //MARK: VERS
    
    var isInitVC:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.automaticallyAdjustsScrollViewInsets = false
        scrollView.contentInset = UIEdgeInsets.zero
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        tap.delegate = self
        viewSubScroll.addGestureRecognizer(tap)
        
       

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        self.navigationController?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        let whitePlaceHolderColor = UIColor.white
        textFieldUsername.attributedPlaceholder = NSAttributedString(string: textFieldUsername.placeholder!, attributes: [NSForegroundColorAttributeName: whitePlaceHolderColor])
        textFieldPassword.attributedPlaceholder = NSAttributedString(string: textFieldPassword.placeholder!, attributes: [NSForegroundColorAttributeName: whitePlaceHolderColor])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Action
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func actionForgotPass(_ sender: Any) {
      
        let resetPass = self.storyboard?.instantiateViewController(withIdentifier: "ResetSearchUserVC") as! ResetSearchUserVC
        
        self.navigationController?.pushViewController(resetPass, animated: false)
        
        
    }
    
    @IBAction func actionSignIn(_ sender: Any) {
        if textFieldUsername.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter username")
            
        }
        else if  textFieldPassword.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter password")
        }
        else {
            self.callLoginApi()
            
            
        }
    }
    
    @IBAction func actionSignUp(_ sender: Any) {
        let signUpVC = storyboard?.instantiateViewController(withIdentifier:"SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(signUpVC, animated: true)
        
    }
    
    //MARK: Delegates
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        let rect = textField.superview?.convert(textField.frame, to: self.view)
        scrollView.scrollContentForViewFrame(activeFrame: rect!, andParentView: self.view)
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // gesture
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view == viewSubScroll {
            return true
        }
        
        return false
        
    }

    @IBAction func actionBack(_ sender: Any) {
        
        if isInitVC{
        _ = self.navigationController?.popViewController(animated: true)
        }else{
            
            let initVC = self.storyboard?.instantiateViewController(withIdentifier: "InitVC") as! InitVC
            
            self.navigationController?.pushViewController(initVC, animated: true)
        }
    }
    
  
    
    func successfulLogin() {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeRootVC") as! HomeRootVC
        self.navigationController?.pushViewController(destVc, animated: true)
//        let appDel: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
//        let tabBarController = SHTabBarController.init()
//        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//        UIView.transition(from: self.view, to: homeVC.view, duration: 0.3, options: [.allowAnimatedContent, .transitionCrossDissolve, .curveEaseInOut, .layoutSubviews], completion: {(_ finished: Bool) -> Void in
//            appDel?.window?.rootViewController = tabBarController
//        })
    }
    
    // MARK: API
    
    //Login
    
    private func callLoginApi() {
        
        self.startActivity()
        
        AppServiceManager.loginWith(username: textFieldUsername.text?.trimString(), password: textFieldPassword.text?.trimString(), handler: {
            (object, message, error) in
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                
                if error == nil {
                    if object != nil {
                        
                        self.successfulLogin()
                    }
                    else {
                        self.showAlert(withTitle: "", andMessage: message!)
                    }
                }
                else {
                    let errorStr = error.debugDescription
                    self.showAlert(withTitle: "", andMessage: errorStr)
                }
                
            })
            
        })
    }



}
