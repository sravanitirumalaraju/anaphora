//
//  SignUpVC.swift
//  AnaphoraChat
//
//  Created by subhajit halder on 20/01/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import MapKit

class SignUpVC: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate, UINavigationControllerDelegate,UIImagePickerControllerDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var scrollViewSignup: UIScrollView!
    @IBOutlet weak var viewSubScroll: UIView!
    
    @IBOutlet weak var imageViewUser: UIImageView!
    
    @IBOutlet weak var textFieldDisplayName: UITextField!
    
    @IBOutlet weak var textFieldGender: SHPickerField!
    @IBOutlet weak var textFieldUsername: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldBirthday: SHPickerField!
    @IBOutlet weak var textFieldMobile: UITextField!
    @IBOutlet weak var buttonSetPhoto: UIButton!
    
    let imagePicker = UIImagePickerController()
    
    let model = RegisterModel()
    
    var locationManager: CLLocationManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        tap.delegate = self
        viewSubScroll.addGestureRecognizer(tap)
        imagePicker.delegate = self
        // Do any additional setup after loading the view.
        
        getCurrentLocation()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        imageViewUser.layoutIfNeeded()
        imageViewUser.layer.cornerRadius = imageViewUser.frame.height / 2
        
        
        let blackPlaceHolderColor = UIColor(red:0.13, green:0.13, blue:0.13, alpha:1.0)
       
        textFieldDisplayName.attributedPlaceholder = NSAttributedString(string: textFieldDisplayName.placeholder!, attributes: [NSForegroundColorAttributeName: blackPlaceHolderColor])
        textFieldGender.attributedPlaceholder = NSAttributedString(string: textFieldGender.placeholder!, attributes: [NSForegroundColorAttributeName: blackPlaceHolderColor])
        textFieldUsername.attributedPlaceholder = NSAttributedString(string: textFieldUsername.placeholder!, attributes: [NSForegroundColorAttributeName: blackPlaceHolderColor])
        textFieldEmail.attributedPlaceholder = NSAttributedString(string: textFieldEmail.placeholder!, attributes: [NSForegroundColorAttributeName: blackPlaceHolderColor])
        textFieldPassword.attributedPlaceholder = NSAttributedString(string: textFieldPassword.placeholder!, attributes: [NSForegroundColorAttributeName: blackPlaceHolderColor])
        textFieldBirthday.attributedPlaceholder = NSAttributedString(string: textFieldBirthday.placeholder!, attributes: [NSForegroundColorAttributeName: blackPlaceHolderColor])
        
        textFieldMobile.attributedPlaceholder = NSAttributedString(string: textFieldMobile.placeholder!, attributes: [NSForegroundColorAttributeName: blackPlaceHolderColor])
        
        
        textFieldGender.pickerType = .default
        textFieldGender.dataSource = ["Male", "Female", "X"]
        textFieldGender.actionCompleted { (text, action) in
            //
        }

        textFieldBirthday.pickerType = .date
        textFieldBirthday.dateFormat = "MM-dd-yyyy"
        textFieldBirthday.actionCompleted { (text, action) in
            //
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestAlwaysAuthorization()
        locationManager?.distanceFilter = 50
        locationManager?.startUpdatingLocation()
        locationManager?.delegate = self
    }

    func retrivePlaceName(withCoordinates coordinate: CLLocationCoordinate2D) {
        GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: {(response, error) in
            var addressObj = GMSAddress()
            if let tempaddressObj = response?.firstResult() {
                addressObj = tempaddressObj
            }
            print(addressObj)
            let city = (addressObj.locality != nil) ? addressObj.locality : ((addressObj.subLocality != nil) ? addressObj.subLocality : addressObj.thoroughfare)
            let country = addressObj.country
            DispatchQueue.main.async(execute: { () -> Void in
                print(city)
                print(country)
//                self.updateUserCityAndCountry(withCity: city, country: country)
//                self.model.location = "\(city!),\(country!)"      //sravani
            })
        })
    }

    
    func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }

    
    @IBAction func actionBack(_ sender: Any) {
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSignUp(_ sender: Any) {
//        self.showAlert(withTitle: "", andMessage: "Coming Soon")
        
        if textFieldDisplayName.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter Display Name")
        }
        else if textFieldUsername.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter username")
        }
        else if textFieldEmail.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter email")
        }
        else if textFieldPassword.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter password")
        }
        else if textFieldBirthday.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter birthday")
        }
        else if textFieldGender.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter gender")
        }
        else if textFieldMobile.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter phone no.")
        }
        else if imageViewUser.image == nil {
            
            self.showAlert(withTitle: "", andMessage: "Please select a profile pic")
        }
        else {
            self.callImageSaveApi()
        }
    }
    
    @IBAction func actionTerms(_ sender: Any) {
        self.showAlert(withTitle: "", andMessage: "Coming Soon")
    }
    
    @IBAction func actionSetPhoto(_ sender: Any) {
//        self.showAlert(withTitle: "", andMessage: "Coming Soon")
        let alertController = UIAlertController(title: title, message: "Options", preferredStyle: .actionSheet)
        
        let firstView: UIView? = alertController.view.subviews.first
        
        let secView: UIView? = firstView?.subviews.first
        
        secView?.backgroundColor = UIColor(red: CGFloat(0.996), green: CGFloat(0.996), blue: CGFloat(0.980), alpha: CGFloat(1.00))
        
        secView?.layer.cornerRadius = 3.0
        
        alertController.view.tintColor = kAlertTintColor
        
        let photoLib = UIAlertAction(title: "Photo Library", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
            DispatchQueue.main.async(execute: { 
                //
                self.photoLibrary()
            })
        })
        alertController.addAction(photoLib)
        
        let cam = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
            DispatchQueue.main.async(execute: {
                //
                self.camera()
            })
        })
        alertController.addAction(cam)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func photoLibrary() {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagePicker, animated: true, completion: nil)
    }
    
    func camera() {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        imagePicker.cameraCaptureMode = .photo
        present(imagePicker, animated: true, completion: nil)
    }
    
    func checkImage() {
        if imageViewUser.image == nil {
            buttonSetPhoto.setTitle("Set Photo", for: .normal)
        }
        else {
            buttonSetPhoto.setTitle("", for: .normal)
        }
        buttonSetPhoto.layoutIfNeeded()
    }
    
    //MARK: - Delegates
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
//        currentCoordinates = location?.coordinate
        let lat = Double((location?.coordinate.latitude)!)
        let long = Double((location?.coordinate.longitude)!)
        print(lat)
        print(long)
        locationManager?.stopUpdatingLocation()
        retrivePlaceName(withCoordinates: (location?.coordinate)!)
        model.latLong = "\(lat),\(long)"
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage //2
        imageViewUser.contentMode = .scaleAspectFit //3
        imageViewUser.image = chosenImage //4
        dismiss(animated:true, completion: nil) //5
        self.checkImage()
    }
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        self.checkImage()
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        let rect = textField.superview?.convert(textField.frame, to: self.view)        
        scrollViewSignup.scrollContentForViewFrame(activeFrame: rect!, andParentView: self.view)
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        print(textFieldDisplayName.text!)
        textField.resignFirstResponder()
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.textFieldMobile {
            let currentText = textField.text ?? ""
            let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
            
            return prospectiveText.characters.count <= 10
        }
        return true
    }

    
    // gesture
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view == viewSubScroll {
            return true
        }
        
        if gestureRecognizer == self.navigationController?.interactivePopGestureRecognizer {
            return true
        }
        return false
        
    }
    
    //MARK: Api Calls
    
    private func callImageSaveApi() {
        self.startActivity()
        AppServiceManager.saveImage(withImage: (imageViewUser.image)!, type: "p", handler: {
            (object, message, error) in
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                
                if error == nil {
                    if object != nil {
                        self.model.profilePic = object as! String
                        self.callRegisterApi()
                    }
                    else {
                        self.showAlert(withTitle: "", andMessage: message!)
                    }
                }
                else {
                    let errorStr = error.debugDescription
                    self.showAlert(withTitle: "", andMessage: errorStr)
                }
            })
        })
    }
    
    private func callRegisterApi() {
        self.startActivity()

        model.name = (textFieldDisplayName?.text?.trimString())!
        model.userName = (textFieldUsername.text?.trimString())!
        model.email = (textFieldEmail.text?.trimString())!
        model.dob = textFieldBirthday.text!
        model.gender = textFieldGender.text!
        model.phNo = (textFieldMobile.text?.trimString())!
        model.password = (textFieldPassword?.text!)!
        
        AppServiceManager.registerUser(model: model, handler: {
            (object, message, error) in
            print(object)
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                
                if error == nil {
                    if object != nil {
                        self.callLoginApi()
                    }
                    else {
                        self.showAlert(withTitle: "", andMessage: message!)
                    }
                }
                else {
                    let errorStr = error.debugDescription
                    self.showAlert(withTitle: "", andMessage: errorStr)
                }
            })
        })
    }
    
    private func callLoginApi() {
        self.startActivity()
        AppServiceManager.loginWith(username: textFieldUsername.text?.trimString(), password: textFieldPassword.text?.trimString(), handler: {
            (object, message, error) in
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                
                if error == nil {
                    if object != nil {
                        self.successfulLogin()
                    }
                    else {
                        self.showAlert(withTitle: "", andMessage: message!)
                    }
                }
                else {
                    let errorStr = error.debugDescription
                    self.showAlert(withTitle: "", andMessage: errorStr)
                }
            })
        })
    }

    
    func successfulLogin() {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeRootVC") as! HomeRootVC
        self.navigationController?.pushViewController(destVc, animated: true)

//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let initialViewController = storyboard.instantiateViewController(withIdentifier: "HomeRootVC") as! HomeRootVC
//        self.navigationController.pushViewController(initialViewController, animated: true)
//        self.window?.rootViewController = navigationController
//        let appDel: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
//        let tabBarController = SHTabBarController.init()
//        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//        UIView.transition(from: self.view, to: homeVC.view, duration: 0.3, options: [.allowAnimatedContent, .transitionCrossDissolve, .curveEaseInOut, .layoutSubviews], completion: {(_ finished: Bool) -> Void in
//            appDel?.window?.rootViewController = tabBarController
//        })
    }

}
