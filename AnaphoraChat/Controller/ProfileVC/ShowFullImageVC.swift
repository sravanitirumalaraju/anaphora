//
//  ShowFullImageVC.swift
//  ACCO
//
//  Created by Omnibuz Labs on 07/11/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit
import SDWebImage

class ShowFullImageVC: UIViewController {

    @IBOutlet weak var view_backOfImage: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    
    var url = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imageView?.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeholder.png"))
//MARK: Blur Effect
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        
        blurEffectView.frame = self.view_backOfImage.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view_backOfImage.addSubview(blurEffectView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func action_cancelBtn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
