//
//  EditProfileVC.swift
//  AnaphoraChat
//
//  Created by subhajit halder on 22/02/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import MapKit

class EditProfileVC: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate, UINavigationControllerDelegate,UIImagePickerControllerDelegate, CLLocationManagerDelegate {

    
    @IBOutlet weak var scrollViewSignup: UIScrollView!
    @IBOutlet weak var viewSubScroll: UIView!
    
    
    @IBOutlet weak var img_profileImage: UIImageView!
    
    @IBOutlet weak var textFieldDisplayName: UITextField!
    
    @IBOutlet weak var textFieldGender: SHPickerField!
    @IBOutlet weak var textFieldUsername: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldBirthday: SHPickerField!
    @IBOutlet weak var textFieldMobile: UITextField!
    @IBOutlet weak var buttonSetPhoto: UIButton!
    @IBOutlet weak var text_age: UITextField!
    @IBOutlet weak var text_hobbies: UITextField!
    @IBOutlet weak var text_bio: UITextField!
    @IBOutlet weak var text_homeTown: UITextField!
    
    let imagePicker = UIImagePickerController()
    
    var model = UserModel()
    
    var registerModel = RegisterModel()
    
    var locationManager: CLLocationManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = "\(kBaseUrl)\(model.profilePic)"
        img_profileImage.sd_setImage(with: URL(string: url))

        self.navigationController?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        tap.delegate = self
        viewSubScroll.addGestureRecognizer(tap)
        imagePicker.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textFieldUsername?.text = self.model.userName
        
        self.textFieldDisplayName.text = self.model.name
        
        self.textFieldEmail.text = self.model.email
        
        self.textFieldGender.text = self.model.gender
        
        //self.textFieldMobile.text = self.model.phNo
        
        self.textFieldBirthday?.text = self.model.dob
        
        self.text_age?.text = self.model.age
        
        self.text_hobbies?.text = self.model.hobbies
        
        self.text_bio?.text = self.model.bio
        self.text_homeTown.text = self.model.location
        
        
        img_profileImage.layoutIfNeeded()
        img_profileImage.layer.cornerRadius = img_profileImage.frame.height / 2
        img_profileImage.layer.masksToBounds = true
        
        textFieldGender.pickerType = .default
        textFieldGender.dataSource = ["Male", "Female", "X"]
        textFieldGender.actionCompleted { (text, action) in
        }
        textFieldBirthday.pickerType = .date
        textFieldBirthday.dateFormat = "MM-dd-yyyy"
        textFieldBirthday.actionCompleted { (text, action) in
            //
        }
        getCurrentLocation()
    }

    
    func getCurrentLocation() {
        // Initialize the location manager.
        locationManager = CLLocationManager()
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestAlwaysAuthorization()
        locationManager?.distanceFilter = 50
        locationManager?.startUpdatingLocation()
        locationManager?.delegate = self
    }
    
    func retrivePlaceName(withCoordinates coordinate: CLLocationCoordinate2D) {
//        print(coordinate)
//        let geocoder = GMSGeocoder()
//        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
//            if let address = response?.firstResult() {
//                let latitude = address.coordinate.latitude
//                let longitude = address.coordinate.longitude
//                let latitudeString = String(format: "%f", latitude)
//                let longitudeString = String(format:"%f", longitude)
//                print(latitudeString)
//                var address1 = address.thoroughfare ?? String()
//                let address2 = address.subLocality ?? String()
//                let city = address.locality ?? String()
//                let state = address.administrativeArea ?? String()
//                let country = address.country ?? String()
//                let zipcode = address.postalCode ?? String()
//
//                print(city)
//                print(country)
//            }
//        }
//
        GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: {(response, error) in
            var addressObj = GMSAddress()
            if let tempaddressObj = response?.firstResult() {
                print(addressObj)
                addressObj = tempaddressObj
            }
            print(addressObj)
            let city = (addressObj.locality != nil) ? addressObj.locality : ((addressObj.subLocality != nil) ? addressObj.subLocality : addressObj.thoroughfare)
            let country = addressObj.country
            DispatchQueue.main.async(execute: { () -> Void in
                //                self.updateUserCityAndCountry(withCity: city, country: country)
                self.registerModel.location = "\(city ?? ""),\(country ?? "")"
                print(self.registerModel.location)
            })

        })
    }

    
    func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionSignUp(_ sender: Any) {
        //        self.showAlert(withTitle: "", andMessage: "Coming Soon")
        
        if textFieldDisplayName.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter Display Name")
        }
        
        else if textFieldEmail.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter email")
        }
        
        else if textFieldBirthday.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter birthday")
        }
        else if textFieldGender.text?.trimString() == "" {
            self.showAlert(withTitle: "", andMessage: "Enter gender")
        }
        else if img_profileImage.image == nil {
            self.callRegisterApi()
        }
        else {
            self.callImageSaveApi()
        }
    }
    
    @IBAction func actionTerms(_ sender: Any) {
        self.showAlert(withTitle: "", andMessage: "Coming Soon")
    }
    
    @IBAction func actionSetPhoto(_ sender: Any) {
        //        self.showAlert(withTitle: "", andMessage: "Coming Soon")
        let alertController = UIAlertController(title: title, message: "Options", preferredStyle: .actionSheet)
        let firstView: UIView? = alertController.view.subviews.first
        let secView: UIView? = firstView?.subviews.first
        secView?.backgroundColor = UIColor(red: CGFloat(0.996), green: CGFloat(0.996), blue: CGFloat(0.980), alpha: CGFloat(1.00))
        secView?.layer.cornerRadius = 3.0
        alertController.view.tintColor = kAlertTintColor
        let photoLib = UIAlertAction(title: "Photo Library", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
            DispatchQueue.main.async(execute: {
                //
                self.photoLibrary()
            })
        })
        alertController.addAction(photoLib)
        
        let cam = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
            DispatchQueue.main.async(execute: {
                //
                self.camera()
            })
        })
        alertController.addAction(cam)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    func photoLibrary() {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagePicker, animated: true, completion: nil)
    }
    
    func camera() {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        imagePicker.cameraCaptureMode = .photo
        present(imagePicker, animated: true, completion: nil)
    }
    
    func checkImage() {
        if img_profileImage.image == nil {
            buttonSetPhoto.setTitle("Set Photo", for: .normal)
        }
        else {
            buttonSetPhoto.setTitle("", for: .normal)
        }
        buttonSetPhoto.layoutIfNeeded()
    }
    
    //MARK: - Delegates
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        //        currentCoordinates = location?.coordinate
        let lat = Double((location?.coordinate.latitude)!)
        let long = Double((location?.coordinate.longitude)!)
        
        locationManager?.stopUpdatingLocation()
        retrivePlaceName(withCoordinates: (location?.coordinate)!)
        registerModel.latLong = "\(lat),\(long)"
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerEditedImage] as? UIImage {//2
            
            self.img_profileImage.contentMode = .scaleAspectFit
            self.img_profileImage.image = chosenImage
            
        }
        dismiss(animated:true, completion: nil) //5
        self.checkImage()
    }
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
        self.checkImage()
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        let rect = textField.superview?.convert(textField.frame, to: self.view)
        scrollViewSignup.scrollContentForViewFrame(activeFrame: rect!, andParentView: self.view)
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    
    // gesture
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view == viewSubScroll {
            return true
        }
        
        if gestureRecognizer == self.navigationController?.interactivePopGestureRecognizer {
            return true
        }
        return false
        
    }
    
    //MARK: Api Calls
    
    private func callImageSaveApi() {
        self.startActivity()
        AppServiceManager.saveImage(withImage: (self.img_profileImage.image)!, type: "p", handler: {
            (object, message, error) in
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                
                if error == nil {
                    if object != nil {
                        self.model.profilePic = object as! String
                        self.callRegisterApi()
                    }
                    else {
                        self.showAlert(withTitle: "", andMessage: message!)
                    }
                }
                else {
                    self.showAlert(withTitle: "", andMessage: message!)
                }
                
            })
            
            
        })
    }
    
    private func callRegisterApi() {
        self.startActivity()
      
        registerModel.name = (textFieldDisplayName.text?.trimString())!
        registerModel.userName = model.userName
        registerModel.email = (textFieldEmail.text?.trimString())!
        registerModel.dob = textFieldBirthday.text!
        registerModel.gender = textFieldGender.text!
        registerModel.profilePic = model.profilePic
        registerModel.phNo = model.phNo
        registerModel.location = text_homeTown.text!
        
        if self.model.private_mode{
            registerModel.private_mode = "true"
        }else{
            registerModel.private_mode = "false"
        }
        
        if text_hobbies?.text == nil{
            
            registerModel.hobbies = ""
            
        }else{
            
            registerModel.hobbies =  text_hobbies.text!
          
        }
        
        if text_bio?.text == nil{
            registerModel.bio = ""
        }else{
            registerModel.bio = text_bio.text!
        }
        registerModel.timeZone = TimeZone.current.abbreviation()!
        
        AppServiceManager.updateUser(model: registerModel, handler: {
            (object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                
                if error == nil {
                    if object != nil {
                        
                        self.showAlert(withTitle: "", andMessage: message!)
                        
                    }
                    else {
                        self.showAlert(withTitle: "", andMessage: message!)
                    }
                }
                else {
                    
                    let errorAsString = error?.localizedDescription
                    
                    self.showAlert(withTitle: "", andMessage: errorAsString!)
                }
                
            })
            
        })
    }
}
