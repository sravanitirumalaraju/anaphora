//
//  ProfileVC.swift
//  AnaphoraChat
//
//  Created by subhajit halder on 22/01/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMaps


class ProfileVC: UIViewController, UIScrollViewDelegate, UICollectionViewDataSource, UITableViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UICollectionViewDelegateFlowLayout,CLLocationManagerDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var view_navigation: UINavigationItem!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageViewButton1: UIImageView!
    @IBOutlet weak var imageViewButton2: UIImageView!
    @IBOutlet weak var imageViewButton3: UIImageView!
    @IBOutlet weak var imageViewProfilePic: UIImageView!
    @IBOutlet weak var text_bio_feed: UITextField?
    @IBOutlet weak var btn_settingsAndBack: UIButton!
    @IBOutlet weak var btn_chat: UIButton!
    @IBOutlet weak var buttonFollow: UIButton!
    @IBOutlet weak var buttonRecommend: UIButton!
    @IBOutlet weak var scrollViewMain: UIScrollView!
    //Page 1
    @IBOutlet weak var collectionViewGrid: UICollectionView!
    //Page 2
    @IBOutlet weak var tableViewSecondPage: UITableView!
    //Page 3
    @IBOutlet weak var labelAbout1: UILabel!
    @IBOutlet weak var labelAbout2: UILabel!
    @IBOutlet weak var labelAbout3: UILabel!
    @IBOutlet weak var labelAbout4: UILabel!
    @IBOutlet weak var labelAbout5: UILabel!
    @IBOutlet weak var labelAbout6: UILabel!
    
    @IBOutlet weak var view_devider1: UIView!
    @IBOutlet weak var view_devider2: UIView!
    @IBOutlet weak var view_devider3: UIView!
    @IBOutlet weak var view_devider4: UIView!
    @IBOutlet weak var view_devider5: UIView!
    @IBOutlet weak var followCount: UIButton!
    
    @IBOutlet weak var homeTownLabel: UILabel!
    @IBOutlet weak var aboutMeLbl: UILabel!
    @IBOutlet weak var hobbiesLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var ageLbl: UILabel!
    // MARK: Vars
    var arrayImages: NSMutableArray?
    var dataSource = NSMutableArray()
    var locationManager: CLLocationManager?
    var userName : String = ""
    var followStatus = Bool()
    var follower = 10
    var recommendStatus = Bool()
    var recommend = 0
    //Models
    var userDetailsModel = UserModel()
    var feedDataModel = UserFeedModel()
    var registerModel = RegisterModel()

    //sravani
    var isHomeSwiped = Bool()
    var isExploreSwiped = Bool()
    var window: UIWindow?
//    var navigationController = UINavigationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageViewButton1.image = UIImage(named: "Grid")
        self.imageViewButton2.image = UIImage(named: "Photos")
        self.imageViewButton3.image = UIImage(named: "about")
        
        self.btn_chat.isHidden = true
//        self.followCount.isHidden = true
        
        UIApplication.shared.statusBarStyle = .lightContent
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor.black
        
        if isHomeSwiped == true || isExploreSwiped == true{
            let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
            swipeUp.direction = UISwipeGestureRecognizerDirection.up
            self.view.addGestureRecognizer(swipeUp)
        }
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
                if isHomeSwiped == true{
                    let transition = CATransition()
                    transition.duration = 0.5
                    transition.type = kCATransitionPush
                    transition.subtype = kCATransitionFromTop
                    view.window!.layer.add(transition, forKey: kCATransition)
                    self.dismiss(animated: false)
                }else if isExploreSwiped == true{
                    if (self.userName == UserDefaults.standard.object(forKey: "userName") as! String){
                       _ = navigationController?.popViewController(animated: true)
                    }
                }
            default:
                break
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
        // Round profile pic
        imageViewProfilePic.layer.cornerRadius = imageViewProfilePic.frame.height / 2
        imageViewProfilePic.layoutIfNeeded()
        buttonRecommend.layer.borderColor = UIColor.black.cgColor
        buttonRecommend.layer.borderWidth = 2
        tableViewSecondPage.estimatedRowHeight = 430
        tableViewSecondPage.rowHeight = UITableViewAutomaticDimension
        callgetApi(userName: self.userName)
        getCurrentLocation()
        print(self.userName)
        print(UserDefaults.standard.object(forKey: "userName") as! String)
        if (self.userName != UserDefaults.standard.object(forKey: "userName") as! String){
            btn_settingsAndBack.setImage(UIImage(named: "back"), for: .normal)
//            btn_settingsAndBack.setImage(UIImage(named: "ic_action_chevron_left"), for: .normal)
            self.text_bio_feed?.isUserInteractionEnabled = false
            self.btn_chat.isHidden = false
//            self.followCount.isHidden = false
            self.buttonRecommend.isEnabled = true
            self.buttonFollow.isEnabled = true
            self.getFollowing()
            self.getFollowers()
        }else{
            self.buttonRecommend.isEnabled = false
            self.buttonFollow.isEnabled = false
            followStatus = false
            recommendStatus = false
            self.getFollowers()
            self.getFollowing()
            self.buttonFollow.setImage(UIImage(named: ""), for: .normal)
        }
    }
    func getCurrentLocation() {
        // Initialize the location manager.
        locationManager = CLLocationManager()
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestAlwaysAuthorization()
        locationManager?.distanceFilter = 50
        locationManager?.startUpdatingLocation()
        locationManager?.delegate = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionButton1(_ sender: Any) {
        let point = CGPoint(x:0, y:0)
        self.scrollViewMain.setContentOffset(point, animated: true)
    }
    @IBAction func actionButton2(_ sender: Any) {
        let point = CGPoint(x:self.view.frame.width, y:0)
        self.scrollViewMain.setContentOffset(point, animated: true)
    }
    @IBAction func actionButton3(_ sender: Any) {
        let point = CGPoint(x:self.view.frame.width * 2, y:0)
        self.scrollViewMain.setContentOffset(point, animated: true)
    }
   
    //MARK: TableView Delegate
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = self.dataSource[indexPath.row] as! UserFeedModel
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableviewCell") as! ProfileTableviewCell
        cell.refresh(model: model){(object,type) in
            if type == "comment"{ // comment action
                let userFeedDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "UserFeedDetailsVC") as! UserFeedDetailsVC
                userFeedDetailsVC.userFeedModel = model
                self.navigationController?.pushViewController(userFeedDetailsVC, animated: true)
            }else if type == "like"{ // like action
                let result = self.call_like_unlike(postid: model.postID)
                if result{
                    if cell.image_love?.image == UIImage(named:"heart_select"){
                        cell.image_love?.image = UIImage(named:"heart_darkgrey")
                    }else{
                        cell.image_love?.image = UIImage(named:"heart_select")
                    }
                }else{
                    if cell.image_love?.image == UIImage(named:"heart_select"){
                        cell.image_love?.image = UIImage(named:"heart_darkgrey")
                    }else{
                        cell.image_love?.image = UIImage(named:"heart_select")
                    }
                }
            }else if type == "option"{
                let model = object as! UserFeedModel
                let  optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                let deletePost = UIAlertAction(title: "DELETE POST", style: .default, handler: {action -> Void in
                    self.delete(postID: model.postID, index: indexPath.row)
                    print("Click on Delete")
                })
                optionMenu.addAction(deletePost)
                let cancel = UIAlertAction(title: "CANCEL", style: .default, handler: { action -> Void in
                    print("cancel event trigar")
                })
                optionMenu.addAction(cancel)
                self.present(optionMenu, animated: true, completion: nil)
            }
        }
        return cell
    }
    
    //MARK: CollectionView Delegate
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return dataSource.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let model = self.dataSource[indexPath.item] as! UserFeedModel
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCollectionCell", for: indexPath) as! ProfileCollectionCell
        cell.refresh(model: model)
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let size = ((self.collectionViewGrid.frame.size.width) / 3) - 2
        let size = ((self.collectionViewGrid.frame.size.width) / 3)
        return CGSize(width: size, height: collectionViewGrid.frame.size.height/2)
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        if (cell?.isSelected)!{
            let fullImageVC = self.storyboard?.instantiateViewController(withIdentifier: "ShowFullImageVC") as! ShowFullImageVC
            let model = self.dataSource[indexPath.item] as! UserFeedModel
            fullImageVC.url = "\(kBaseUrl)\(model.imageUrl)"
            fullImageVC.modalPresentationStyle = .overCurrentContext
            fullImageVC.modalTransitionStyle = .crossDissolve
            self.present(fullImageVC, animated: true, completion: nil)
        }
    }
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageSize = self.view.frame.size.width;
        let pageNo = scrollViewMain.contentOffset.x / pageSize;
        if (pageNo < 0.50) {
            self.setPage1()
        }
        else if (pageNo < 1.5) {
            self.setPage2()
        }
        else if (pageNo < 2.5) {
            self.setPage3()
        }
        self.view.endEditing(true)
    }
    
    //MARK: Design Helpers
    func setPage1() {
//        UIView.animate(withDuration: 0.3) {
            self.imageViewButton1.image = UIImage(named: "GridBeige")
            self.imageViewButton2.image = UIImage(named: "Photos")
            self.imageViewButton3.image = UIImage(named: "about")
//            self.view.layoutIfNeeded()
//        }
    }
    
    func setPage2() {
//        UIView.animate(withDuration: 0.3) {
            self.imageViewButton1.image = UIImage(named: "Grid")
            self.imageViewButton2.image = UIImage(named: "PhotoBeige")
            self.imageViewButton3.image = UIImage(named: "about")
//            self.view.layoutIfNeeded()
//        }
    }
    
    func setPage3() {
//        UIView.animate(withDuration: 0.3) {
            self.imageViewButton1.image = UIImage(named: "Grid")
            self.imageViewButton2.image = UIImage(named: "Photos")
            self.imageViewButton3.image = UIImage(named: "aboutBeige")
//            self.view.layoutIfNeeded()
//        }
    }
    
    
    @IBAction func action_editProfile(_ sender: Any) {
        if (self.userName == "\(UserDefaults.standard.object(forKey: "userName") as! String)"){
            let vbVC = storyboard?.instantiateViewController(withIdentifier:
                "EditProfileVC") as! EditProfileVC
            self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vbVC.model = self.userDetailsModel
            present(vbVC, animated: true, completion: {})
        }
    }
    func refreshData() {
        labelName.text = userDetailsModel.name
        view_navigation.title = "@" + "\(userDetailsModel.userName)"

        if userDetailsModel.bio.characters.count > 0{
            self.text_bio_feed?.text = self.userDetailsModel.bio
        }else{
            self.text_bio_feed?.placeholder = "Write your status here🖋"
        }
        
        self.view.layoutIfNeeded()
        let url = "\(kBaseUrl)\(userDetailsModel.profilePic)"
        imageViewProfilePic.sd_setImage(with: URL(string: url))
        print(userDetailsModel.location)
        if(self.userName == UserDefaults.standard.object(forKey: "userName")as! String){
            if userDetailsModel.location == ","{
                homeTownLabel.text = ""
            }else{
               homeTownLabel.text = "\(userDetailsModel.location)"
            }
            
            aboutMeLbl.text = "\(userDetailsModel.bio)"
            hobbiesLbl.text = "\(userDetailsModel.hobbies)"
            genderLbl.text = "\(userDetailsModel.gender)"
            ageLbl.text = "\(userDetailsModel.age)"
//            labelAbout6.text = ""
  //          labelAbout1.text = "Age:  \(userDetailsModel.age)"
 //           labelAbout2.text = "Dob: \(userDetailsModel.dob)"
  //          labelAbout3.text = "Gender: \(userDetailsModel.gender)"
 //           labelAbout4.text = "Location: \(userDetailsModel.location)"
 //           labelAbout5.text = "Hobbies: \(userDetailsModel.hobbies)"
 //           labelAbout6.text = "About Me: \(userDetailsModel.bio)"
        }else{
            if userDetailsModel.location == ","{
                homeTownLabel.text = ""
            }else{
                homeTownLabel.text = "\(userDetailsModel.location)"
            }
//            homeTownLabel.text = "\(userDetailsModel.location)"
            aboutMeLbl.text = "\(userDetailsModel.bio)"
            hobbiesLbl.text = "\(userDetailsModel.hobbies)"
            genderLbl.text = "\(userDetailsModel.gender)"
            ageLbl.text = "\(userDetailsModel.age)"
//            labelAbout1.text = "Age:  \(userDetailsModel.age)"
//            labelAbout2.text = "Gender: \(userDetailsModel.gender)"
//            labelAbout3.text = "Location: \(userDetailsModel.location)"
//            labelAbout4.text = "Hobbies: \(userDetailsModel.hobbies)"
//            labelAbout5.text = "About Me: \(userDetailsModel.bio)"
//            view_devider5.isHidden = true
//            labelAbout6.text = ""
        }
        
        let currentTime = getCurrentMillis()
        self.call_own_postedData(epochTime: currentTime)
    }
    
    //MARK: Delegate
    //MARK: Textfield Delegate
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        let rect = textField.superview?.convert(textField.frame, to: self.view)
        scrollViewMain.scrollContentForViewFrame(activeFrame: rect!, andParentView: self.view)
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.update_user()
        return true
    }
    
    //MARK: Epoch Time Call
    func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        //        currentCoordinates = location?.coordinate
        let lat = Double((location?.coordinate.latitude)!)
        let long = Double((location?.coordinate.longitude)!)
        
        locationManager?.stopUpdatingLocation()
        retrivePlaceName(withCoordinates: (location?.coordinate)!)
        registerModel.latLong = "\(lat),\(long)"
    }
    
    func retrivePlaceName(withCoordinates coordinate: CLLocationCoordinate2D) {
        GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: {(response, error) in
            var addressObj = GMSAddress()
            if let tempaddressObj = response?.firstResult() {
                addressObj = tempaddressObj
            }
            let city = (addressObj.locality != nil) ? addressObj.locality : ((addressObj.subLocality != nil) ? addressObj.subLocality : addressObj.thoroughfare)
            let country = addressObj.country
            DispatchQueue.main.async(execute: { () -> Void in
                //
                //                self.updateUserCityAndCountry(withCity: city, country: country)
                if city == nil || country == nil{
                    self.registerModel.location = ""
                }else{
                   self.registerModel.location = "\(city ?? ""),\(country ?? "")"
                }
            })
        })
    }
    
    //MARK: Action
    @IBAction func action_recommendBtn(_ sender: Any) {
        self.recommendUnrecommend()
    }
    
    @IBAction func action_followBtn(_ sender: Any) {
        self.follow_unfollow(username: self.userName)
    }
    
    @IBAction func action_settings(_ sender: Any) {
        if (self.userName == UserDefaults.standard.object(forKey: "userName") as! String){
            let SettingsVC = storyboard?.instantiateViewController(withIdentifier:
                "SettingsVC") as! SettingsVC
            self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            SettingsVC.userDetailsModel = self.userDetailsModel
//            SettingsVC.isOwnProfileClicked = true
            present(SettingsVC, animated: true, completion: {})
        }else{
            self.navigationController?.popViewController(animated: true)
        }
//        if (self.userName == UserDefaults.standard.object(forKey: "userName") as! String){
//            let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
//            settingsVC.userDetailsModel = self.userDetailsModel
//            UIView.animate(withDuration: 2.0,delay: 1.20,options: UIViewAnimationOptions.curveEaseInOut,animations: {
//                settingsVC.view.transform = CGAffineTransform(translationX: 200, y: 200)
//            },completion: { finished in
//                self.navigationController.pushViewController(settingsVC, animated: false)
//            })
//        }
//        else{
//            self.navigationController.popViewController(animated: true)
//        }
    }
    
    @IBAction func action_chatBtn(_ sender: Any) {
        self.letsChat()
    }
    // MARK: Api 
    
    //get all user data
    func callgetApi(userName: String) {
        self.startActivity()
        AppServiceManager.getUserDetails(username: userName, handler: {
            (object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil {
                    if object != nil {
                        self.userDetailsModel = object as! UserModel
                        self.refreshData()
                    }
                    else {
                        self.showAlert(withTitle: "", andMessage: message!)
                    }
                }
                else {
                    let errorAsString = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: errorAsString!)
                }
            })
        })
    }
    
    //get the posted data
    func call_own_postedData(epochTime:Int64)  {
        self.startActivity()
        AppServiceManager.getOwnFeed(userName: "\(self.userName)",time: "\(epochTime)") { (object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil{
                    if object != nil{
                        self.dataSource = object as! NSMutableArray
                        if self.dataSource.count != 0{
                            self.imageViewButton1.image = UIImage(named: "GridBeige")
                            self.imageViewButton2.image = UIImage(named: "Photos")
                            self.imageViewButton3.image = UIImage(named: "about")
                           self.collectionViewGrid.reloadData()
                        }else{
                            let point = CGPoint(x:self.view.frame.width * 2, y:0)
                            self.scrollViewMain.setContentOffset(point, animated: true)
                        }
                        self.tableViewSecondPage.reloadData()

                    }else{
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    let error_as_string = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_as_string)
                }
            })
        }
    }
    
    //Update status
    func update_user(){
        registerModel.name = userDetailsModel.name
        registerModel.age = userDetailsModel.age
        registerModel.dob = userDetailsModel.dob
        registerModel.email = userDetailsModel.email
        registerModel.gender = userDetailsModel.gender
        registerModel.hobbies = userDetailsModel.hobbies
        registerModel.phNo = userDetailsModel.phNo
        registerModel.profilePic = userDetailsModel.profilePic
        registerModel.userName = userDetailsModel.userName
        if self.userDetailsModel.private_mode{
            registerModel.private_mode = "true"
        }else{
            registerModel.private_mode = "false"
        }
        if self.text_bio_feed?.text == nil{
            registerModel.bio = ""
        }else{
            registerModel.bio = (self.text_bio_feed?.text)!
        }
        AppServiceManager.updateUser(model: registerModel, handler: {
            (object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil {
                    if object != nil {
                        self.showAlert(withTitle: "", andMessage: message!)
                    }
                    else {
                        self.showAlert(withTitle: "", andMessage: message!)
                    }
                }
                else {
                    let errorAsString = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: errorAsString!)
                }
            })
        })
    }
    
    //DELETE POST
    private func delete(postID: String, index: Int){
        startActivity()
        AppServiceManager.deletePost(postID: postID, handler: {(object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil{
                    if object != nil{
                        print("delete successfully")
                        self.showAlert(withTitle: "", andMessage: message)
                        self.dataSource.removeObject(at: index)
                        self.tableViewSecondPage.reloadData()
                    }else{
                        print("not delete successfully")
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    let error = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error)
                }
            })
        })
    }
    //FOLLOW AND UNFOLLOW API
    func follow_unfollow(username:String) {
        self.startActivity()
        AppServiceManager.follow_unfollow(username: username) { (object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil{
                    if object != nil {
                        self.showAlert(withTitle: "", andMessage: message, handler: { (_, _) in
                            self.followStatus = !(self.followStatus)
//                            self.getFollowing()
                            if self.followStatus {
                                self.buttonFollow.setImage(UIImage(named: "checkBoxWhite"), for: .normal)
                                self.buttonFollow.setTitle("", for: .normal)
                            }else{
                                self.buttonFollow.setTitle("Follow", for: .normal)
                                self.buttonFollow.setImage(UIImage(named: ""), for: .normal)
                            }
                        })
                        
                    }else{
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    let error_as_string = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_as_string)
                }
            })
        }
    }
    //get follower
    func getFollowers() {
        //self.startActivity()
        AppServiceManager.fetchFollower(handler: { (object,message,error) in
            DispatchQueue.main.async(execute: {() -> Void in
                if error == nil{
                    if object != nil{
                        //Count follower
                        self.follower = ((object as! NSArray).count)
                        print(self.follower)
//                        self.buttonFollow.setTitle("\(self.follower) Follow", for: .normal)
                        self.getUserRecommendation()
                    }else{
                        self.buttonFollow.setTitle("Follow", for: .normal)
                        self.getUserRecommendation()
                    }
                }else{
                    let error_as_string = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_as_string)
                }
                //self.stopActivity()
            })
        })
    }
    
    //Get Following
    func getFollowing(){
        AppServiceManager.fetchFollowing(handler: {(object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                if error == nil{
                    if object != nil{
                        //Set FollowStatus
                        let allFollower = object as! NSArray
                        print(allFollower.count)
                        self.followCount.setTitle("\(allFollower.count)", for: .normal)
                        for i in 0 ..< allFollower.count{
                            let followerModel = allFollower[i] as! FollowModel
                            if followerModel.iAmFollowingUserName == self.userName{
                                self.followStatus = true
                                break
                            }else{
                                self.followStatus = false
                            }
                        }
                        //UI changes on Folloe button
                        if self.followStatus {
                            self.buttonFollow.setTitle("", for: .normal)
                            self.buttonFollow.setImage(UIImage(named: "checkBoxWhite"), for: .normal)
                        }else{
                            self.buttonFollow.setTitle("Follow", for: .normal)
                            self.buttonFollow.setImage(UIImage(named: ""), for: .normal)
                        }
                        self.getMyRecommendation()
                    }else{
                        self.followStatus = false
                        self.buttonFollow.setTitle("Follow", for: .normal)
//                        self.buttonFollow.setImage(UIImage(named: "plus_1.png"), for: .normal)
                        self.getMyRecommendation()
                    }
                }else{
                    let error_API = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_API)
                }
            })
        })
    }
    
    // Get User Recommendation
    func getUserRecommendation()  {
        AppServiceManager.fetchUserRecommendation(userName: self.userName, handler: {(object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                if error == nil{
                    if (object != nil){
                        //Count Recomendation
                        let countRecommend = (object as! NSArray).count
//                        self.buttonRecommend.setTitle("\(countRecommend) Recommend", for: .normal)
                    }else{
//                        self.buttonRecommend.setTitle("\(0) Recommend", for: .normal)
                    }
                }else{
                    let errorAsString = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: errorAsString)
                }
            })
        })
    }
    //get my Recommendation
    func getMyRecommendation(){
        let userName = UserDefaults.standard.object(forKey: "userName") as! String
        AppServiceManager.fetchMyRecommendation(userName: userName, handler: {(object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                if error == nil {
                    if object != nil{
                        //Set RecomendationStatus
                        let allRecomender = object as! NSArray
                        for i in 0 ..< allRecomender.count {
                            let recomendModel = allRecomender[i] as! RecommendModel
                            if recomendModel.userNameRecommended == self.userName {
                                self.recommendStatus = true
                                break
                            }else{
                                self.recommendStatus = false
                            }
                        }
                        //changes on Recomend Button
                        if self.recommendStatus {
                            self.buttonRecommend.setImage(UIImage(named: "checkBoxBlack"), for: .normal)
                            self.buttonRecommend.setTitle("", for: .normal)
                        }else{
                            self.buttonRecommend.setImage(UIImage(named: ""), for: .normal)
                            self.buttonRecommend.setTitle("Recommend", for: .normal)
                        }
                    }else{
                        self.recommendStatus = false
                        self.buttonRecommend.setTitle("Recommend", for: .normal)
                    }
                }else{
                    let error_API = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_API)
                }
            })
        })
    }
    
    //Recommend or UnRecommend
    func recommendUnrecommend()  {
        AppServiceManager.recommendUnRecommend(userName: self.userName, handler: {(object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                if error == nil{
                    self.showAlert(withTitle: "", andMessage: message)
                    self.recommendStatus = !(self.recommendStatus)
                    if self.recommendStatus{
                        self.buttonRecommend.setImage(UIImage(named: "checkBoxBlack"), for: .normal)
                        self.buttonRecommend.setTitle("", for: .normal)
//                        self.buttonRecommend.setTitle("UnRecommend", for: .normal)
                    }else{
                        self.buttonRecommend.setImage(UIImage(named: ""), for: .normal)
                        self.buttonRecommend.setTitle("Recommend", for: .normal)
                    }
                }else{
                    let errorAsString = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: errorAsString)
                }
            })
        })
    }

    // Chat
    func letsChat(){
        print(self.userName)
        self.startActivity()
        AppServiceManager.createP2PChat(type: "p2p", userName: self.userName, handler: {(object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil {
                    if object != nil{
                        let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                        chatVC.chatModel = ChatGroupModel(object as! NSDictionary)
                        chatVC.isChatRoomClicked = false
                        //chatVC.userDetailsModel = self.userDetailsModel
                        //chatVC.type = "private"
                        self.navigationController?.pushViewController(chatVC, animated: true)
                    }else{
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    let error_ApiCall = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_ApiCall)
                }
            })
        })
    }
    
    //Like / Unlike
    func call_like_unlike(postid: String) -> Bool{
        //self.startActivity()
        var success:Bool = false
        AppServiceManager.like_Post(postID: postid) { (object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil{
                    if object != nil{
                        let currentTime = self.getCurrentMillis()
                        self.call_own_postedData(epochTime: currentTime )
                        //self.showAlert(withTitle: "", andMessage: message)
                        success = true
                    }else{
                        self.showAlert(withTitle: "", andMessage: message)
                        success = false
                    }
                }else{
                    let error_as_string = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_as_string)
                    success = false
                }
            })
        }
        return success
    }
}
