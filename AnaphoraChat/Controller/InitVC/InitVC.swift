//
//  InitVC.swift
//  AnaphoraChat
//
//  Created by subhajit halder on 22/01/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit

class InitVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func actionSignUp(_ sender: Any) {
        guard let signUp = storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC? else { return }
        self.navigationController?.pushViewController(signUp, animated: true)
    }
    
    
    @IBAction func actionSignIn(_ sender: Any) {
        guard let login = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC? else { return }
        
        login.isInitVC = true
        
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    

}
