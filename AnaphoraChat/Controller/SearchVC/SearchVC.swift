//
//  SearchVC.swift
//  AnaphoraChat
//
//  Created by Tanmoy on 24/05/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class SearchVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UITextFieldDelegate {
    
    
    
    //MARK:Outlets

    @IBOutlet weak var tableView_searchPeople: UITableView?
    @IBOutlet weak var tableView_searchPost: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var view_tableViewContainer: UIView!
    
    //MARK : VAR
    var dataSource_people = NSMutableArray()
    var dataSource_post = NSMutableArray()
    
    var success:Bool = false
    
    //MARK: ACTION
    @IBAction func action_postSearchBtn(_ sender: Any) {
        self.view_tableViewContainer.frame = CGRect(x: 0, y: self.view_tableViewContainer.frame.origin.y, width: self.view_tableViewContainer.bounds.size.width, height: self.view_tableViewContainer.bounds.size.height)
    }
    
    @IBAction func action_peopleSearchBtn(_ sender: Any) {
        
        self.view_tableViewContainer.frame = CGRect(x: -(self.view.bounds.size.width), y: self.view_tableViewContainer.frame.origin.y, width: self.view_tableViewContainer.bounds.size.width, height: self.view_tableViewContainer.bounds.size.height)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
//        
//        view.addGestureRecognizer(tap)
        
        searchBar.showsCancelButton = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Table View Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == self.tableView_searchPost){
            
            return self.dataSource_post.count
            
        }
        
        return self.dataSource_people.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == self.tableView_searchPost{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserFeedCell") as! UserFeedCell
            
            let model = self.dataSource_post[indexPath.row] as! UserFeedModel
            
            cell.refresh(model: model) { (object, type) in
                
                if type == "comment"{
                    
                    let userFeedDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "UserFeedDetailsVC") as! UserFeedDetailsVC
                    
                    userFeedDetailsVC.userFeedModel = model
                    
                    self.navigationController?.pushViewController(userFeedDetailsVC, animated: true)
                    
                    
                }else if type == "like"{
                    
                    let result = self.call_like_unlike(postid: model.postID)
                    
                    
                    if result{
                        
                        if cell.image_love?.image == UIImage(named:"heart_select"){
                            
                            cell.image_love?.image = UIImage(named:"heart_darkgrey")
                            
                        }else{
                            
                            cell.image_love?.image = UIImage(named:"heart_select")
                            
                        }
                        
                    }else{
                        
                        
                        if cell.image_love?.image == UIImage(named:"heart_select"){
                            
                            cell.image_love?.image = UIImage(named:"heart_darkgrey")
                            
                        }else{
                            
                            cell.image_love?.image = UIImage(named:"heart_select")
                            
                        }
                        
                    }
                    
                    
                }else if type == "option"{ // Action Option
                    let model = object as! UserFeedModel
                    let  optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    if model.userName == (UserDefaults.standard.value(forKey: "userName") as! String) {
                        
                        let deletePost = UIAlertAction(title: "DELETE POST", style: .default, handler: {action -> Void in
                            
                            self.delete(postID: model.postID, index: indexPath.row)
                            
                            print("Click on Delete")
                            
                            
                        })
                        optionMenu.addAction(deletePost)
                    }else{
                        let reportPost = UIAlertAction(title: "REPORT THIS POST", style: .default, handler: { action -> Void in
                            
                            self.report(postID: model.postID)
                            
                            print("click on Report")
                        })
                        
                        optionMenu.addAction(reportPost)
                    }
                    let cancel = UIAlertAction(title: "CANCEL", style: .default, handler: { action -> Void in
                        
                        print("cancel event trigar")
                        
                    })
                    
                    optionMenu.addAction(cancel)
                    
                    self.present(optionMenu, animated: true, completion: nil)
                    
                }
                
            }
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell") as! SearchCell
            
            let model = self.dataSource_people[indexPath.row] as! UserModel
            
            cell.refreshCellData(model: model){(object) in
                
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableView_searchPeople {
            
            let cell = self.tableView_searchPeople?.cellForRow(at: indexPath) as! SearchCell
            
            if cell.isSelected{
                let profileVC = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                let model = self.dataSource_people[indexPath.row] as! UserModel
                
                profileVC.userName = model.userName
                
                self.navigationController?.pushViewController(profileVC, animated: true)
            }
        }
        
    }
    
    //MARK: Search bar Delegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.startActivity()
        self.dataSource_people.removeAllObjects()
        self.dataSource_post.removeAllObjects()
        self.searchAPI()
        
        searchBar.resignFirstResponder()
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        view.endEditing(true)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Call API
    
    func searchAPI() {
        
        
        AppServiceManager.findChat(userName: "\(self.searchBar.text ?? "")", handler: {(object, message, error) in
            
            DispatchQueue.main.async(execute:{ () -> Void in
                self.stopActivity()
                
                if error == nil {
                    if object != nil {
                        
                        let objectAsDictionary = object as? NSDictionary
                        
                        if(objectAsDictionary?.value(forKey: "people") as? NSMutableArray)?.count != 0 {
                            
                            let tempData = objectAsDictionary?["people"] as? NSArray
                            
                            
                            tempData?.enumerateObjects(using: {(obj, index, stop) in
                                
                                let userList = UserModel(dict: obj as! NSDictionary)
                                
                                self.dataSource_people.add(userList)
                                
                            })
                            self.tableView_searchPeople?.reloadData()
                        }
                        if(objectAsDictionary?.value(forKey: "posts") as? NSMutableArray)?.count != 0 {
                            let tempData = objectAsDictionary?["posts"] as? NSArray
                            
                            tempData?.enumerateObjects(using: {(obj, index, stop) in
                                
                                let userFeedList = UserFeedModel(obj as! NSDictionary)
                                
                                self.dataSource_post.add(userFeedList)
                                
                            })
                            
                            self.tableView_searchPost?.reloadData()
                        }    
                    }else{
                        
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    
                    let string_as_error = error?.localizedDescription
                    
                    self.showAlert(withTitle: "", andMessage: string_as_error)
                }
            })
        })
    }
    
    //Call function for like
    
    func call_like_unlike(postid: String) -> Bool{
        
        self.startActivity()
        
        AppServiceManager.like_Post(postID: postid) { (object, message, error) in
            
            DispatchQueue.main.async(execute: {() -> Void in
                
                self.stopActivity()
                
                if error == nil{
                    
                    if object != nil{
                        
                        self.showAlert(withTitle: "", andMessage: message)
                        
                        self.success = true
                        
                    }else{
                        
                        self.showAlert(withTitle: "", andMessage: message)
                        
                        self.success = false
                        
                    }
                }else{
                    
                    let error_as_string = error?.localizedDescription
                    
                    self.showAlert(withTitle: "", andMessage: error_as_string)
                    
                    self.success = false
                }
            })
        }
        
        return success
    }
    
    //DELETE POST
    private func delete(postID: String, index: Int){
        
        startActivity()
        
        AppServiceManager.deletePost(postID: postID, handler: {(object, message, error) in
            
            DispatchQueue.main.async(execute: {() -> Void in
                
                self.stopActivity()
                
                if error == nil{
                    if object != nil{
                        print("delete successfully")
                        
                        self.showAlert(withTitle: "", andMessage: message)
                        
                        self.dataSource_post.removeObject(at: index)
                        
                        self.tableView_searchPost.reloadData()
                    }else{
                        print("not delete successfully")
                        
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                    
                }else{
                    
                    let error = error?.localizedDescription
                    
                    self.showAlert(withTitle: "", andMessage: error)
                    
                }
            })
        })
    }
    
    //REPORT POST
    private func report(postID: String){
        startActivity()
        
        AppServiceManager.reportPost(postID: postID, handler: {(object, message, error) in
            
            DispatchQueue.main.async(execute: {() -> Void in
                
                self.stopActivity()
                
                if error == nil{
                    if object != nil {
                        print("reported successfully")
                        
                        self.showAlert(withTitle: "", andMessage: message)
                    }else{
                        
                        print("not report successfully")
                        
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    
                    let error = error?.localizedDescription
                    
                    self.showAlert(withTitle: "", andMessage: error)
                    
                }
            })
        })
    }
    

}
