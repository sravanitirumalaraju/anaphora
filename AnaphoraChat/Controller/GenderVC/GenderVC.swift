//
//  GenderVC.swift
//  AnaphoraChat
//
//  Created by subhajit halder on 15/02/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit
import MapKit
import SocketIO

class GenderVC: UIViewController,CLLocationManagerDelegate {
    
    //MARK: Outlate
    @IBOutlet weak var female: UIView!
    @IBOutlet weak var male: UIView!
    @IBOutlet weak var view_loading: UIView!
    @IBOutlet weak var btn_close: UIButton!
    
    //MARK: Var
    var chatType = ""
    var chatGroupModel = ChatGroupModel()
    
    var gender = ""
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var lat = ""
    var lng = ""
    
    //var socket : SocketIOClient?
    let delegate = UIApplication.shared.delegate as! AppDelegate
    var user_gender = ""
    var emiterData = NSDictionary()
    var vc = LoaderVC()
    
    //MARK: Method
    override func viewDidLoad() {
        super.viewDidLoad()
        locManager.requestWhenInUseAuthorization()
//        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
//
//            currentLocation = locManager.location
//
//            self.lat = "\(currentLocation.coordinate.latitude)" //as! String
//            self.lng = "\(currentLocation.coordinate.longitude)" //as! String
//
//            print("lat=> \(lat) & lng=> \(lng) & type=> \(chatType)")
//        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.layoutIfNeeded()
        
        female.layer.cornerRadius = female.frame.size.width / 2
        female.clipsToBounds = true
        
        male.layer.cornerRadius = male.frame.size.width / 2
        male.clipsToBounds = true
        
        locManager.delegate = self
        
        locManager.startUpdatingLocation()
        
        
        user_gender = UserDefaults.standard.value(forKey: "userGender") as! String
        
        vc = storyboard?.instantiateViewController(withIdentifier: "LoaderVC") as! LoaderVC
        
        self.socketConnectionInit()
        
    }
    //Location Delegate
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.startActivity()
        DispatchQueue.main.async(execute: {() -> Void in
            self.stopActivity()
            
            let location = locations.last
            //        currentCoordinates = location?.coordinate
            let lat = Double((location?.coordinate.latitude)!)
            let long = Double((location?.coordinate.longitude)!)
            
            self.locManager.stopUpdatingLocation()
            
            print("\(lat),\(long)")
            
            self.lat = "\(lat)"
            
            self.lng =  "\(long)"
        })
    }
    
    func socketConnectionInit(){
        
        let connectParam = ["userName": "\(UserDefaults.standard.object(forKey: "userName") as! String)"]//["conversationGroupID": "\(self.chatModel.conversationGroupID)",
        if !(delegate.socketIsConnected ?? false) {
            // Socket Not Connected
            delegate.socket = SocketIOClient(socketURL: (URL(string: "\(kBaseUrl)")! as NSURL) as URL, config: [.log(false), .compress, .connectParams(connectParam)])
            
            delegate.socket!.on(clientEvent: .connect, callback: {(data, ack) in
                print("****** Socket Connected ******")
                self.delegate.socketIsConnected = true
                // on Connect, set listeners
                self.setListeners()
            })
            delegate.socket?.on(clientEvent: .disconnect, callback: {(data, ack) in
                print("****** Socket Dis-Connected ******")
                self.delegate.socketIsConnected = false
            })
            delegate.socket?.disconnect()
            delegate.socket!.connect()
        }
        else {
            // Already Connected, set listeners
            self.setListeners()
        }
    }
    
    func setListeners(){
        self.delegate.socket?.on("result", callback: {(data, ack) in
            let result = data[0] as! NSDictionary
            let arr = result["users"] as! NSArray
            let endEpochTime = Int(result["creatorEpochTimeStamp"] as! String) as! Int
            let currentEpochTime = Int(NSDate().timeIntervalSince1970 * 1000)
            
            print("Number of user :> \(arr.count)")
            
            if ((arr.count >= 2)) {
                
                self.chatGroupModel = ChatGroupModel(result as! NSDictionary)
                self.vc.status = "search found"
                self.vc.endEpochTime = endEpochTime
                //self.vc.fourceDismiss(status: "search found")
                
            }else if (arr.count == 1) {
                //self.vc.fourceDismiss(status: "search expire")
                self.vc.endEpochTime = endEpochTime
                self.vc.status = "search not found"
                
            }
        })
    }
    
    func startSearching(){
        self.startActivity()
        emiterData = ["userName": "\(UserDefaults.standard.object(forKey: "userName") as! String)",
            "type": "\(self.chatType)",
            "user_gender": "\(self.user_gender)",
            "opt_gender": "\(self.gender)",
            "lat": "\(self.lat)",
            "lng": "\(self.lng)"]
        print(emiterData)
        self.delegate.socket?.emit("search", emiterData)
        
        self.vc.callBack(handler: {(message) in
            if message == "cancel search"{
                print("search is stop")
                self.delegate.socket?.emit("chatoff")
                self.delegate.socket?.emit("cancel")
            }
            if message == "search found"{
                print("search found")
                self.delegate.socket?.emit("chatoff")
                let messageVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                messageVC.latitude = self.lat
                messageVC.longitude = self.lng
                messageVC.chatModel = self.chatGroupModel
                messageVC.isChatRoomClicked = true
                messageVC.optedGender = self.gender
                messageVC.chatType = self.chatType
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(messageVC, animated: true)
                }
            }
            if message == "search expire"{
                self.delegate.socket?.emit("chatoff")
                
                let message = "There are not enough users to connect!"
                let alertVC = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                let findAgainBtn = UIAlertAction(title: "Search Again", style: .default, handler: {(action) in
                    self.startSearching()
                })
                
                let cancelBtn = UIAlertAction(title: "Cancel Searching", style: .cancel, handler: {(action) in
                    
                    self.navigationController?.popToRootViewController(animated: true)
                })
                alertVC.addAction(findAgainBtn)
                alertVC.addAction(cancelBtn)
                self.present(alertVC, animated: true, completion: nil)
            }
        })
        self.stopActivity()
        self.vc.startTimer()
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: Action
    @IBAction func action_backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func action_selectFemale(_ sender: Any) {
        self.gender = "Female"
        self.startSearching()
    }
    
    @IBAction func actionselectMale(_ sender: Any) {
        self.gender = "Male"
        self.startSearching()
    }
    //MARK: API Call
    /*
    func letsChat(){
//        self.stopActivity()
//        self.startActivity()
        
        AppServiceManager.createRandomChat(type: self.chatType, user_gender: self.user_gender, chatroom_opt_gender: self.gender, lat: self.lat, lng: self.lng, handler: {(object, message, error) in
            
            DispatchQueue.main.sync(execute: {() -> Void in
                
                if error == nil{
                
                    if object != nil{
                        self.stopActivity()
                        print("api call is working")
                        
                        self.chatGroupModel = ChatGroupModel(object as! NSDictionary)
                        
                        self.navigat()
                        
                    }else{
                        
                        if message == "Something Went Wrong! Please check your network or try again!"{
                            self.stopActivity()
                            self.delegate.socket?.emit("chatoff")
                            self.showAlert(withTitle: "", andMessage: message)
                        }else{
                            if self.second < 10{
                                self.letsChat()
                            }else{
                                self.stopActivity()
                                
                                self.delegate.socket?.emit("chatoff")
                                
                                let alertVC = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                                let findAgainBtn = UIAlertAction(title: "Search Again", style: .default, handler: {(action) in
                                    
                                    self.startActivity()
//                                    self.second = 0
//                                    self.startTimer()
//                                    self.letsChat()
                                    self.chatOnWithSearching()
                                })
                                
                                let cancelBtn = UIAlertAction(title: "Cancel Searching", style: .cancel, handler: {(action) in
                                    
                                    self.navigationController?.popToRootViewController(animated: true)
                                })
                                alertVC.addAction(findAgainBtn)
                                alertVC.addAction(cancelBtn)
                                self.present(alertVC, animated: true, completion: nil)
                            }
                        }
                    }
                }else{
                    self.stopActivity()
                    self.delegate.socket?.emit("chatoff")
                    let error_Api = error?.localizedDescription
                    
                    self.showAlert(withTitle: "", andMessage: error_Api)
                    
                }
            })
        })
    }*/
    
   
}
