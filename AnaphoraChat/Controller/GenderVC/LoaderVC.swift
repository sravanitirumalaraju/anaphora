//
//  LoaderVC.swift
//  ACCO
//
//  Created by Omnibuz Labs on 28/05/18.
//  Copyright © 2018 Omnibuz. All rights reserved.
//

import UIKit
import MBProgressHUD

class LoaderVC: UIViewController {
    
    typealias Handler = (_ message: String?) -> Void
    var handler : Handler!
    
    var progressLoader : MBProgressHUD!
    var endEpochTime = Int()
    var status = ""
    var timer = Timer()
    //var chatType = ""
    
    @IBOutlet weak var view_progress: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        progressLoader = MBProgressHUD.showAdded(to: view_progress, animated: true)
        progressLoader.label.text = "Searching..."
        
    }
    
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
    }
    
    func updateTime(){
        if status != ""{
            let currentEpochTime = Int(NSDate().timeIntervalSince1970 * 1000)
            print("cet:> \(currentEpochTime)")
            print("eet:> \(endEpochTime)")
            if currentEpochTime > endEpochTime {
                timer.invalidate()
                self.fourceDismiss(status: self.status)
            }
        }
    }
    
    func callBack(handler: @escaping Handler){
        self.handler = handler
    }
    func fourceDismiss(status: String){
        
        if status == "search found"{
            self.dismiss(animated: true, completion: {() in
                MBProgressHUD.hide(for: self.view_progress, animated: true)
                self.handler("search found")
            })
        }else if status == "search not found"{
            self.dismiss(animated: true, completion: {() in
                MBProgressHUD.hide(for: self.view_progress, animated: true)
                self.handler("search expire")
            })
        }
        
    }
    @IBAction func action_closeProgressBtn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: {() in
            MBProgressHUD.hide(for: self.view_progress, animated: true)
            self.handler("cancel search")
        })
    }
    

}
