  //
//  NotificationVC.swift
//  AnaphoraChat
//
//  Created by Tanmoy on 24/05/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: OUTLETS 
    @IBOutlet weak var tableView: UITableView?
    
    //MARK: VAR
    var dataSource = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.fetchNotification()
    }

    @IBAction func searchClicked(_ sender: Any) {
        let SearchVC = storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        self.navigationController?.pushViewController(SearchVC, animated: true)
    }
    @IBAction func HomeClicked(_ sender: Any) {
        let HomeVC = storyboard?.instantiateViewController(withIdentifier: "HomeRootVC") as! HomeRootVC
        self.navigationController?.pushViewController(HomeVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Tableview delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        let model = dataSource[indexPath.row] as! NotificationModel
        cell.refresh(model: model, handler: {(object) in
            let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            profileVC.userName = (object as! NotificationModel).related_user
            self.navigationController?.pushViewController(profileVC, animated: true)
        })
        return cell
    
    }
    //MARK: CALL API
    //fetch motification
    private func fetchNotification(){
        self.startActivity()
        AppServiceManager.notificationFetch(handler: { (object, message, error) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self.stopActivity()
                if error == nil {
                    if object != nil {
                        self.dataSource = object as! NSMutableArray
                        self.tableView?.reloadData()
                    }else{
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    let errorMessage = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: errorMessage)
                }
            })
        })
    }

}
