//
//  SettingsVC.swift
//  ACCO
//
//  Created by Tanmoy on 14/07/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit
import SDWebImage

class SettingsVC: UIViewController {

    
    @IBOutlet weak var switchOnOff: UISwitch!
    
    //MARK Var
    var userDetailsModel = UserModel()
    var registerModel = RegisterModel()
    
    //sravani
//    var isOwnProfileClicked = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.userDetailsModel.private_mode{
            self.switchOnOff.isOn = false
        }else{
            self.switchOnOff.isOn = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Actions
    
    @IBAction func action_back(_ sender: Any) {
//        print(isOwnProfileClicked)
//        if isOwnProfileClicked == false{
//            _ = self.navigationController?.popViewController(animated: true)
//        }else{
            self.dismiss(animated: true, completion: nil)
//        }
    }

    @IBAction func action_logout(_ sender: Any) {
        
        /*let defaults = UserDefaults.standard
        
        defaults.removeObject(forKey: "userName")
        defaults.removeObject(forKey: "token")
        
        
        let imageCache = SDImageCache.shared()
        
        imageCache.clearMemory()
        
        imageCache.clearDisk(onCompletion: {
            
        })
        
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        
        self.navigationController?.pushViewController(loginVC, animated: true)*/
        
        
        let alertController = UIAlertController(title: "Confirmation!", message: "Are you sure to logout ?" , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            
            let defaults = UserDefaults.standard
            
            defaults.removeObject(forKey: "userName")
            defaults.removeObject(forKey: "token")
            
            
            let imageCache = SDImageCache.shared()
            
            imageCache.clearMemory()
            
            imageCache.clearDisk(onCompletion: {
                
            })
            
            
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            let initialViewController = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "LoginVC")
            let nav = UINavigationController.init(rootViewController: initialViewController)
            nav.navigationController?.navigationBar.isHidden = true
            appDelegate.window?.rootViewController = nav
            
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func actionSwitchOnOff(_ sender: Any) {
        
        if self.switchOnOff.isOn{
            
            registerModel.private_mode = "false"
            
            self.updateUserDetails()
            
        }else{
            registerModel.private_mode = "true"
            
            self.updateUserDetails()
        }
        
    }
    
    //MARK : API
    
    func updateUserDetails() {
        
        registerModel.name = userDetailsModel.name
        registerModel.profilePic = userDetailsModel.profilePic
        registerModel.email = userDetailsModel.email
        registerModel.dob = userDetailsModel.dob
        registerModel.gender = userDetailsModel.gender
        registerModel.phNo = userDetailsModel.phNo
        registerModel.hobbies = userDetailsModel.hobbies
        registerModel.bio = userDetailsModel.bio
        registerModel.latLong = userDetailsModel.latLong.componentsJoined(by: ",")
        registerModel.location = userDetailsModel.location
        registerModel.timeZone = userDetailsModel.timeZone
        
        
        AppServiceManager.updateUser(model: registerModel, handler: {
            (object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                
                if error == nil {
                    if object != nil {
                        
                        self.showAlert(withTitle: "", andMessage: message!)
                        //self.switchOnOff.setOn(!(self.switchOnOff.isOn), animated: true)
                        
                    }
                    else {
                        self.showAlert(withTitle: "", andMessage: message!)
                    }
                }
                else {
                    
                    let errorAsString = error?.localizedDescription
                    
                    self.showAlert(withTitle: "", andMessage: errorAsString!)
                }
                
            })
            
        })
    }

}
