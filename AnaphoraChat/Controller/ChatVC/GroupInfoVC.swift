//
//  GroupInfoVC.swift
//  ACCO
//
//  Created by Omnibuz Labs on 06/12/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit
import SDWebImage

class GroupInfoVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Outlate
    @IBOutlet weak var txt_groupName: UITextField!
    @IBOutlet weak var image_groupPic: UIImageView!
    @IBOutlet weak var lbl_numberOfMember: UILabel!
    
    //MARK: Var
    var chatGroupModel = ChatGroupModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txt_groupName.text = chatGroupModel.title
        
        image_groupPic.sd_setImage(with: URL(string: "\(chatGroupModel.imageURL)"), placeholderImage: UIImage(named: "placeholder.png"))
        
        self.image_groupPic.layoutIfNeeded()
        
        self.image_groupPic.layer.cornerRadius = self.image_groupPic.frame.size.height / 2
        
        self.image_groupPic.clipsToBounds = true
        
        lbl_numberOfMember.text = "\(chatGroupModel.users.count) members"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Table View Delegate And DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatGroupModel.users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupMemberCell") as! GroupMemberCell
        
        cell.fillCell(user: self.chatGroupModel.users[indexPath.row] as! String)
        
        return cell
    }
    // select Row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        if (cell?.isSelected)!{
            let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            
            profileVC.userName = self.chatGroupModel.users[indexPath.row] as! String
            
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
    }
    
    
    //MARK: Action
    @IBAction func action_uploadImageBtn(_ sender: Any) {
        
    }
    @IBAction func action_backBtn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
}
