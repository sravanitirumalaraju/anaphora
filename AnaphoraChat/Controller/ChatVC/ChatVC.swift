//
//  ChatVC.swift
//  AnaphoraChat
//
//  Created by Tanmoy on 12/04/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit
import SDWebImage
import SocketIO

class ChatVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //MARK: Outlate
    
    @IBOutlet weak var text_chat: UITextField?
    
    @IBOutlet weak var scrollview: UIScrollView?

    @IBOutlet weak var view_text: UIView?
    
    @IBOutlet weak var varw_scroll: UIView?
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lbl_userName: UILabel!
    
    @IBOutlet weak var image_userPrfilePic: UIImageView!
    
    @IBOutlet weak var imageViewOnline: UIImageView!
    
    @IBOutlet weak var constrain_viewText: NSLayoutConstraint!
    
    @IBOutlet weak var btn_sendMessage: UIButton!
    
    @IBOutlet weak var height_leftMessageView: NSLayoutConstraint!
    @IBOutlet weak var lbl_leftMessage: UILabel!
    
    @IBOutlet weak var findNewBtn: UIButton!
    @IBOutlet weak var disConnectBtn: UIButton!
    @IBOutlet weak var aboutMeLbl: UILabel!
    
    @IBOutlet weak var rightArrowBtn: UIButton!
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    //MARK: Var
    //var type = ""
    var tabbarHeight = CGFloat()
    var chatModel = ChatGroupModel()
    //var userDetailsModel = UserModel()
    //var socket: SocketIOClient?
    let delegate = UIApplication.shared.delegate as! AppDelegate
    var conversationGroupID = ""
    var userName = ""
    var onlineStatus = false
    var dataSource = NSMutableArray()
    var keyboardHeight: CGFloat = 0.0
//    var tabBarHeight: CGFloat = 0.0
    var leftUser = ""
    var leftConversationGroupID = ""
    
    //sravani
    var isChatRoomClicked = Bool()
    
    var vc = LoaderVC()
    var optedGender = String()
    var latitude = String()
    var longitude = String()
    var chatType = ""
    //MARK: Method
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                let socketData = ["userName": "\(UserDefaults.standard.object(forKey: "userName") as! String)",
                    "conversationGroupID": "\(self.chatModel.conversationGroupID)"]
                self.delegate.socket?.emit("leave", socketData)
                self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
//        navigationController?.setNavigationBarHidden(true, animated: true)
        if isChatRoomClicked == false{
            bottomView.isHidden = true
            self.constrain_viewText.constant = 0.0
            rightArrowBtn.isHidden = false
            followBtn.isHidden = false
        }else{
            bottomView.isHidden = false
            rightArrowBtn.isHidden = true
            followBtn.isHidden = true
        }
//        tabBarHeight = (tabBarController?.tabBar.frame.size.height)!
        disConnectBtn.backgroundColor = UIColor(red: 222.0/255.0, green: 170.0/255.0, blue: 136.0/255.0, alpha: 1.0)
        disConnectBtn.layer.cornerRadius = disConnectBtn.frame.size.height/2
        findNewBtn.layer.borderColor = UIColor(red: 222.0/255.0, green: 170.0/255.0, blue: 136.0/255.0, alpha: 1.0).cgColor
        findNewBtn.layer.borderWidth = 1
        findNewBtn.layer.cornerRadius = findNewBtn.frame.size.height/2
        let url = "\(kBaseUrl)\(self.chatModel.imageURL)"
        var userDict = NSDictionary()
        if chatModel.type == "p2p" {
            if (self.chatModel.users[0] as! String == UserDefaults.standard.object(forKey: "userName") as! String) {                
                self.lbl_userName.text = self.chatModel.users[1] as? String
//                print(self.chatModel.users[1] as! String)
                userDict = self.chatModel.user_details["\(self.chatModel.users[1] as! String)"]! as! NSDictionary
                print(userDict["bio"] as! String)
                self.aboutMeLbl.text = userDict["bio"] as? String
                self.image_userPrfilePic.sd_setImage(with: URL(string: "\(kBaseUrl)" + "/api/auth/photo/" + "\(chatModel.users[1])"), placeholderImage: UIImage(named: "user.png"))
            }else{
                self.lbl_userName.text = self.chatModel.users[0] as? String
                userDict = self.chatModel.user_details["\(self.chatModel.users[0] as! String)"]! as! NSDictionary
                print(userDict["bio"] as! String)
                self.aboutMeLbl.text = userDict["bio"] as? String
//                print(self.chatModel.user_details)
                self.image_userPrfilePic.sd_setImage(with: URL(string: "\(kBaseUrl)" + "/api/auth/photo/" + "\(chatModel.users[0])"), placeholderImage: UIImage(named: "user.png"))
            }
        }else if chatModel.type == "anon" {
            self.lbl_userName.text = "Anonymous"
            self.image_userPrfilePic.sd_setImage(with: URL(string: url), placeholderImage:UIImage(named:"user.png"))
        }else if chatModel.type == "group" {
            self.lbl_userName.text = chatModel.users.componentsJoined(by: ", ")
            self.image_userPrfilePic.sd_setImage(with: URL(string: url), placeholderImage:UIImage(named:"user.png"))
        }
        self.image_userPrfilePic.layer.cornerRadius = self.image_userPrfilePic.frame.size.height / 2
        self.image_userPrfilePic.clipsToBounds = true
        //self.text_chat?.layer.cornerRadius = (self.text_chat?.frame.height)!/2
        self.tableView.estimatedRowHeight = 50
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        //Socket
        let data_to_send = ["conversationGroupID": "\(self.chatModel.conversationGroupID)",
            "userName": "\(UserDefaults.standard.object(forKey: "userName") as! String)"]//[
        if !(delegate.socketIsConnected ?? false){
            delegate.socket = SocketIOClient(socketURL: (URL(string: "\(kBaseUrl)")! as NSURL) as URL, config: [.log(false), .compress, .connectParams(data_to_send)])
            delegate.socket!.on(clientEvent: .connect, callback: {(data, ack) in
                print("****** Socket Connected ******")
                print("id of conversationGroup=> \(self.chatModel.id) & conversationGroupID=> \(self.chatModel.conversationGroupID) & userName=> \(UserDefaults.standard.object(forKey: "userName") as! String)")
                self.delegate.socketIsConnected = true
                self.delegate.socket?.emit("enter", data_to_send)
                // set flag to true
                self.onlineStatus = true
            })
            delegate.socket!.on(clientEvent: .disconnect, callback: {(data, ack) in
                print("****** Socket Dis-Connected ******")
                print("id of conversationGroup=> \(self.chatModel.id) & conversationGroupID=> \(self.chatModel.conversationGroupID) & userName=> \(UserDefaults.standard.object(forKey: "userName") as! String)")
                self.delegate.socketIsConnected = false
                // set flag to true
                self.onlineStatus = true
            })
            
            delegate.socket?.disconnect()
            delegate.socket!.connect()
        }else{
            self.onlineStatus = true
            
            self.delegate.socket?.emit("enter", data_to_send)
            
            //Event Listener
            delegate.socket!.on("joined", callback: {(data, ack) in
                print("\(data)")
                print("****** Someone Joined Conversation ******")
            })
            
            delegate.socket!.on("refresh", callback: {(data, ack) in
                
                DispatchQueue.main.async(execute: {() -> Void in
                    
                    print("****** Received A New Message ******")
                    
                    let data_dict = data[0]
                    
                    let model = MessageModel(dictionary: data_dict as! NSDictionary)
                    
                    print(model)
                    
                    
                    self.dataSource.add(model)
                    
                    self.tableView.beginUpdates()
                    
                    let indexPath:IndexPath = IndexPath(row:(self.dataSource.count - 1), section:0)
                    
                    self.tableView.insertRows(at: [indexPath], with: .left)
                    
                    self.tableView.endUpdates()
                    
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                    
                })
                
            })
            
            delegate.socket?.on("left", callback: {(data, ask) in
                DispatchQueue.main.async {
                    print("******* Left one user *******")
                    let dict = data[0] as! NSDictionary
                    self.leftUser = dict["userName"] as! String
                    self.leftConversationGroupID = dict["conversationGroupID"] as! String
                    
                    self.lbl_leftMessage.text = "\(self.leftUser) left this conversation"
                    self.height_leftMessageView.constant = 40
                    
                }
            })
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.statusBarStyle = .lightContent
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor.lightGray
        
        let time = "\(Int(NSDate().timeIntervalSince1970 * 1000))"
        self.getAllMessage(conversationGroupID: self.chatModel.conversationGroupID, fromTime: time)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        self.height_leftMessageView.constant = 0
        
        socketConnectionInit()
        vc = storyboard?.instantiateViewController(withIdentifier: "LoaderVC") as! LoaderVC
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor.black
//        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    //For show text field view top of keyboard
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.keyboardHeight = keyboardSize.height
            print(keyboardHeight)
        }
        
        self.constrain_viewText.constant = self.keyboardHeight //- tabBarHeight
        DispatchQueue.main.async {
            
            if self.dataSource.count > 0 {
                let indexPath:IndexPath = IndexPath(row:(self.dataSource.count - 1), section:0)
                
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    //MARK: Statusbar 
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    //MARK: table View Delegate & DataSource
    //Number of Section
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    // Number Of Row
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    // Create Cell
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = dataSource[indexPath.row] as! MessageModel
        print(model.sender)
        if model.sender != UserDefaults.standard.object(forKey: "userName")  as! String {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellOther", for: indexPath) as! CellOther
            cell.refreshWithData(messageModel: model, chatModel: self.chatModel)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellOwn", for: indexPath) as! CellOwn
            cell.refreshWithData(messageModel: model, chatModel: self.chatModel)
            return cell
        }
    }
    // Select Row
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath)
        let model = self.dataSource[indexPath.row] as! MessageModel
        if (cell?.isSelected)!{
            print("Message ID is -> \(model._id) and Message is -> \(model.message_text)")
            let optionShit = UIAlertController(title: "Options", message: nil, preferredStyle: .actionSheet)
            let delete = UIAlertAction(title: "Delete Message", style: .default, handler: { action -> Void in
                print("Delete Clicked")
                self.dataSource.removeObject(at: indexPath.row)
                self.deleteMessage(id: model._id, indexPath: indexPath, index: indexPath.row)
            })
            optionShit.addAction(delete)
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { action -> Void in
                print("Cancel Clicked")
            })
            optionShit.addAction(cancel)
            self.present(optionShit, animated: true, completion: nil)
        }
    }
    
    //MARK: Text Field Delegates
    public func textFieldDidBeginEditing(_ textField: UITextField) {
//        let rect = textField.superview?.convert((self.view_text?.frame)!, to: self.view)
//        self.scrollview?.scrollContentForViewFrame(activeFrame: rect!, andParentView: self.view)
        textField.autocorrectionType = .no
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if isChatRoomClicked == false{
            self.constrain_viewText.constant = 0.0
        }else{
            self.constrain_viewText.constant = 40.0
        }
        return true
    }
    //MARK: Action
    //For back
    @IBAction func action_back(_ sender: Any) {
        //leave conversation and back
        let socketData = ["userName": "\(UserDefaults.standard.object(forKey: "userName") as! String)",
            "conversationGroupID": "\(self.chatModel.conversationGroupID)"]
        self.delegate.socket?.emit("leave", socketData)
        self.navigationController?.popViewController(animated: true)
//        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //Forr Send Message
    @IBAction func action_sendTextBtn(_ sender: Any) {
        if self.text_chat?.text?.trimString() != ""{
            let message_text = self.text_chat?.text
            self.text_chat?.text = ""
            self.sendMessage(messageTxt: message_text!)
        }else{
            //self.showAlert(withTitle: "Warning!", andMessage: "Blank Message con't be sent")
        }
    }
    
    //For Option
    @IBAction func action_optionBtn(_ sender: Any) {
        
        let optionShit = UIAlertController(title: "Options", message: nil, preferredStyle: .actionSheet)
        
        //Report
        let reportChat = UIAlertAction(title: "Report Chat", style: .default, handler: { action in
            
            print("Report Chat alart action clicked")
            
        })
        optionShit.addAction(reportChat)
        
        //Delete
        let deleteConversation = UIAlertAction(title: "Delete Conversation", style: .default, handler: { action in
            
            print("Delete Conversation alart action clicked")
            let confermationAlert = UIAlertController(title: "Warning", message: "Are you want to delete conversation" , preferredStyle: .alert)
            
            let yes = UIAlertAction(title: "Yes", style: .default, handler: {action in
                
                self.deleteChatGroupOrLeaveGroup(id: self.chatModel.id)
                
            })
            confermationAlert.addAction(yes)
            
            let cancel = UIAlertAction(title: "No", style: .cancel, handler: nil)
            confermationAlert.addAction(cancel)
            
            self.present(confermationAlert, animated: true, completion: nil)
            
            
        })
        optionShit.addAction(deleteConversation)
        if chatModel.type == "p2p" {
            
            //Profile Info
            let info = UIAlertAction(title: "Info", style: .default, handler: { action in
                
                print("Info alart action clicked")
                
                let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                
                if (self.chatModel.users[0] as! String == UserDefaults.standard.object(forKey: "userName") as! String) {
                    
                    profileVC.userName = self.chatModel.users[1] as! String
                }else{
                    profileVC.userName = self.chatModel.users[0] as! String
                }
                
                self.navigationController?.pushViewController(profileVC, animated: true)
            })
            optionShit.addAction(info)
            
            //BLock User
            let blockUser = UIAlertAction(title: "Block User", style: .default, handler: { action in
                
                print("Block User alart action clicked")
                
                var blockUserName = ""
                
                if (self.chatModel.users[0] as! String == UserDefaults.standard.object(forKey: "userName") as! String) {
                    
                     blockUserName = self.chatModel.users[1] as! String
                    
                }else{
                    
                    blockUserName = self.chatModel.users[0] as! String
                    
                }
                
                self.blockUser(blockUserName: blockUserName)
                
            })
            optionShit.addAction(blockUser)
        }
        if chatModel.type == "group" {
            
            //Group Info
            let groupInfo = UIAlertAction(title: "Group Info", style: .default, handler: { action in
                
                print("Group Info alart action clicked")
                
                let groupInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "GroupInfoVC") as! GroupInfoVC
                
                groupInfoVC.chatGroupModel = self.chatModel
                
                self.navigationController?.pushViewController(groupInfoVC, animated: true)
                
            })
            optionShit.addAction(groupInfo)
            
            //Leave
            let leaveChat = UIAlertAction(title: "Leave Chat", style: .default, handler: { action in
                
                print("Leave Chat alart action clicked")
                
                let confermationAlert = UIAlertController(title: "Warning", message: "Are you want to leave conversation" , preferredStyle: .alert)
                
                let yes = UIAlertAction(title: "Yes", style: .default, handler: {action in
                    
                    self.deleteChatGroupOrLeaveGroup(id: self.chatModel.id)
                    
                })
                confermationAlert.addAction(yes)
                
                let cancel = UIAlertAction(title: "No", style: .cancel, handler: nil)
                confermationAlert.addAction(cancel)
                
                self.present(confermationAlert, animated: true, completion: nil)
                
            })
            optionShit.addAction(leaveChat)
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
            print("Cancel alart action clicked")
            
        })
        optionShit.addAction(cancel)
        
        self.present(optionShit, animated: true, completion: nil)
    }
    
    @IBAction func action_disconnectBtn(_ sender: Any) {
        
        print("Disconnect Button clicked")
        
        let confermationAlert = UIAlertController(title: "Warning", message: "Are you want to disconnect conversation" , preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Yes", style: .default, handler: {action in
            
            self.deleteChatGroupOrLeaveGroup(id: self.chatModel.id)
            
        })
        confermationAlert.addAction(yes)
        
        let cancel = UIAlertAction(title: "No", style: .cancel, handler: nil)
        confermationAlert.addAction(cancel)
        
        self.present(confermationAlert, animated: true, completion: nil)
    }
    
    //For close Left Message View
    @IBAction func action_closeLeftMessageView(_ sender: Any) {
        
        self.height_leftMessageView.constant = 0
    }
    
    
    //MARK: API
    
    //Send Message
    func sendMessage(messageTxt: String){
        AppServiceManager.sendMessage(conversationGroupID: self.chatModel.conversationGroupID, message_text: messageTxt, message_file: "", message_type: "text", handler: {(object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                if error == nil{
                    if object != nil{
                        if self.onlineStatus{
                            self.text_chat?.text = ""
                            //self.text_chat?.resignFirstResponder()
                            let dataModel = MessageModel(dictionary: object as! NSDictionary)
                            let socketData = ["userName": "\(dataModel.sender)",
                                "conversationGroupID": "\(dataModel.conversationGroupID)",
                                "conversationsID": "\(dataModel.conversationsID)",
                                "message": "\(dataModel.message_text)"]
                            self.delegate.socket?.emit("message", socketData)
                        }else{
                            print("socket not connect correctly")
                        }
                    }else{
                        self.showAlert(withTitle: "", andMessage: messageTxt)
                    }
                }else{
                    let error_api = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_api)
                }
            })
        })
    }
    
    // Get all Message
    func getAllMessage(conversationGroupID: String, fromTime: String){
        
        self.startActivity()
        
        AppServiceManager.allMessages(conversationGroupID: conversationGroupID, fromTime: fromTime, handler: {(object, message, error) in
            
            DispatchQueue.main.async(execute: {() -> Void in
                
                self.stopActivity()
                
                if error == nil {
                    
                    self.dataSource = object as! NSMutableArray
                    
                    self.dataSource = NSMutableArray(array: self.dataSource.reverseObjectEnumerator().allObjects).mutableCopy() as! NSMutableArray
       
                    self.tableView.reloadData()
                    
                    if self.dataSource.count > 0{
                        let indexPath:IndexPath = IndexPath(row:(self.dataSource.count - 1), section:0)
                        
                        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                    }
                    
                }else{
                    
                    let error_api = error?.localizedDescription
                    
                    self.showAlert(withTitle: "", andMessage: error_api)
                
                }
            })
            
        })
        
    }
    
    //Delete Message
    func deleteMessage(id: String, indexPath: IndexPath, index: Int){
        
        AppServiceManager.deleteMessage(id: id, handler: {(object, message, error) in
            
            DispatchQueue.main.async(execute: {() -> Void in
                
                if error == nil{
                    if object != nil {
                        
                        self.tableView.deleteRows(at: [indexPath], with: .automatic)
                        
                    }else{
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    let error_API = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_API)
                }
            })
        })
    }
    
    //Delete Converssion And leave Chat
    func deleteChatGroupOrLeaveGroup(id: String){
        self.startActivity()
        
        AppServiceManager.deleteChateGroup(id: id, handler: {(object, message, error) in
            
            DispatchQueue.main.async(execute: {() -> Void in
                
                self.stopActivity()
                
                if error == nil{
                    
                    if object != nil {
                        
                        let socketData = ["userName": "\(UserDefaults.standard.object(forKey: "userName") as! String)",
                            "conversationGroupID": "\(self.chatModel.conversationGroupID)"]
                        
                        self.delegate.socket?.emit("leave", socketData)
                       
                        self.navigationController?.popToRootViewController(animated: true)
                        
                    }else{
                        
                        self.showAlert(withTitle: "", andMessage: message)
                    
                    }
                }else{
                    
                    let error_Api = error?.localizedDescription
                    
                    self.showAlert(withTitle: "", andMessage: error_Api)
                }
                
            })
        })
    }
    
    
    //Block User
    func blockUser(blockUserName: String){
        
        AppServiceManager.blockUser(blockUserName: blockUserName, handler: {(object, message, error) in
          
            DispatchQueue.main.async(execute: {() -> Void in
                
                if error == nil{
                   
                    if object != nil {
                        
                        self.showAlert(withTitle: "", andMessage: message, handler: { action in
                           
//                            let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//
//                            self.navigationController?.pushViewController(homeVC, animated: true)
                        //self.navigationController?.popViewController(animated: true)
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        })
                    }else{
                        
                        self.showAlert(withTitle: "", andMessage: message)
                    
                    }
                }else{
                    
                    let error_API = error?.localizedDescription
                    
                    self.showAlert(withTitle: "", andMessage: error_API)
                    
                }
            })
        })
    }
    
    @IBAction func followClicked(_ sender: Any) {
        
    }
    @IBAction func rightArrowClicked(_ sender: Any) {
        let profileVC = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        profileVC.userName = self.lbl_userName.text!
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    @IBAction func disConnectClicked(_ sender: Any) {
        
        print("Disconnect Button clicked")
        
        let confermationAlert = UIAlertController(title: "Warning", message: "Are you want to disconnect conversation" , preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Yes", style: .default, handler: {action in
            
            self.deleteChatGroupOrLeaveGroup(id: self.chatModel.id)
            
        })
        confermationAlert.addAction(yes)
        
        let cancel = UIAlertAction(title: "No", style: .cancel, handler: nil)
        confermationAlert.addAction(cancel)
        
        self.present(confermationAlert, animated: true, completion: nil)
    }
    func socketConnectionInit(){
        
        let connectParam = ["userName": "\(UserDefaults.standard.object(forKey: "userName") as! String)"]//["conversationGroupID": "\(self.chatModel.conversationGroupID)",
        if !(delegate.socketIsConnected ?? false) {
            // Socket Not Connected
            delegate.socket = SocketIOClient(socketURL: (URL(string: "\(kBaseUrl)")! as NSURL) as URL, config: [.log(false), .compress, .connectParams(connectParam)])
            
            delegate.socket!.on(clientEvent: .connect, callback: {(data, ack) in
                print("****** Socket Connected ******")
                self.delegate.socketIsConnected = true
                // on Connect, set listeners
                self.setListeners()
            })
            delegate.socket?.on(clientEvent: .disconnect, callback: {(data, ack) in
                print("****** Socket Dis-Connected ******")
                self.delegate.socketIsConnected = false
            })
            delegate.socket?.disconnect()
            delegate.socket!.connect()
        }
        else {
            // Already Connected, set listeners
            self.setListeners()
        }
    }
    func setListeners(){
        self.delegate.socket?.on("result", callback: {(data, ack) in
            let result = data[0] as! NSDictionary
            let arr = result["users"] as! NSArray
            let endEpochTime = Int(result["creatorEpochTimeStamp"] as! String) as! Int
            let currentEpochTime = Int(NSDate().timeIntervalSince1970 * 1000)
            
            print("Number of user :> \(arr.count)")
            
            if ((arr.count >= 2)) {
                
                self.chatModel = ChatGroupModel(result as! NSDictionary)
                self.vc.status = "search found"
                self.vc.endEpochTime = endEpochTime
                //self.vc.fourceDismiss(status: "search found")
                
            }else if (arr.count == 1) {
                //self.vc.fourceDismiss(status: "search expire")
                self.vc.endEpochTime = endEpochTime
                self.vc.status = "search not found"
                
            }
        })
    }
    @IBAction func findNewClicked(_ sender: Any) {
        startSearching()
    }
    
    func startSearching(){
        self.startActivity()
        var emiterData = NSDictionary()
        
        emiterData = ["userName": "\(UserDefaults.standard.object(forKey: "userName") as! String)",
            "type": "\(self.chatType)",
            "user_gender": "\(UserDefaults.standard.value(forKey: "userGender") as! String)",
            "opt_gender": "\(optedGender)",
            "lat": "\(self.latitude)",
            "lng": "\(self.longitude)"]
        print(emiterData)
        self.delegate.socket?.emit("search", emiterData)
        
        self.vc.callBack(handler: {(message) in
            if message == "cancel search"{
                print("search is stop")
                self.delegate.socket?.emit("chatoff")
                self.delegate.socket?.emit("cancel")
            }
            if message == "search found"{
                print("search found")
                self.delegate.socket?.emit("chatoff")
                
                let messageVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                messageVC.chatModel = self.chatModel
                messageVC.isChatRoomClicked = true
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(messageVC, animated: true)
                }
            }
            if message == "search expire"{
                self.delegate.socket?.emit("chatoff")
                
                let message = "There are not enough users to connect!"
                let alertVC = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                let findAgainBtn = UIAlertAction(title: "Search Again", style: .default, handler: {(action) in
                    self.startSearching()
                })
                
                let cancelBtn = UIAlertAction(title: "Cancel Searching", style: .cancel, handler: {(action) in
                    
                    self.navigationController?.popToRootViewController(animated: true)
                })
                alertVC.addAction(findAgainBtn)
                alertVC.addAction(cancelBtn)
                self.present(alertVC, animated: true, completion: nil)
            }
        })
        self.stopActivity()
        self.vc.startTimer()
        self.present(vc, animated: true, completion: nil)
    }
}
