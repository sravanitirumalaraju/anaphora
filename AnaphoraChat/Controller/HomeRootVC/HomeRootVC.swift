//
//  HomeRootVC.swift
//  AnaphoraChat
//
//  Created by Tanmoy on 24/05/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class HomeRootVC: UIPageViewController, UIPageViewControllerDataSource {
    
    lazy var viewControllerList:[UIViewController] = {
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let FeedVC = self.storyboard?.instantiateViewController(withIdentifier: "FeedVC") as! FeedVC
        let MapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapVC") as! MapVC
        return[FeedVC, homeVC, MapVC]
//        let notificationVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
//        let serachVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
//        return[notificationVC, homeVC, serachVC]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        let firstVC = viewControllerList[1]
        self.setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vcIndex =  viewControllerList.index(of: viewController) else{ return nil}
        let previousIndex =  vcIndex - 1
        guard previousIndex >= 0 else{return nil}
        guard viewControllerList.count > previousIndex else{return nil}
        
        return viewControllerList[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vcIndex =  viewControllerList.index(of: viewController) else{ return nil}
        let nextIndex =  vcIndex + 1
        guard viewControllerList.count != nextIndex else{return nil}
        guard viewControllerList.count > nextIndex else{return nil}
        
        return viewControllerList[nextIndex]
    }
    

}
