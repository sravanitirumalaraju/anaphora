//
//  MapVC.swift
//  AnaphoraChat
//
//  Created by subhajit halder on 22/01/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class MapVC: UIViewController, UITableViewDataSource,UITableViewDelegate,GMSMapViewDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var tableViewExplore: UITableView!
    @IBOutlet weak var viewGmap: UIView!
    //MARK: VARS
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var mapView: GMSMapView!
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 0.04
    var markercurrent:GMSMarker?
    var lat = ""
    var lng = ""
    var dataSource =  NSMutableArray()
    var loadFlag:Bool = true
    var refreshControl : UIRefreshControl!
    let kStartingImageScale = 3.0 // make it available in your class.
    // A default location to use when location permission is not granted.
    let defaultLocation = CLLocation(latitude: -33.869405, longitude: 151.199)
    
    //MARK: Models 
    
    var followModel = FollowModel()
    
    //MARK: Method
    override func viewDidLoad() {
        super.viewDidLoad()
        placesClient = GMSPlacesClient.shared()
        //MARK: Create a map.
//        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: zoomLevel)
//        mapView = GMSMapView.map(withFrame: (self.viewGmap?.bounds)!, camera: camera)
        
        let camera = GMSCameraPosition.camera(withLatitude: defaultLocation.coordinate.latitude,
                                              longitude: defaultLocation.coordinate.longitude,
                                              zoom: zoomLevel)
        mapView = GMSMapView.map(withFrame: (self.viewGmap?.bounds)!, camera: camera)
        
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        mapView.settings.myLocationButton = false
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //mapView.isMyLocationEnabled = true
        mapView.settings.zoomGestures = true
        mapView.setMinZoom(2, maxZoom: 8)
        mapView.delegate = self
        
        // Add the map to the view, hide it until we've got a location update.
        viewGmap?.addSubview(mapView)
        
        //tableView Refreshing
        self.refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Data Refreshing")
        refreshControl.addTarget(self, action: #selector(self.refreshData), for: .valueChanged)
        self.tableViewExplore.refreshControl = refreshControl
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        locationManager.requestWhenInUseAuthorization()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Initialize the location manager.
        self.loadFlag = true
        //locationManager.requestWhenInUseAuthorization()
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 400
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }
    
    func zoomImg(_ imgPath: String){
        let fullImageVC = self.storyboard?.instantiateViewController(withIdentifier: "ShowFullImageVC") as! ShowFullImageVC
        fullImageVC.url = "\(kBaseUrl)\(imgPath)"
        fullImageVC.modalPresentationStyle = .overCurrentContext
        fullImageVC.modalTransitionStyle = .crossDissolve
        self.present(fullImageVC, animated: true, completion: nil)
    }
    //MARK: Table view
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExploreCell") as! ExploreCell
        let model = self.dataSource[indexPath.row] as! ExploreModel
        print(model.connection)
//        print(model.locationName)
//        print(model.userDetails.bio)
//        print(model.userDetails.location)
//        print(model.userDetails.latLong)
//        print(model.userFeed)
//        print(model.userName)
        cell.refresh(model: model) { (object, type) in
            let model = self.dataSource[indexPath.row] as! ExploreModel
            if type == "follow"{
                if(UserDefaults.standard.object(forKey: "userName")as! String == model.userName){
                    self.showAlert(withTitle: "Alert", andMessage: "You Con't Follow Yourself")
                }else{
                    self.follow_unfollow(username: model.userName)
                }
            }
            if type == "navigate"{
                let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                profileVC.userName = model.userName
                profileVC.isExploreSwiped = true
                self.navigationController?.pushViewController(profileVC, animated: true)
            }
            if type == "pic1"{
                if model.userFeed.count >= 1{
                    cell.image_1st?.isHidden = false
                    self.zoomImg("\(model.userFeed[0])")
                }else{
//                    self.zoomImg("placeholder.png")
//                    self.zoomImg("")
                    cell.image_1st?.isHidden = true
                }
            }
            if type == "pic2"{
                if model.userFeed.count >= 2{
                    cell.image_2nd?.isHidden = false
                    self.zoomImg("\(model.userFeed[1])")
                }else{
//                    self.zoomImg("placeholder.png")
//                    self.zoomImg("")
                    cell.image_2nd?.isHidden = true
                }
            }
            if type == "pic3"{
                if model.userFeed.count >= 3{
                    cell.image_3rd?.isHidden = false
                    self.zoomImg("\(model.userFeed[2])")
                }else{
//                    self.zoomImg("placeholder.png")
//                    self.zoomImg("")
                    cell.image_3rd?.isHidden = true
                }
            }
        }
        return cell
    }
    // refresh TableView Data
    func refreshData(){
        self.call_API()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = self.dataSource[indexPath.row] as! ExploreModel
        if model.userFeed.count == 0{
            return 110
        }else{
            return 140
        }
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        let marker = GMSMarker()
        marker.position = coordinate
        marker.isDraggable = true
        marker.icon = UIImage(named:"airo.png")
        marker.map = mapView
        
        self.lat = "\(coordinate.latitude)"
        self.lng = "\(coordinate.longitude)"
        self.call_API()
    }
    //MARK: API CALL
    func call_API(){
        self.startActivity()
        print("LAT> \(self.lat) LNG> \(self.lng)")
        AppServiceManager.getExploreFeed(lat: self.lat, lng: self.lng, distance: "250", offset: "0", limit: "10") { (object, message, error) in
        DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
            if error == nil{
                if object != nil {
                    self.dataSource = object as! NSArray as! NSMutableArray
                    self.refreshControl.endRefreshing()
                    self.tableViewExplore.reloadData()
                }else{
                    self.showAlert(withTitle: "", andMessage: message)
                }
            }else{
                let error_as_string = error?.localizedDescription
                self.showAlert(withTitle: "", andMessage: error_as_string)
            }
            })
        }
    }
    //FOLLOW AND UNFOLLOW API
    func follow_unfollow(username:String) {
        self.startActivity()
        AppServiceManager.follow_unfollow(username: username) { (object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil{
                    if object != nil {
                        self.followModel = object as! FollowModel
                        self.showAlert(withTitle: "", andMessage: message, handler: { (_, _) in
                            self.call_API()
                        })
                    }else{
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    let error_as_string = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_as_string)
                }
            })
        }
    }
}

extension MapVC: CLLocationManagerDelegate {
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        self.mapView.clear()
        self.markercurrent = GMSMarker(position:location.coordinate)

        self.markercurrent?.icon = UIImage(named:"airo.png")
        self.markercurrent?.map = mapView
        
        self.lat = "\(location.coordinate.latitude)"
        self.lng =  "\(location.coordinate.longitude)"
        
        if loadFlag {
            self.loadFlag = false
            self.call_API()
        }
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
    }

    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}

