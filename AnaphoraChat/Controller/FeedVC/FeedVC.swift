//
//  FeedVC.swift
//  AnaphoraChat
//
//  Created by subhajit halder on 22/01/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit

class FeedVC: UIViewController,UITableViewDataSource {

    //MARK: OUTLETS
    @IBOutlet weak var tableViewFeed: UITableView!
    
    //MARK: Vars
    var dataSource = NSMutableArray()
    var success:Bool = false
    var currentTime = Int64()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableViewFeed.estimatedRowHeight = 471
        tableViewFeed.rowHeight = UITableViewAutomaticDimension
        self.currentTime = getCurrentMillis()
        self.load_feed(epochTime: currentTime)
    }
    
    //MARK: Action
    //Upload new media
    @IBAction func action_feedUpload(_ sender: Any) {
        let feedupload = self.storyboard?.instantiateViewController(withIdentifier: "FeedUploadVC") as! FeedUploadVC
        self.navigationController?.pushViewController(feedupload, animated: true)
    }
    
    //Action Comment
    @IBAction func action_comment(_ sender: Any) {
        let userFeedDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "UserFeedDetailsVC") as! UserFeedDetailsVC
        self.navigationController?.pushViewController(userFeedDetailsVC, animated: true)
    }
    
    //Action Like_Unlike
    @IBAction func action_likeDislike(_ sender: Any) {
    }
    
    // MARK: TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserFeedCell") as! UserFeedCell
        let model = dataSource[indexPath.row] as! UserFeedModel
        print(model.commentsModel.count)
//        print(model.message)
        cell.refresh(model: model) { (object, type) in
            if type == "comment"{ // comment action
                let userFeedDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "UserFeedDetailsVC") as! UserFeedDetailsVC
                userFeedDetailsVC.userFeedModel = model
                self.navigationController?.pushViewController(userFeedDetailsVC, animated: true)
            }else if type == "like"{ // like action
               let result = self.call_like_unlike(postid: model.postID)
                if result{
                    if cell.image_love?.image == UIImage(named:"heart_select"){
                        cell.image_love?.image = UIImage(named:"heart_darkgrey")
                        cell.lbl_likeCounter?.text = "\(model.likesModel.count - 1) likes"
                        
//                        if model.likesModel.count == 1{
//                            cell.lbl_likeCounter?.text = "\(model.likesModel.count - 1) like"
//                        }else{
//                            cell.lbl_likeCounter?.text = "\(model.likesModel.count - 1) likes"
//                        }
                    }else{
                        cell.image_love?.image = UIImage(named:"heart_select")
                        cell.lbl_likeCounter?.text = "\(model.likesModel.count + 1) likes"
//                        if model.likesModel.count == 1{
//                            cell.lbl_likeCounter?.text = "\(model.likesModel.count + 1) like"
//                        }else{
//                            cell.lbl_likeCounter?.text = "\(model.likesModel.count + 1) likes"
//                        }
                    }
                }/*else{
                    if cell.image_love?.image == UIImage(named:"heart_select"){
                        cell.image_love?.image = UIImage(named:"heart_darkgrey")
                    }else{
                        cell.image_love?.image = UIImage(named:"heart_select")
                    }
                }*/
            }else if type == "option"{ // Action Option
                let model = object as! UserFeedModel
                let  optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                if model.userName == (UserDefaults.standard.value(forKey: "userName") as! String) {
                    let deletePost = UIAlertAction(title: "Delete Post", style: .default, handler: {action -> Void in
                        self.delete(postID: model.postID, index: indexPath.row)
                        print("Click on Delete")
                    })
                    optionMenu.addAction(deletePost)
                }else{
                    let reportPost = UIAlertAction(title: "Report This Post", style: .default, handler: { action -> Void in
                        self.report(postID: model.postID)
                        print("click on Report")
                    })
                    optionMenu.addAction(reportPost)
                }
                let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action -> Void in
                    print("cancel event trigar")
                })
                optionMenu.addAction(cancel)
                self.present(optionMenu, animated: true, completion: nil)
            }
        }
        return cell
    }
    
    //MARK: Epoch Time Call
    func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    //MARK: API CAL
    //Load Feed Data
    private func load_feed(epochTime:Int64){
        //self.startActivity()
        AppServiceManager.getConnectionFeed(time: "\(epochTime)") { (object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
            self.stopActivity()
            if error == nil{
                if object != nil{
                    self.dataSource.removeAllObjects()
                    self.dataSource = object as! NSMutableArray
                    self.tableViewFeed.reloadData()
                }else{
                    self.showAlert(withTitle: "", andMessage: message)
                }
            }else{
                let error_asData =  error?.localizedDescription
                self.showAlert(withTitle: "", andMessage: error_asData)
            }
        })
        }
    }
    //Call function for like
    func call_like_unlike(postid: String) -> Bool{
        //self.startActivity()
        AppServiceManager.like_Post(postID: postid) { (object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil{
                    if object != nil{
                        self.currentTime = self.getCurrentMillis()
                        self.load_feed(epochTime: self.currentTime )
                        //self.showAlert(withTitle: "", andMessage: message)
                        self.success = true
                    }else{
                        self.showAlert(withTitle: "", andMessage: message)
                        self.success = false
                    }
                }else{
                    let error_as_string = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_as_string)
                    self.success = false
                }
            })
        }
        return success
    }
    
    //DELETE POST
    private func delete(postID: String, index: Int){
        startActivity()
        AppServiceManager.deletePost(postID: postID, handler: {(object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil{
                    if object != nil{
                        print("delete successfully")
                        self.showAlert(withTitle: "", andMessage: message)
                        self.dataSource.removeObject(at: index)
                        self.tableViewFeed.reloadData()
                    }else{
                        print("not delete successfully")
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    let error = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error)
                }
            })
        })
    }
    
    //REPORT POST
    private func report(postID: String){
        startActivity()
        AppServiceManager.reportPost(postID: postID, handler: {(object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil{
                    if object != nil {
                        print("reported successfully")
                        self.showAlert(withTitle: "", andMessage: message)
                    }else{
                        print("not report successfully")
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    let error = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error)
                }
            })
        })
    }
    
}
