//
//  FeedUploadVC.swift
//  ACCO
//
//  Created by Tanmoy on 11/07/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class FeedUploadVC: UIViewController,UITextFieldDelegate, UIGestureRecognizerDelegate, UINavigationControllerDelegate,UIImagePickerControllerDelegate,CLLocationManagerDelegate {
    
    //MARK: IBOUTLETS
    @IBOutlet weak var image_upload: UIImageView?
    @IBOutlet weak var text_comment: UITextField?
    @IBOutlet weak var scrollView:UIScrollView?
    @IBOutlet weak var viewSubScroll: UIView?
    @IBOutlet weak var view_container: UIView?
    
    //MARK: Vars
    let imagePicker = UIImagePickerController()
    var locationManager = CLLocationManager()
    
    //Models
    var feedPostModel = FeedPostModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Initialize the location manager.
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        self.startActivity()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Statusbar hidden
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // gesture
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view == viewSubScroll {
            return true
        }
        return false
    }
    
    //MARK: API CALL
    private func saveImage(){
        self.startActivity()
        AppServiceManager.saveImage(withImage: (image_upload?.image)!, type: "p", handler: {
            (object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil {
                    if object != nil {
                        self.feedPostModel.imageURL = object as! String
                        self.upload_feed()
                    }
                    else {
                        self.showAlert(withTitle: "", andMessage: message!)
                    }
                }
                else {
                    self.showAlert(withTitle: "", andMessage: message!)
                }
                
            })
        })
    }
    
    func upload_feed(){
        self.startActivity()
        AppServiceManager.addFeed(model: self.feedPostModel) { (object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil{
                    if object != nil{
                        self.showAlert(withTitle: "", andMessage: message)
                        _ = self.navigationController?.popViewController(animated: true)
                    }else{
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    let error_as_String = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_as_String)
                }
            })
        }
    }
    
    //MARK: Actions
    @IBAction func action_back(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func action_upload(_ sender: Any) {
        if self.image_upload?.image == nil{
            self.showAlert(withTitle: "", andMessage: "Select One Media To Upload")
        }else {
            self.feedPostModel.message = (self.text_comment?.text)!
            self.saveImage()
        }
    }
    
    @IBAction func action_selectPhoto(_ sender: Any) {
        let alertController = UIAlertController(title: title, message: "Options", preferredStyle: .actionSheet)
        let firstView: UIView? = alertController.view.subviews.first
        let secView: UIView? = firstView?.subviews.first
        secView?.backgroundColor = UIColor(red: CGFloat(0.996), green: CGFloat(0.996), blue: CGFloat(0.980), alpha: CGFloat(1.00))
        secView?.layer.cornerRadius = 3.0
        alertController.view.tintColor = kAlertTintColor
        let photoLib = UIAlertAction(title: "Photo Library", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
            DispatchQueue.main.async(execute: {
                //
                self.photoLibrary()
            })
        })
        
        alertController.addAction(photoLib)
        
        let cam = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
            DispatchQueue.main.async(execute: {
                //
                self.camera()
            })
        })
        
        alertController.addAction(cam)
        
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    //MARK: Delegates
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage //2
        image_upload?.contentMode = .scaleAspectFit //3
        image_upload?.image = chosenImage //4
        dismiss(animated:true, completion: nil) //5
        
    }
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    //image select function
    
    func photoLibrary() {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagePicker, animated: true, completion: nil)
    }
    
    func camera() {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        imagePicker.cameraCaptureMode = .photo
        present(imagePicker, animated: true, completion: nil)
    }
    
    //TestField
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        let rect = textField.superview?.convert(textField.frame, to: self.view)
        self.scrollView?.scrollContentForViewFrame(activeFrame: rect!, andParentView: self.view)
        
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //Location Delegate
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        //        currentCoordinates = location?.coordinate
        let lat = Double((location?.coordinate.latitude)!)
        let long = Double((location?.coordinate.longitude)!)
        
        locationManager.stopUpdatingLocation()
        retrivePlaceName(withCoordinates: (location?.coordinate)!)
        
        print("\(lat),\(long)")
    }
    
    func retrivePlaceName(withCoordinates coordinate: CLLocationCoordinate2D) {
        GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: {(response, error) in
            
            var currentAddress = String()
            
            var addressObj = GMSAddress()
            if let tempaddressObj = response?.firstResult() {
                addressObj = tempaddressObj
            }
            let city = (addressObj.locality != nil) ? addressObj.locality : ((addressObj.subLocality != nil) ? addressObj.subLocality : addressObj.thoroughfare)
            
            let country = addressObj.country
            
            currentAddress = "\(city ?? "")" + "," + "\(country ?? "")"
                
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    self.stopActivity()
                    
                    self.feedPostModel.location = "\(currentAddress)"
                
                })
            })
        }
    
}
