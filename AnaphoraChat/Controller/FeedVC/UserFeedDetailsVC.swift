//
//  UserFeedDetailsVC.swift
//  ACCO
//
//  Created by Tanmoy on 11/07/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit
import SDWebImage

class UserFeedDetailsVC: UIViewController,UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var image_user: UIImageView?
    @IBOutlet weak var label_name: UILabel?
    @IBOutlet weak var label_time: UILabel?
    @IBOutlet weak var image_feed: UIImageView?
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var text_commentbox: UITextField?
    @IBOutlet weak var scrollView: UIScrollView?
    
    //MARK: Models
    var userFeedModel = UserFeedModel()
    
    //MARK: Vars
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.image_user?.sd_setImage(with: URL(string:"\(kBaseUrl)" + "/api/auth/photo/" + userFeedModel.userName), placeholderImage:UIImage(named:"user.png"))
        self.image_feed?.sd_setImage(with: URL(string:"\(kBaseUrl)"  + "\(userFeedModel.imageUrl)"), placeholderImage: UIImage(named:"placeholder.png"))
        print("\(kBaseUrl)" + "\(userFeedModel.imageUrl)")
        self.label_name?.text =  userFeedModel.userName
        self.label_time?.text = getPostDuration(userFeedModel.logTimeStamp)
        //self.label_time?.text = userFeedModel.message
        self.tableView?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: ACTIONS
    @IBAction func action_back(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func action_send(_ sender: Any) {
        if text_commentbox?.text?.trimString() == ""{
            self.showAlert(withTitle: "", andMessage: "You Can't Post Empty Comment")
        }else{
            self.post_comment()
        }
    }
    
    //MARK: Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userFeedModel.commentsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = UserCommentModel(userFeedModel.commentsModel[indexPath.row] as! NSDictionary)
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell") as! CommentCell
        cell.refresh(model: model)
        return cell
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        let rect = textField.superview?.convert(textField.frame, to: self.view)
        scrollView?.scrollContentForViewFrame(activeFrame: rect!, andParentView: self.view)
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    //MARK: API
    private func post_comment(){
        self.startActivity()
        AppServiceManager.commentOn_Post(postID: userFeedModel.postID, message: (self.text_commentbox?.text)!) { (object, message, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                self.stopActivity()
                if error == nil{
                    if object != nil{
                        self.text_commentbox?.text = ""
                        self.text_commentbox?.resignFirstResponder()
                        self.showAlert(withTitle: "", andMessage: message)
                        _ = self.navigationController?.popViewController(animated: true)
                    }else{
                        self.showAlert(withTitle: "", andMessage: message)
                    }
                }else{
                    let error_as_string = error?.localizedDescription
                    self.showAlert(withTitle: "", andMessage: error_as_string)
                }
            })
        }
    }
}
