//
//  ChatTypeVC.swift
//  AnaphoraChat
//
//  Created by Tanmoy on 11/04/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyStoreKit


var shareSecret = "40daa81bf58d405b9a3d1c3b055cfb5f"

enum RegisteredPurchase: String{
    case case1 = "Dollar10"
    case autoRenewable = "autoRenewable"
}
class NetworkActivityIndicatorManager : NSObject{
    private static var loadingCount = 0
    
    class func NetworkOperationStarted() {
        if loadingCount == 0{
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
        }
        loadingCount += 1
    }
    
    class func networkOperationFinished() {
        if loadingCount > 0{
            loadingCount -= 1
            
        }
        if loadingCount == 0{
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
}

class ChatTypeVC: UIViewController {

    //MARK: Outlate
    @IBOutlet weak var button_singlechat: UIButton?
    @IBOutlet weak var button_groupchat: UIButton?
    @IBOutlet weak var button_anonymous: UIButton?
    
    
    //MARK: VAR
    let bundleID = "com.acco.app"
    var case1 = RegisteredPurchase.case1
    var delegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        statusBar.backgroundColor = UIColor.white
        UIApplication.shared.statusBarStyle = .lightContent
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor.black
    }

    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
        self.delegate.socket?.emit("chatoff")
        self.button_singlechat?.layer.cornerRadius = (self.button_singlechat?.frame.height)!/2
        self.button_groupchat?.layer.cornerRadius  =  (self.button_groupchat?.frame.height)!/2
        self.button_anonymous?.layer.cornerRadius = (self.button_anonymous?.frame.height)!/2
        self.button_anonymous?.clipsToBounds = true
        self.button_groupchat?.clipsToBounds = true
        self.button_singlechat?.clipsToBounds = true
    }
    
    @IBAction func searchClicked(_ sender: Any) {
        let SearchVC = storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        self.navigationController?.pushViewController(SearchVC, animated: true)
    }
    @IBAction func notificationClicked(_ sender: Any) {
        let NotificationVC = storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(NotificationVC, animated: true)
    }
    
    @IBAction func appIconClicked(_ sender: Any) {
        let HomeVC = storyboard?.instantiateViewController(withIdentifier: "HomeRootVC") as! HomeRootVC
        self.navigationController?.pushViewController(HomeVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    //MARK: Action
    @IBAction func action_single(_ sender: Any) {
        self.next_vc(type: "p2p")
    }
    
    @IBAction func action_group(_ sender: Any) {
        self.next_vc(type: "group")
    }
    
    @IBAction func action_anon(_ sender: Any) {
        self.next_vc(type: "anon")
    }
    @IBAction func action_inAppPurchase(_ sender: Any) {
        //purchase(purchase: case1)
        let purchaseVC = self.storyboard?.instantiateViewController(withIdentifier: "InAppPurchaseTest") as! InAppPurchaseTest
        self.navigationController?.pushViewController(purchaseVC, animated: true)
    }
    //MARK: Method
    func next_vc(type: String){
        let genderVC = storyboard?.instantiateViewController(withIdentifier: "GenderVC") as! GenderVC
        genderVC.chatType = type
        self.navigationController?.pushViewController(genderVC, animated: true)
    }
    func getInfo(purchase : RegisteredPurchase){
        NetworkActivityIndicatorManager.NetworkOperationStarted()
        //SwiftyStoreKit.retrieveProductsInfo(["1095662508"], completion: { result in
        SwiftyStoreKit.retrieveProductsInfo([bundleID + "." + purchase.rawValue], completion: { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            self.showAlert(alert: self.alertForRetrivalInfo(result: result))
        })
    }
    
    func purchase(purchase : RegisteredPurchase){
        NetworkActivityIndicatorManager.NetworkOperationStarted()
        print("\(bundleID + "." + purchase.rawValue)")
        SwiftyStoreKit.purchaseProduct(bundleID + "." + purchase.rawValue, completion: { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            if case .success(let product) = result {
//                if product.productId == self.bundleID + "." + "Anaphora"{
//                    
//                    print("May be all ok")
//                   
//                }
                if product.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(product.transaction)
                }
                self.showAlert(alert: self.alertForPurchaseResult(result: result))
            }
        })
    }
    
    func restorePurchase(){
        NetworkActivityIndicatorManager.NetworkOperationStarted()
        SwiftyStoreKit.restorePurchases(atomically: true, completion: { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            for product in result.restoredPurchases{
                if product.needsFinishTransaction{
                    SwiftyStoreKit.finishTransaction(product.transaction)
                }
            }
            self.showAlert(alert: self.alertForRestorePurchases(result: result))
        })
    }
    
    func verifyReceipt(){
        NetworkActivityIndicatorManager.NetworkOperationStarted()
        SwiftyStoreKit.verifyReceipt(using: (AppleReceiptValidator.VerifyReceiptURLType.sandbox as! ReceiptValidator), password: shareSecret, completion:  {
            result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            self.showAlert(alert: self.alertForVerifyReceipt(result: result))
            if case .error(let error) = result {
                if case .noReceiptData = error {
                    self.refreshReceipt()
                }
            }
        })
    }
    
    func verifyPurcahse(product : RegisteredPurchase) {
        NetworkActivityIndicatorManager.NetworkOperationStarted()
        SwiftyStoreKit.verifyReceipt(using: (AppleReceiptValidator.VerifyReceiptURLType.sandbox as! ReceiptValidator), password: shareSecret, completion: {
            result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            switch result{
            case .success(let receipt):
                let productID = self.bundleID + "." + product.rawValue
                if product == .autoRenewable {
                    let purchaseResult = SwiftyStoreKit.verifySubscription(type: .autoRenewable, productId: productID, inReceipt: receipt, validUntil: Date())
                    self.showAlert(alert: self.alertForVerifySubscription(result: purchaseResult))
                }
                else {
                    let purchaseResult = SwiftyStoreKit.verifyPurchase(productId: productID, inReceipt: receipt)
                    self.showAlert(alert: self.alertForVerifyPurchase(result: purchaseResult))
                }
            case .error(let error):
                self.showAlert(alert: self.alertForVerifyReceipt(result: result))
                if case .noReceiptData = error {
                    self.refreshReceipt()
                }
            }
        })
    }
    func refreshReceipt() {
        SwiftyStoreKit.refreshReceipt(completion: {
            result in
            self.showAlert(alert: self.alertForRefreshRecepit(result: result))
        })
    }
}

extension ChatTypeVC {
    func alertWithTitle(title: String, message : String) -> UIAlertController{
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    func showAlert(alert : UIAlertController) {
        guard let _ = self.presentationController else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func alertForRetrivalInfo(result : RetrieveResults) -> UIAlertController{
        if let product = result.retrievedProducts.first {
            let priceString = product.localizedPrice!
            return alertWithTitle(title: product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
        }
        else if let invalidProductID = result.invalidProductIDs.first {
            return alertWithTitle(title: "Could not retreive product info", message: "Invalid product identifier: \(invalidProductID)")
        }
        else {
            let errorString = result.error?.localizedDescription ?? "Unknown Error. Please Contact Support"
            return alertWithTitle(title: "Could not retreive product info" , message: errorString)
        }
    }
    
    func alertForPurchaseResult(result : PurchaseResult) -> UIAlertController {
        switch result {
        case .success(let product):
            print("Purchase Succesful: \(product.productId)")
            return alertWithTitle(title: "Thank You", message: "Purchase completed")
        case .error(let error):
            print("Purchase Failed: \(error)")
            switch error.code {
            case .unknown: return alertWithTitle(title: "Purchase failed", message: error.localizedDescription)
            case .clientInvalid: // client is not allowed to issue the request, etc.
                return alertWithTitle(title: "Purchase failed", message: "Not allowed to make the payment")
            case .paymentCancelled: // user cancelled the request, etc.
                return alertWithTitle(title: "Purchase failed", message: "payment is cancelled")
            case .paymentInvalid: // purchase identifier was invalid, etc.
                return alertWithTitle(title: "Purchase failed", message: "The purchase identifier was invalid")
            case .paymentNotAllowed: // this device is not allowed to make the payment
                return alertWithTitle(title: "Purchase failed", message: "The device is not allowed to make the payment")
            case .storeProductNotAvailable: // Product is not available in the current storefront
                return alertWithTitle(title: "Purchase failed", message: "The product is not available in the current storefront")
            case .cloudServicePermissionDenied: // user has not allowed access to cloud service information
                return alertWithTitle(title: "Purchase failed", message: "Access to cloud service information is not allowed")
            case .cloudServiceNetworkConnectionFailed: // the device could not connect to the nework
                return alertWithTitle(title: "Purchase failed", message: "Could not connect to the network")
            case .cloudServiceRevoked: // user has revoked permission to use this cloud service
                return alertWithTitle(title: "Purchase failed", message: "Cloud service was revoked")
            }
        }
    }
    
    func alertForRestorePurchases(result : RestoreResults) -> UIAlertController {
        if result.restoreFailedPurchases.count > 0 {
            print("Restore Failed: \(result.restoreFailedPurchases)")
            return alertWithTitle(title: "Restore failed", message: "Unknown error. Please contact support")
        } else if result.restoredPurchases.count > 0 {
            print("Restore Success: \(result.restoredPurchases)")
            return alertWithTitle(title: "Purchases Restored", message: "All purchases have been restored")
        } else {
            print("Nothing to Restore")
            return alertWithTitle(title: "Nothing to restore", message: "No previous purchases were found")
        }
        
    }
    
    func alertForVerifyReceipt(result: VerifyReceiptResult) -> UIAlertController {
        switch result {
        case.success(let receipt):
            return alertWithTitle(title: "Receipt Verified", message: "Receipt Verified Remotely")
        case .error(let error):
            switch error {
            case .noReceiptData:
                return alertWithTitle(title: "Receipt Verification", message: "No receipt data found, application will try to get a new one. Try Again.")
            default:
                return alertWithTitle(title: "Receipt verification", message: "Receipt Verification failed")
            }
        }
    }
    
    func alertForVerifySubscription(result: VerifySubscriptionResult) -> UIAlertController {
        switch result {
        case .purchased(let expiryDate):
            return alertWithTitle(title: "Product is Purchased", message: "Product is valid until \(expiryDate)")
        case .notPurchased:
            return alertWithTitle(title: "Not purchased", message: "This product has never been purchased")
        case .expired(let expiryDate):
            
            return alertWithTitle(title: "Product Expired", message: "Product is expired since \(expiryDate)")
        }
    }
    
    func alertForVerifyPurchase(result : VerifyPurchaseResult) -> UIAlertController {
        switch result {
        case .purchased:
            return alertWithTitle(title: "Product is Purchased", message: "Product will not expire")
        case .notPurchased:
            return alertWithTitle(title: "Product not purchased", message: "Product has never been purchased")
        }
        
    }
    
    func alertForRefreshRecepit(result : RefreshReceiptResult) -> UIAlertController {        
        switch result {
        case .success(let receiptData):
            return alertWithTitle(title: "Receipt Refreshed", message: "Receipt refreshed successfully")
        case .error(let error):
            return alertWithTitle(title: "Receipt refresh failed", message: "Receipt refresh failed")
        }
    }
}
