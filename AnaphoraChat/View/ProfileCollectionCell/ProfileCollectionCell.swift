//
//  ProfileCollectionCell.swift
//  ACCO
//
//  Created by Tanmoy on 12/07/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var image_posted: UIImageView!
    
    func refresh(model:UserFeedModel){
        let url = "\(kBaseUrl)\(model.imageUrl)"
        self.image_posted?.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named:"placeholder.png"))
    }
    
}
