//
//  HomeCell.swift
//  AnaphoraChat
//
//  Created by subhajit halder on 20/01/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit
import SDWebImage



class HomeCell: UITableViewCell {
    typealias CallBackHandler = (_ object: Any?) -> Void
    
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelLastMessage: UILabel!
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var imageViewOnline: UIImageView!
    @IBOutlet weak var lbl_time: UILabel!
    
    var chatGroupModel = ChatGroupModel()
    var completionHandler:CallBackHandler!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageViewUser.image = UIImage(named: "placeholder.png")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    public func refreshWithData(model: ChatGroupModel, handler: @escaping CallBackHandler) {
        completionHandler = handler
        self.chatGroupModel = model
        if ((model.type as String) == "p2p") {
            print(model.users)
            print(model.user_details)
            var userDict = NSDictionary()
            if ((UserDefaults.standard.object(forKey: "userName") as! String) == model.users[0] as! String){
                userDict = (model.user_details[(model.users[1] as! String)]!) as! NSDictionary
                labelUserName.text = userDict["name"] as? String
//                print(userDict["name"] as! String)
                self.imageViewUser?.sd_setImage(with: URL(string: "\(kBaseUrl)" + "/api/auth/photo/" + "\(model.users[1])"), placeholderImage: UIImage(named: "placeholder.png"))
            }else{
                userDict = (model.user_details[(model.users[0] as! String)]!) as! NSDictionary
                labelUserName.text = userDict["name"] as? String
//                labelUserName.text = (model.users[0] as! String)
                self.imageViewUser?.sd_setImage(with: URL(string: "\(kBaseUrl)" + "/api/auth/photo/" + "\(model.users[0])"), placeholderImage: UIImage(named: "placeholder.png"))
            }
        }
        if ((model.type as String) == "anon") {
            self.imageViewUser.image = UIImage(named: "placeholder.png")
            labelUserName.text = "Anonymous"
        }
        if ((model.type as String) == "group") {
            self.imageViewUser?.sd_setImage(with: URL(string: model.imageURL), placeholderImage: UIImage(named: "placeholder.png"))
            labelUserName.text = model.users.componentsJoined(by: ", ")
        }
        //labelUserName.text = model.users[0] as! String
//        print(model.last_message)
//        print(model.creatorUserName)
//        print(model.last_message_time)
//        print(model.type)
        labelLastMessage.text = "\(model.last_message)"
        self.lbl_time.text = getPostDuration(model.last_message_time)
        imageViewUser.layoutIfNeeded()
        imageViewUser.layer.cornerRadius = imageViewUser.frame.height / 2
    }
    
    @IBAction func action_optionBtn(_ sender: Any) {
        completionHandler(chatGroupModel)
    }

}
