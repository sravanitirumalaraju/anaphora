//
//  UserFeedCell.swift
//  AnaphoraChat
//
//  Created by subhajit halder on 15/02/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation

//MARK: Call Backs
typealias FeedCellHandler = (Any?,String?) -> Void

class UserFeedCell: UITableViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var image_profilePic: UIImageView?
    @IBOutlet weak var label_userName: UILabel?
    @IBOutlet weak var label_timeStamp: UILabel?
    @IBOutlet weak var image_feed: UIImageView?
    @IBOutlet weak var btn_like: UIButton?
    @IBOutlet weak var btn_comment: UIButton?
    @IBOutlet weak var label_message: UILabel?
    @IBOutlet weak var image_love: UIImageView?
    @IBOutlet weak var commentCounter: UILabel!
    @IBOutlet weak var lbl_likeCounter: UILabel?
    
    //MARK: Model
    var userFeedModel =  UserFeedModel()
    //MARK: Vars
    var completionHandler : FeedCellHandler!
    var userName = UserDefaults.standard.object(forKey: "userName") as! String
    var audioPlayer = AVAudioPlayer()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let music = Bundle.main.path(forResource: "tick_Like", ofType: "mp3")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: music! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
            try AVAudioSession.sharedInstance().setActive(true)
        }
        catch{
            print(error)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func refresh(model:UserFeedModel, handler: @escaping FeedCellHandler){
        self.userFeedModel = model
        self.completionHandler = handler
        self.image_profilePic?.sd_setImage(with: URL(string:"\(kBaseUrl)" + "/api/auth/photo/" + model.userName), placeholderImage:UIImage(named:"user.png"))
        self.image_feed?.sd_setImage(with: URL(string:"\(kBaseUrl)"  + "\(model.imageUrl)"), placeholderImage: UIImage(named:"placeholder.png"))
//        print("\(kBaseUrl)" + "\(model.imageUrl)")
        self.label_userName?.text =  model.userName
        self.label_timeStamp?.text = getPostDuration(model.logTimeStamp)
//        print(self.label_timeStamp?.text)
        self.label_message?.text = model.message
        self.image_love?.image = UIImage(named: "heart_darkgrey")
        self.lbl_likeCounter?.text = "\(self.userFeedModel.likesModel.count) likes"
        self.commentCounter.text = "\(self.userFeedModel.commentsModel.count) comments"
//        if self.userFeedModel.likesModel.count == 1{
//            self.lbl_likeCounter?.text = "\(self.userFeedModel.likesModel.count) like"
//        }else{
//            self.lbl_likeCounter?.text = "\(self.userFeedModel.likesModel.count) likes"
//        }
//        if self.userFeedModel.commentsModel.count == 1{
//           self.commentCounter.text = "\(self.userFeedModel.commentsModel.count) comment"
//        }else{
//           self.commentCounter.text = "\(self.userFeedModel.commentsModel.count) comments"
//        }
        
        for i in 0 ..< self.userFeedModel.likesModel.count {
            let like_model = UserLikesModel(self.userFeedModel.likesModel[i] as! NSDictionary)
            if like_model.userName == UserDefaults.standard.object(forKey: "userName") as! String{
                self.image_love?.image = UIImage(named: "heart_select")
                return
            }else{
                self.image_love?.image = UIImage(named: "heart_darkgrey")
            }
        }
    }
    
    @IBAction func action_like(_ sender: Any) {
        audioPlayer.play()
        completionHandler(self.userFeedModel,"like")
    }
    
    @IBAction func action_comment(_ sender: Any) {
        completionHandler(self.userFeedModel,"comment")
    }
    
    @IBAction func action_optionBtn(_ sender: Any) {
        completionHandler(self.userFeedModel, "option")
    }
    
    func timeFormatted(totalms: Int) -> String {
        let minutes: Int = (totalms % 60)
        let hours: Int = (totalms / 60)%24
        let day: Int = totalms / 1440
        return String(format: "%d D %02d H %02d M" + " " + "Ago",day, hours, minutes)
    }
    
}
