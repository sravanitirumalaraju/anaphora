//
//  ExploreCell.swift
//  AnaphoraChat
//
//  Created by Tanmoy on 15/02/17.
//  Copyright © 2017 Tanmoy. All rights reserved.
//

import UIKit
import SDWebImage

typealias CallBackHandler = (_ object: Any?, _ type: String) -> Void

class ExploreCell: UITableViewCell {

    //MARK: Vars
    @IBOutlet weak var label_name: UILabel?
    @IBOutlet weak var image_user: UIImageView?
    @IBOutlet weak var image_1st: UIImageView?
    @IBOutlet weak var image_2nd: UIImageView?
    @IBOutlet weak var image_3rd: UIImageView?
    @IBOutlet weak var aboutMeLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var btn_follow: UIButton!
    
    var exploreModel = ExploreModel()
    var completionHandler:CallBackHandler!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func refresh(model: ExploreModel, handler: @escaping CallBackHandler ){
//        self.image_1st?.image = UIImage(named:"placeholder.png")
//        self.image_2nd?.image = UIImage(named:"placeholder.png")
//        self.image_3rd?.image = UIImage(named:"placeholder.png")
        self.image_1st?.image = UIImage(named:"")
        self.image_2nd?.image = UIImage(named:"")
        self.image_3rd?.image = UIImage(named:"")
        completionHandler = handler
        
        self.exploreModel = model
        
        self.label_name?.text = model.userDetails.name
        
        self.aboutMeLbl?.text = model.userDetails.bio
        self.locationLbl.text = model.userDetails.location
        
        self.image_user?.sd_setImage(with: URL(string:"\(kBaseUrl)" + "/api/auth/photo/" + model.userName), placeholderImage:UIImage(named:"user.png"))
        
        let userFeedArr = NSMutableArray(array: model.userFeed)
        
        if userFeedArr.count >= 1{
            print("name: \(model.userDetails.name) userFeed1: \(userFeedArr[0])")
            self.image_1st?.sd_setImage(with: URL(string:"\(kBaseUrl)" + "\(userFeedArr[0])"), placeholderImage:UIImage(named:"placeholder.png"))
        }
        if userFeedArr.count >= 2 {
            print("name: \(model.userDetails.name) userFeed2: \(userFeedArr[1])")
            self.image_2nd?.sd_setImage(with: URL(string:"\(kBaseUrl)" + "\(userFeedArr[1])"), placeholderImage:UIImage(named:"placeholder.png"))
        }
        if userFeedArr.count >= 3 {
            print("name: \(model.userDetails.name) userFeed3: \(userFeedArr[2])")
            self.image_3rd?.sd_setImage(with: URL(string:"\(kBaseUrl)" + "\(userFeedArr[2])"), placeholderImage:UIImage(named:"placeholder.png"))
        }
        userFeedArr.removeAllObjects()
        if model.connection{
            self.btn_follow.setImage(UIImage(named: "follow"), for: .normal)
//            self.btn_follow.setImage(UIImage(named: "if_icon-minus-circled"), for: .normal)
        }else{
            self.btn_follow.setImage(UIImage(named: "addFollow"), for: .normal)
        }
    }
    
    //MARK: Action 
    //Follow
    @IBAction func action_follow(_ sender: Any) {
        completionHandler(self.exploreModel, "follow")
    }
    
    
    @IBAction func action_btnOnProfilePic(_ sender: Any) {
        completionHandler(self.exploreModel, "navigate")
    }

    @IBAction func action_pic1Btn(_ sender: Any) {
        completionHandler(self.exploreModel, "pic1")
    }
    
    @IBAction func action_pic2Btn(_ sender: Any) {
        completionHandler(self.exploreModel, "pic2")
    }
    
    @IBAction func action_pic3Btn(_ sender: Any) {
        completionHandler(self.exploreModel, "pic3")
    }
    
}
