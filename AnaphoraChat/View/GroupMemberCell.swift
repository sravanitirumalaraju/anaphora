//
//  GroupMemberCell.swift
//  ACCO
//
//  Created by Omnibuz Labs on 06/12/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit
import SDWebImage

class GroupMemberCell: UITableViewCell {

    //MARK: Outlate
    @IBOutlet weak var image_profilePic: UIImageView!
    
    @IBOutlet weak var lbl_userName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.image_profilePic.layer.cornerRadius = (self.image_profilePic.frame.size.height) / 2
        
        self.image_profilePic.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Method
    func fillCell(user: String){
        self.image_profilePic?.sd_setImage(with: URL(string:"\(kBaseUrl)" + "/api/auth/photo/" + user), placeholderImage:UIImage(named:"user.png"))
        
        self.lbl_userName.text = "\(user)"
    }

}
