//
//  ProfileTableviewCell.swift
//  ACCO
//
//  Created by Tanmoy on 12/07/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileTableviewCell: UITableViewCell {

    //MARK: IBOUTLETS
    
    @IBOutlet weak var image_profilePic: UIImageView!
    @IBOutlet weak var label_postedBy: UILabel!
    @IBOutlet weak var label_time: UILabel!
    @IBOutlet weak var image_posted: UIImageView!
    @IBOutlet weak var btn_like: UIButton!
    @IBOutlet weak var btn_comment: UIButton!
    @IBOutlet weak var label_message: UILabel!
    @IBOutlet weak var image_love: UIImageView!

    //MARK: VAR
    typealias FeedCellHandler = (Any?,String) -> Void
    
    var completioHandler : FeedCellHandler!
    var userFeedModel = UserFeedModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func refresh(model:UserFeedModel, handler: @escaping FeedCellHandler){
        
        self.userFeedModel = model
        
        completioHandler = handler
        
        self.label_postedBy?.text = model.userName
        
        let url = "\(kBaseUrl)" + "/api/auth/photo/" + "\(model.userName)"
        
        let photo_url = "\(kBaseUrl)" + "/" + "\(model.imageUrl)"
        
        self.image_posted?.sd_setImage(with: URL(string: photo_url), placeholderImage: UIImage(named:"placeholder.png"))
        
        self.label_time?.text =  getPostDuration(model.logTimeStamp)
        
        self.image_profilePic?.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named:"placeholder.png"))
        
        self.label_message?.text = model.message
        
        for i in 0..<self.userFeedModel.likesModel.count {
            
            let like_model = UserLikesModel(self.userFeedModel.likesModel[i] as! NSDictionary)
            
            if like_model.userName == UserDefaults.standard.object(forKey: "userName") as! String{
                
                let image = UIImage(named: "heart_select") as UIImage!
                
                self.image_love?.image = image
                
                return
            }else{
                
                let image = UIImage(named: "heart_darkgrey") as UIImage!
                
                self.image_love?.image = image
                
                
            }
            
        }
        
        
    }
    
    
    func timeFormatted(totalSeconds: Int) -> String {
        
        let date = NSDate(timeIntervalSince1970: TimeInterval(totalSeconds/1000))
        
        let elapsedTime = NSDate().timeIntervalSince(date as Date)
        
        print(elapsedTime)
        
        return "\(elapsedTime)"
    
    }
    
    //MARK: ACTION
    
    @IBAction func action_optionBtn(_ sender: Any) {
        
        completioHandler(self.userFeedModel, "option")
        
    }
    
    @IBAction func action_likeBtn(_ sender: Any) {
        
        completioHandler(self.userFeedModel, "like")
    }
    
    @IBAction func action_commentBtn(_ sender: Any) {
        
        completioHandler(self.userFeedModel, "comment")
        
    }
    
    
}
