//
//  CellOther.swift
//  AnaphoraChat
//
//  Created by Tanmoy on 12/04/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class CellOther: UITableViewCell {

    @IBOutlet weak var view_chatLayer: UIView?
    @IBOutlet weak var label_name: UILabel?
    @IBOutlet weak var label_message: UILabel?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.view_chatLayer?.layer.cornerRadius = 20
    }
    
    public func refreshWithData(messageModel: MessageModel, chatModel: ChatGroupModel) {
        if chatModel.type != "anon"{
            let dic = chatModel.user_details
            let userModel = dic["\(messageModel.sender)"] as! NSDictionary
            
            self.label_name?.text = "@\(userModel["name"] ?? "")"
        }else{
            self.label_name?.text = "@Anonymous"
        }
        self.label_message?.text = messageModel.message_text
    }

}
