//
//  CellOwn.swift
//  AnaphoraChat
//
//  Created by Tanmoy on 12/04/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class CellOwn: UITableViewCell {
    
    @IBOutlet weak var view_layer: UIView?
    
    @IBOutlet weak var label_message: UILabel?
    @IBOutlet weak var label_name: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
         self.view_layer?.layer.cornerRadius = 20
        
        //self.view_layer?.layer.borderColor = UIColor(red: 215/255, green: 215/255, blue: 215/255, alpha: 1.0).cgColor
        //self.view_layer?.layer.borderWidth = 0.5
        

        // Configure the view for the selected state
    }
    
    public func refreshWithData(messageModel: MessageModel, chatModel: ChatGroupModel) {
        
        let dic = chatModel.user_details
        print(dic)
        let userModel = dic["\(messageModel.sender)"] as! NSDictionary
        
        self.label_message?.text = messageModel.message_text
        
        self.label_name?.text = "@\(userModel["name"] ?? "")"
        
    }

}
