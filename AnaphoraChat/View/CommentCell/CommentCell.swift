//
//  CommentCell.swift
//  ACCO
//
//  Created by Tanmoy on 12/07/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit
import SDWebImage

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var image_user: UIImageView?
    @IBOutlet weak var label_name: UILabel?
    @IBOutlet weak var label_comment: UILabel?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
    func refresh(model:UserCommentModel){
       
        self.label_name?.text = model.userName
        
        self.label_comment?.text = model.message
        
        self.image_user?.sd_setImage(with: URL(string:"\(kBaseUrl)" + "/api/auth/photo/" + "\(model.userName)"), placeholderImage: UIImage(named:"placeholder.png"))
        
        
    }

}
