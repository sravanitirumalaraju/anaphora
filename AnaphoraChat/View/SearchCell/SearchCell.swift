//
//  SearchCell.swift
//  AnaphoraChat
//
//  Created by Tanmoy on 25/05/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
    
    
    //MARK: Outlets
    
    @IBOutlet weak var image_user: UIImageView?
    
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var lbl_about: UILabel!
    
    //MARK: VAR
    typealias CallBackHandler = (_ object: Any?) -> Void
    
    var complitionHandler : CallBackHandler!
    
    var userModel = UserModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.image_user?.layer.cornerRadius = (self.image_user?.frame.height)!/2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    public func refreshCellData(model: UserModel, handler: @escaping CallBackHandler){
        
        complitionHandler = handler
        
        self.userModel = model
        
        self.image_user?.sd_setImage(with: URL(string: "\(kBaseUrl)" + "/api/auth/photo/" + "\(model.userName)"), placeholderImage: UIImage(named: "placeholder.png"))
        
        lbl_userName.text = model.name
        
        lbl_about.text = model.bio
    }

}
