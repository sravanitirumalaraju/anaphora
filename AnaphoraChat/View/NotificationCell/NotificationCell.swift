//
//  NotificationCell.swift
//  AnaphoraChat
//
//  Created by Tanmoy on 25/05/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//
import UIKit
import SDWebImage

typealias ComplesionHandler = (Any?) -> Void
class NotificationCell: UITableViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var image_cellPic: UIImageView!
    @IBOutlet weak var label_notification: UILabel?
    @IBOutlet weak var label_message: UILabel!
    //MARK: VAR
    var callBackHandler : ComplesionHandler!
    var notificationModel = NotificationModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func refresh(model: NotificationModel, handler: @escaping ComplesionHandler){
        DispatchQueue.main.async {
            self.notificationModel = model
            self.callBackHandler = handler
            self.image_cellPic?.sd_setImage(with: URL(string:"\(kBaseUrl)" + "/api/auth/photo/" + model.related_user), placeholderImage:UIImage(named:"user.png"))
            self.image_cellPic.layoutIfNeeded()
            self.image_cellPic.layer.cornerRadius = self.image_cellPic.frame.size.height / 2
            self.label_notification?.text = model.title
            self.label_message.text = model.message
            print(model.message)
            print(model.imageUrl)
//            print(model.userNameReceiver)
        }
    }
    
    @IBAction func action_btnOnTopOfImg(_ sender: Any) {
        callBackHandler(self.notificationModel)
    }
}
