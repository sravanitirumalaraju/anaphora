//
//  SHTabBarController.swift
//
//
//  Created by Tanmoy Khanra on 23/12/16.
//  Copyright © 2016 Tanmoy. All rights reserved.
//

import UIKit

class SHTabBarController: UITabBarController {
    
    var arrayOfControllers : NSMutableArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrayOfControllers = NSMutableArray.init(capacity: 5)
        let storyboard = UIStoryboard.init(name: "Main", bundle: .main)
        
        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeRootVC") as! HomeRootVC
        let mapVC = storyboard.instantiateViewController(withIdentifier: "MapVC") as! MapVC
        let messageVC = storyboard.instantiateViewController(withIdentifier: "ChatTypeVC") as! ChatTypeVC
        let feedVC = storyboard.instantiateViewController(withIdentifier: "FeedVC") as! FeedVC
        let profileVC = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//        let profileVC = storyboard.instantiateViewController(withIdentifier: "ProfileVC")
        profileVC.userName = UserDefaults.standard.object(forKey: "userName") as! String
        
        let nav1 = UINavigationController.init(rootViewController: homeVC)
        let nav2 = UINavigationController.init(rootViewController: mapVC)
        let nav3 = UINavigationController.init(rootViewController: messageVC)
        let nav4 = UINavigationController.init(rootViewController: feedVC)
        let nav5 = UINavigationController.init(rootViewController: profileVC)
        
        nav1.setNavigationBarHidden(true, animated: false)
        nav2.setNavigationBarHidden(true, animated: false)
        nav3.setNavigationBarHidden(true, animated: false)
        nav4.setNavigationBarHidden(true, animated: false)
        nav5.setNavigationBarHidden(true, animated: false)
        
        arrayOfControllers = NSMutableArray.init()
        arrayOfControllers.add(nav1)
        arrayOfControllers.add(nav2)
        arrayOfControllers.add(nav3)
        arrayOfControllers.add(nav4)
        arrayOfControllers.add(nav5)
        
        self.viewControllers = (NSArray.init(array: arrayOfControllers) as! [UIViewController])
        self.view.autoresizingMask = .flexibleHeight
        
        UITabBar.appearance().barTintColor = UIColor.white
        UITabBar.appearance().tintColor = UIColor.black
        UITabBar.appearance().isTranslucent = false
        let tabBar = self.tabBar
        let tabBarItem1 = tabBar.items?[0]
        let tabBarItem2 = tabBar.items?[1]
        let tabBarItem3 = tabBar.items?[2]
        let tabBarItem4 = tabBar.items?[3]
        let tabBarItem5 = tabBar.items?[4]
        
        tabBarItem1?.image = UIImage(named: "homeBlack")
        tabBarItem2?.image = UIImage(named: "compassBlack")
        tabBarItem3?.image = UIImage(named: "msgBlack")
        tabBarItem4?.image = UIImage(named: "feed")
        tabBarItem5?.image = UIImage(named: "accountBlack")
        
        tabBarItem1?.selectedImage = UIImage(named: "homeBlack")
        tabBarItem2?.selectedImage = UIImage(named: "compassBlack")
        tabBarItem3?.selectedImage = UIImage(named: "msgBlack")
        tabBarItem4?.selectedImage = UIImage(named: "feed")
        tabBarItem5?.selectedImage = UIImage(named: "accountBlack")
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        let onceExec: () = {
//           self.selectedIndex = 1
//        }()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
