
//
//  NSString+Utility.swift
//  Late Lateef
//
//  Created by subhajit halder on 21/01/17.
//  Copyright © 2017 Tanmoy. All rights reserved.
//

import Foundation

extension String {
    func trimString() -> String {
        var trimmedStr: String = ""
        var str_Trimmed: String = (self as NSString).replacingCharacters(in: (self as NSString).range(of:"^\\s*", options: .regularExpression), with: "")
        if str_Trimmed.characters.count > 0 {
            trimmedStr = (str_Trimmed as NSString).replacingCharacters(in: (str_Trimmed as NSString).range(of:"\\s*$", options: .regularExpression), with: "")
        }
        return trimmedStr
    }
    
    
    func index(of string: String, options: String.CompareOptions = .literal) -> String.Index? {
        return range(of: string, options: options)?.lowerBound
    }
    
    func indexes(of string: String, options: String.CompareOptions = .literal) -> [String.Index] {
        var result: [String.Index] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range.lowerBound)
            start = range.upperBound
        }
        return result
    }
    func ranges(of string: String, options: String.CompareOptions = .literal) -> [Range<String.Index>] {
        var result: [Range<String.Index>] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range)
            start = range.upperBound
        }
        return result
    }
    
    
    
}

extension Character
{
    func unicodeScalar() -> UnicodeScalar
    {
        let characterString = String(self)
        let scalars = characterString.unicodeScalars
        
        return scalars[scalars.startIndex]
    }
}



extension TimeInterval {
    func timeIntervalAsString(_ format : String = "dd days, hh hours, mm minutes, ss seconds, sss ms") -> String {
        var asInt   = NSInteger(self)
        let ago = (asInt < 0)
        if (ago) {
            asInt = -asInt
        }
        let ms = Int(self.truncatingRemainder(dividingBy: 1) * (ago ? -1000 : 1000))
        let s = asInt % 60
        let m = (asInt / 60) % 60
        let h = ((asInt / 3600))%24
        let d = (asInt / 86400)
        
        var value = format
        value = value.replacingOccurrences(of: "hh", with: String(format: "%0.2d", h))
        value = value.replacingOccurrences(of: "mm",  with: String(format: "%0.2d", m))
        value = value.replacingOccurrences(of: "sss", with: String(format: "%0.3d", ms))
        value = value.replacingOccurrences(of: "ss",  with: String(format: "%0.2d", s))
        value = value.replacingOccurrences(of: "dd",  with: String(format: "%d", d))
        if (ago) {
            value += " ago"
        }
        return value
    }
    
}

func isValidEmail(testStr:String) -> Bool {
    print("validate emilId: \(testStr)")
    let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let result = emailTest.evaluate(with: testStr)
    return result
}

func getPostDuration(_ campEpochTime: String) -> String {
    
    
    let returnTime: String
    let campEpochSeconds: TimeInterval = Double("\(campEpochTime)")!/1000
    
    
    let date = Date()
    let nowEpochSeconds: TimeInterval = date.timeIntervalSince1970
    let differenceTime: Double = nowEpochSeconds - campEpochSeconds
    let postDate = Date(timeIntervalSince1970: campEpochSeconds)
    let postdateFormat = DateFormatter()
    postdateFormat.dateFormat = "dd MMM, yyyy"
    let stringToday: String = postdateFormat.string(from: date)
    let stringPostDate: String = postdateFormat.string(from: postDate)
    
    if differenceTime < 60 {
        return "\(Int(differenceTime)) secs ago"
    }
    else if (differenceTime / 60) < 60 {
        return "\(Int(differenceTime / 60)) mins ago"
    }
    else if ((differenceTime / 60) / 60) < 2 {
        return "1 hour ago"
    }
    else if ((differenceTime / 60) / 60) < 24 {
        if (stringToday == stringPostDate) {
            return "\(Int((differenceTime / 60) / 60)) hours ago"
        }
        else {
            return "Yesterday"
        }
        //        return [NSString stringWithFormat:@"%d hours ago",(int)((differenceTime / 60) / 60)];
    }
    else if (((differenceTime / 60) / 60) / 24) < 2 {
        return "Yesterday"
    }
    else {
        let postEpoch: TimeInterval = campEpochSeconds
        let postDate = Date(timeIntervalSince1970: postEpoch)
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM, dd, yyyy"
        return "\(dateFormat.string(from: postDate))"
    }
    
    
    //    else if((((differenceTime / 60) / 60) / 24) < 30)
    //    {
    //        return [NSString stringWithFormat:@"%d days ago", (int)(((differenceTime / 60) / 60) / 24)];
    //    }
    //    else if (((((differenceTime / 60) / 60) / 24) / 30) < 2)
    //    {
    //        return [NSString stringWithFormat:@"1 month ago"];
    //    }
    //    else if (((((differenceTime / 60) / 60) / 24) / 30) < 12)
    //    {
    //        return [NSString stringWithFormat:@"%d months ago",(int)((((differenceTime / 60) / 60) / 24) / 30)];
    //    }
    //    else if ((((((differenceTime / 60) / 60) / 24) / 30) / 12) < 2)
    //    {
    //        return [NSString stringWithFormat:@"1 year ago"];
    //    }
    //    else
    //    {
    //        return [NSString stringWithFormat:@"%d years ago",(int)(((((differenceTime / 60) / 60) / 24) / 30) / 12)];
    //    }
    return returnTime
}


