//
//  NSDictionary+Utility.swift
//  DNADemo
//
//  Created by subhajit halder on 21/02/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import Foundation

extension NSDictionary {
    
    func toStringFrom(key akey: String) -> String {
        if let str = self[akey] as? String {
            return str
        } else {
            return ""
        }
    }
    
    func toNSArrayFrom(key aKey: String) -> NSArray {
        if let arr = self[aKey] as? NSArray {
            return arr
        } else {
            return []
        }
    }
    
    func toBoolFrom(key akey: String) -> Bool {
        if let bool = self[akey] as? Bool {
            return bool
        } else {
            return false
        }
    }
    
    func toNSDictionaryFrom(key aKey: String) -> NSDictionary {
        if let dic = self[aKey] as? NSDictionary {
            return dic
        } else {
            return [:]
        }
    }
//    func toDictionaryFrom(key: String) -> [String: Any]? {
//        if let data = text.data(using: .utf8) {
//            do {
//                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
//            } catch {
//                print(error.localizedDescription)
//            }
//        }
//        return nil
//    }
    
    
}
