//
//  SHJsonSerialization.swift
//  Late Lateef
//
//  Created by subhajit halder on 18/01/17.
//  Copyright © 2017 Tanmoy. All rights reserved.
//

import UIKit

class SHJsonSerialization: NSObject {

    typealias SHJsonSerializationHandler = ( _ json: NSDictionary?, _ error: Error?) -> Void
    
    public class func getJsonResponseFrom(_ responseData: NSData!, handler: SHJsonSerializationHandler) {
            if (responseData != nil) {
                var jsonParserError: Error?
                guard let json = SHJsonSerialization.nsdataToJSON(responseData, error: &jsonParserError!) else {
                    return
                }
                if jsonParserError != nil {
                    handler(nil, jsonParserError)
                }
                else {
                    handler((json as! NSDictionary) , nil)
                }
            }
            else {
                let responseError = NSError(domain: "com.regor.serviceresponsenil", code: 2017, userInfo: [NSLocalizedDescriptionKey : NSLocalizedString("No response data arrived", comment: "")])
                handler(nil, responseError)
            }
        
       
       
    }    
    
   class func nsdataToJSON(_ data: NSData, error: inout Error) -> Any? {
        
        do {
            return try JSONSerialization.jsonObject(with: data as Data, options: .mutableLeaves)
        } catch let myJSONError {
            error = myJSONError
        }
    
        return nil
    }
    
    
    public class func jsonString(fromDictionary dict: NSDictionary) -> String {
        var jsonError: Error?
//        var jsonData: Data? = JSONSerialization.data(withJSONObject: dict, options: (JSONSerialization.WritingOptions), error)
        let jsonData : Any?
        do {
            jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        } catch let error as NSError {
            jsonError = error
            jsonData = nil
            print("\(jsonError)")
        } catch {
            fatalError()
        }
//        if error != nil {
//            return "{}"
//        }
//        return String( jsonData, encoding: String.Encoding.utf8)
        return String(data: jsonData as! Data, encoding: .utf8)!
    }
    
//    public class func jsonToData(_ json: NSDictionary) -> String? {
//        do {
//            return try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted) as? String
//        } catch let myDataError {
////            error = myDataError
//            print("\(myDataError)")
//        }
//        return nil
//    }
    
}


