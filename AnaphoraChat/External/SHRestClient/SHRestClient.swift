//
//  SHRestClient.swift
//  Late Lateef
//
//  Created by subhajit halder on 19/01/17.
//  Copyright © 2017 Subhajit Halder. All rights reserved.
//

import UIKit

class SHRestClient: NSObject {
    
    typealias SHHTTPResponseHandler = (_ data: NSDictionary?,_ error: Error?) -> Void
    var completionHandler : SHHTTPResponseHandler!
    
    var httpMethod: String = ""
    var httpBody: String = ""
    var urlString: String = ""
    var contentType: String = ""
    var authorizationValue: String = ""
    var headerFieldsAndValues : NSDictionary?
    var recievedData : NSMutableData?
    
    convenience init(withUrl url: String!, Body body: String?, Method method: String?, Headers headerFieldsAndValues: NSDictionary?) {
        self.init()
        
        self.httpBody = body!
        self.urlString = url
        self.httpMethod = method!
        
        if headerFieldsAndValues != nil {
            self.headerFieldsAndValues = headerFieldsAndValues
        }
    }
    
    // MARK: get/delete method
    public func getJsonData(_ httpResponseHandler: @escaping SHHTTPResponseHandler) {
        if !self.urlString.isEmpty {
            guard let url = URL(string: self.urlString) else {
                print("Error: cannot create URL")
                return
            }
            
            let defaultConfig = URLSessionConfiguration.default
            let defaultSession = URLSession(configuration: defaultConfig)
            
            var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval:30.0)
            request.httpMethod = self.httpMethod
            request.httpBody = !self.httpBody.isEmpty ? self.httpBody.data(using: .utf8) : "".data(using: String.Encoding.utf8)
            
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            self.headerFieldsAndValues?.enumerateKeysAndObjects({ (key, value, stop) -> Void in
                request.setValue(value as! NSString as String, forHTTPHeaderField: key as! NSString as String)
                
                request.setValue(value as! NSString as String, forHTTPHeaderField: key as! NSString as String)
            })
            
            let token = UserDefaults.standard.object(forKey: "token") as! String?
            
            request.setValue(!self.authorizationValue.isEmpty ? self.authorizationValue : token, forHTTPHeaderField: "Authorization")
            
            let task = defaultSession.dataTask(with: request, completionHandler: { (data, response, error) in
                // do stuff with response, data & error here
                
                //            let httpResponse = response as? HTTPURLResponse
                if error == nil {
                    
                    guard let responseData = data else {
                        print("Error: did not receive data")
                        return
                    }
                    do {
                        guard let json = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                            print("error trying to convert data to JSON")
                            return
                        }
                        httpResponseHandler(json as NSDictionary?, nil)
                        // ...
                    } catch  {
                        print("error trying to convert data to JSON")
                        let jsonError = NSError(domain: "com.regor.serviceresponsenil", code: 2017, userInfo: [NSLocalizedDescriptionKey : NSLocalizedString("error trying to convert data to JSON", comment: "")])
                        httpResponseHandler(nil, jsonError)
                        return
                    }
                }
                else {
                    httpResponseHandler(nil, error)
                }
                
                
            })
            task.resume()
        }
    }
    
    
    // MARK: fetch Method
    public func sendParamswithCompletion(_ httpResponseHandler: @escaping SHHTTPResponseHandler) {
        
        guard let url = URL(string: self.urlString) else {
            print("Error: cannot create URL")
            return
        }
        let defaultConfig = URLSessionConfiguration.default
        
        let defaultSession = URLSession(configuration: defaultConfig)
        
        var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 30.0)
        
        request.httpMethod = self.httpMethod
        
        request.httpBody = !self.httpBody.isEmpty ? self.httpBody.data(using: .utf8) : "".data(using: String.Encoding.utf8)
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        let token = UserDefaults.standard.object(forKey: "token") as! String?
        
        request.setValue(!self.authorizationValue.isEmpty ? self.authorizationValue : token, forHTTPHeaderField: "Authorization")
        
        
        
        self.headerFieldsAndValues?.enumerateKeysAndObjects({ (key, value, stop) -> Void in
            request.setValue(value as! NSString as String, forHTTPHeaderField: key as! NSString as String)
        })
        
        //request.setValue(!self.authorizationValue.isEmpty ? self.authorizationValue : "Basic YXBwOmNvdmVyZWQ=", forHTTPHeaderField: "Authorization")
        //request.setValue(!self.authorizationValue.isEmpty ? self.authorizationValue : "EiBBnmUUwQ7hhzi81ZFjzEaDJ1mSRTqKm6PSxg8oJHvN69STYU", forHTTPHeaderField: "CLIENTSECRET")
        
        let task = defaultSession.dataTask(with: request, completionHandler: { (data, response, error) in
            // do stuff with response, data & error here
            
            //            let httpResponse = response as? HTTPURLResponse
            if error == nil {
                
                guard let responseData = data else {
                    print("Error: did not receive data")
                    return
                }
                do {
                    guard let json = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                        print("error trying to convert data to JSON")
                        return
                    }
                    httpResponseHandler(json as NSDictionary?, nil)
                    // ...
                } catch  {
                    print("error trying to convert data to JSON")
                    let jsonError = NSError(domain: "com.regor.serviceresponsenil", code: 2017, userInfo: [NSLocalizedDescriptionKey : NSLocalizedString("error trying to convert data to JSON", comment: "")])
                    httpResponseHandler(nil, jsonError)
                    return
                }
            }
            else {
                httpResponseHandler(nil, error)
            }
            
            
        })
        task.resume()
        
        
    }
    
    // MARK: Single Image upload
    
    public func saveData(withParams data: Data, withFileName fileName: String, fileKey key: String, otherData params: NSDictionary, withCompletion httpResponseHandler: @escaping SHHTTPResponseHandler) {
        let boundary: String = "14737809831466499882746641449"
        let defaultConfigObject = URLSessionConfiguration.default
        
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: OperationQueue.main)
        
        var request = URLRequest(url: URL(string: self.urlString)!, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 30.0)
        
        request.httpMethod = self.httpMethod
        
        let contentType: String = "multipart/form-data; boundary=\(boundary)"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        
        let token = UserDefaults.standard.object(forKey: "token") as! String?
        
        request.setValue(!self.authorizationValue.isEmpty ? self.authorizationValue : token, forHTTPHeaderField: "Authorization")
        
        var body = Data()
        for (param, _) in params {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(param as! String)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(String(describing: params[param] as? String))\r\n".data(using: String.Encoding.utf8)!)
        }
        
        body.append("\r\n--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"\(key)\"; filename=\"\(fileName)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: application/octet-stream\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(Data(data))
        body.append("\r\n--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        request.httpBody = body
        
        let dataTask = defaultSession.uploadTask(with: request as URLRequest, from: body, completionHandler: {( data,  response, error) in
            
            
            if error == nil {
                guard let responseData = data else {
                    print("Error: did not receive data")
                    return
                }
                do {
                    guard let json = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                        print("error trying to convert data to JSON")
                        return
                    }
                    httpResponseHandler(json as NSDictionary?, nil)
                    
                } catch  {
                    let jsonError = NSError(domain: "com.regor.serviceresponsenil", code: 2017, userInfo: [NSLocalizedDescriptionKey : NSLocalizedString("error trying to convert data to JSON", comment: "")])
                    httpResponseHandler(nil, jsonError)
                    return
                }
            }
            else {
                httpResponseHandler(nil, error)
            }
            
            
        })
        
        dataTask.resume()
    }
    
}
