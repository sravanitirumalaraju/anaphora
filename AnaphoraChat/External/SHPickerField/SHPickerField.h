//
//  SHPickerField.h
//  Odyssey
//
//  Created by subhajit halder on 17/09/16.
//  Copyright © 2016 SubhajitHalder. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSUInteger, SHPickerType) {
    SHPickerTypeDefault,
    SHPickerTypeDate,
    SHPickerTypeDateAndTime,
    SHPickerTypeTime,
    SHPickerTypeMultipleSelection
};


typedef NS_ENUM(NSUInteger, SHPickerAction) {
    SHPickerActionDidBeginEditing,
    SHPickerActionDidEndEditing,
    SHPickerActionDateTimeSelected,
    SHPickerActionPickerDidSelectRow,
    SHPickerActionPickerDone,
    SHPickerActionPickerCancel
};


typedef void (^SHTextFieldActionCompletion) (NSString  * _Nonnull textFieldText, SHPickerAction  actionID);

@interface SHPickerField : UITextField {
    SHTextFieldActionCompletion completionHandler;
}

/*!
 * @brief Always Required as it will switch the picker type.
 */@property (nonatomic) SHPickerType pickerType;

/*!
 * @brief To set NSDateFormatter with format. Provide only date format string e.g.: dateField.dateFormat = \@"MM - dd - yyyy".
 */@property (nonatomic, nonnull) NSString * dateFormat;

/*!
 * @brief Used only for Default Picker. Use this for custom data in Picker.
 */@property (nonatomic, nullable) NSArray *dataSource;

/*!
 * @brief ToolBar Color, (Deafault / Dark).
 */@property (nonatomic) UIBarStyle toolbarStyle;

/*!
 * @brief Set maximum date or minimum date for date picker (Cannot be used in Default Picker).
 */@property (nonatomic, nullable) NSDate *minimumDate, *maximumDate;

/*!
 * @brief Picker Color
 */@property (nonatomic, nullable) UIColor *pickerBackgroundColor;

/*!
 * @brief Picker Color
 */@property (nonatomic, nullable) UIColor *pickerTintColor;



/*!
 * @brief Picker ToolBar Color
 */@property (nonatomic, nullable) UIColor *pickerToolBarColor;


/*!
 * @brief Picker ToolBar Color
 */@property (nonatomic, nullable) UIColor *pickerToolBarItemColor;


/*!
 * @brief Picker Toolbar translucent YES or NO.
 */@property (nonatomic) BOOL pickerToolBarTranslucent;




/*!
 * @brief Multi Select Table Cell (If not provided will use default)
 */@property (nonatomic, nullable) UITableViewCell *multiSelectionCell;


/*!
 * @brief Multi Selection Table Cell identifier (Pass custom cell identifier)
 */@property (nonatomic, nullable) NSString *multiSelectCellIdentifier;

/*!
 * @brief Multi Select Result Seperation String (Pass Seperation String for result)
 */@property (nonatomic, nullable) NSString *multiSelectResultSeperationString;


/*!
 * @brief Multi Select Array Multi Table Data Source
 */@property (nonatomic, nullable) NSMutableArray *arrayMultiTableDataSource;



/*!
 * @discussion Returns Data whenever a ction performed
 * @param handler Returns textField Data and action ID
 */
- (void)actionCompletedInPicker:(_Nonnull SHTextFieldActionCompletion)handler;

/*!
 * @discussion Reload Table for multi select if Using Multi Select Table picker
 */
- (void)multiTableReloadData;


@end
