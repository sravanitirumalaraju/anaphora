//
//  UserCommentModel.swift
//  ACCO
//
//  Created by Tanmoy on 29/06/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class UserCommentModel: NSObject {
    
    var id = "_id"
    
    var updatedAt = "updatedAt"
    
    var createdAt = "createdAt"
    
    var postID = "postID"
    
    var userName = "userName"
    
    var message = "message"
    
    var logTimeStamp = "logTimeStamp"
    
    override init() {
        super.init()
    }
    
    convenience init(_ dictionary:NSDictionary){
        
        self.init()
        
        self.id  =  dictionary.toStringFrom(key: "_id")
        
        self.updatedAt = dictionary.toStringFrom(key: "updatedAt")
        
        self.createdAt = dictionary.toStringFrom(key: "createdAt")
        
        self.postID = dictionary.toStringFrom(key: "postID")
        
        self.userName = dictionary.toStringFrom(key: "userName")
        
        self.message = dictionary.toStringFrom(key: "message")
        
        self.logTimeStamp = dictionary.toStringFrom(key: "logTimeStamp")
        
    }

}


/*
 "_id": "58ed59ff09fcdd077502dc98",
 "updatedAt": "2017-04-11T22:34:39.788Z",
 "createdAt": "2017-04-11T22:34:39.788Z",
 "postID": "58ed58e9e33ede07306cc402",
 "userName": "pierce",
 "message": "Sed varius id mi tempor vulputate",
 "logTimeStamp": "1491950079786",
 "__v": 0
 
 */
