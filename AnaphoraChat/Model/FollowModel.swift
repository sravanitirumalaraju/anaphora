//
//  FollowModel.swift
//  ACCO
//
//  Created by Tanmoy Khanra on 19/09/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class FollowModel: NSObject {
    
    var updatedAt = ""
    
    var createdAt = ""
    
    var myUserName = ""
    
    var iAmFollowingUserName = ""
    
    var followRequestAccepted = ""
    
    override init() {
        super.init()
    }
    
    convenience init(_ dictionary: NSDictionary) {
        self.init()
        
        self.updatedAt = dictionary.toStringFrom(key: "updatedAt")
        
        self.createdAt = dictionary.toStringFrom(key: "createdAt")
        
        self.myUserName = dictionary.toStringFrom(key: "myUserName")
        
        self.iAmFollowingUserName = dictionary.toStringFrom(key: "iAmFollowingUserName")
        
        self.followRequestAccepted = dictionary.toStringFrom(key: "followRequestAccepted")
    }

}



/*
 
 {
 "response": {
 "success": true,
 "message": "Saved your new connection",
 "data": {
 "__v": 0,
 "updatedAt": "2017-04-12T13:36:32.683Z",
 "createdAt": "2017-04-12T13:36:32.683Z",
 "myUserName": "perma",
 "iAmFollowingUserName": "pierce",
 "followTimeStamp": "1492004192682",
 "followRequestAccepted": "1",
 "_id": "58ee2d6011abad05bafb51c3"
 }
 }
 }

 
 
 */
