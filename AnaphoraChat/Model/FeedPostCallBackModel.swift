//
//  FeedPostCallBackModel.swift
//  ACCO
//
//  Created by Tanmoy on 11/07/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class FeedPostCallBackModel: NSObject {
    
    var userName = ""
    
    var postID = ""
    
    var message = ""
    
    var imageUrl = ""
    
    var logTimeStamp = ""
    
    var userLocation = ""
    
    override init() {
        super.init()
    }
    
    convenience init(_ dictionary: NSDictionary) {
        self.init()
        
        self.userName = dictionary.toStringFrom(key: "userName")
        
        self.message =  dictionary.toStringFrom(key: "message")
        
        self.imageUrl = dictionary.toStringFrom(key: "imageUrl")
        
        self.logTimeStamp = dictionary.toStringFrom(key: "logTimeStamp")
        
        self.userLocation = dictionary.toStringFrom(key: "userLocation")
        
    }

}

/*
 
 "response": {
 "success": true,
 "message": "Saved your post",
 "data": {
 "__v": 0,
 "updatedAt": "2017-04-11T22:30:01.803Z",
 "createdAt": "2017-04-11T22:30:01.803Z",
 "postID": "POST1491949801794",
 "userName": "perma",
 "message": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur a molestie nisi, vel ullamcorper quam. In in nulla dolor. Pellentesque nulla felis, pellentesque sit amet vestibulum ut, dapibus quis odio.",
 "imageUrl": "/feedpic/2354353654.jpg",
 "logTimeStamp": "1491949801794",
 "userLocation": "Bhawanipur, Kolkata",
 "reported": "0",
 "_id": "58ed58e9e33ede07306cc402"
 }
 }
 
 */
