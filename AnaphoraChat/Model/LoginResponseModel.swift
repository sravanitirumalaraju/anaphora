//
//  LoginResponseModel.swift
//  DNADemo
//
//  Created by subhajit halder on 13/02/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit

class LoginResponseModel: NSObject {

    var userId : String = ""
    var contactPerson : String = ""
    var companyName : String = ""
    var securityToken : String = ""
    var email : String = ""
    var status : String = ""
    
    convenience init(with dict: NSDictionary) {
        self.init()
        status = dict["status"] as! String
        userId = dict["user_id"] as! String
        contactPerson = dict["contact_person"] as! String
        companyName = dict["companyName"] as! String
        securityToken = dict["security_token"] as! String
        email = dict["email"] as! String
    }
    
}

/*
{
    "status": "success",
    "remark": "User Credentials Verified",
    "user_id": "62",
    "contact_person": "ssss",
    "companyName": "",
    "security_token": "AixWlgDWp5",
    "email": ""
}*/
