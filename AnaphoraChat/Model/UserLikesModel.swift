//
//  UserLikesModel.swift
//  ACCO
//
//  Created by Tanmoy on 29/06/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class UserLikesModel: NSObject {
    
    var id = ""
    
    var updatedAt = ""
    
    var createdAt = ""
    
    var postID = ""
    
    var userName = ""
    
    var likedOn = ""
    
    
    override init(){
        
        super.init()
    }
   
    convenience init(_ dictionary:NSDictionary){
        
        self.init()
        
        self.id  = dictionary.toStringFrom(key: "_id")
        
        self.updatedAt = dictionary.toStringFrom(key: "updatedAt")
        
        self.createdAt = dictionary.toStringFrom(key: "createdAt")
        
        self.postID = dictionary.toStringFrom(key: "postID")
        
        self.userName = dictionary.toStringFrom(key: "userName")
        
        self.likedOn = dictionary.toStringFrom(key: "likedOn")
        
    }
    
    

}


/*
 
 "_id": "58ed5c432c0a5f086ab0c462",
 "updatedAt": "2017-04-11T22:44:19.607Z",
 "createdAt": "2017-04-11T22:44:19.607Z",
 "postID": "58ed58e9e33ede07306cc402",
 "userName": "pierce",
 "likedOn": "1491950659600",
 "__v": 0
 
 */
