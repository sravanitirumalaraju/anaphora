//
//  RecommendModel.swift
//  ACCO
//
//  Created by Omnibuz Labs on 08/11/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class RecommendModel: NSObject {

    var __v = ""
    var updatedAt = ""
    var createdAt = ""
    var userNameRecommended = ""
    var userName = ""
    var recommendedOn = ""
    var id = ""
    
    override init() {
        super.init()
    }
    
    convenience init(_ dictionary: NSDictionary) {
        self.init()
        
        
        self.updatedAt = dictionary.toStringFrom(key: "updatedAt")
        self.createdAt = dictionary.toStringFrom(key: "createdAt")
        self.userNameRecommended = dictionary.toStringFrom(key: "userNameRecommended")
        self.userName = dictionary.toStringFrom(key: "userName")
        self.recommendedOn = dictionary.toStringFrom(key: "recommendedOn")
        self.id = dictionary.toStringFrom(key: "_id")
        
        
    }
}
