//
//  UserModel.swift
//  AnaphoraChat
//
//  Created by subhajit halder on 22/02/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit

class UserModel: NSObject {

    var userName = ""
    var name = ""
    var profilePic = ""
    var email = ""
    var dob = ""
    var gender = ""
    var phNo = ""
    var age = ""
    var location = ""
    var latLong:NSArray = []
    var hobbies = ""
    var bio = ""
    var timeZone = ""
    var private_mode = Bool()
    
    convenience init(dict: NSDictionary) {
        self.init()
        userName = dict.toStringFrom(key: "userName")
        name = dict.toStringFrom(key: "name")
        profilePic = dict.toStringFrom(key: "profilePic")
        email = dict.toStringFrom(key: "email")
        dob = dict.toStringFrom(key: "dob")
        gender = dict.toStringFrom(key: "gender")
        phNo = dict.toStringFrom(key: "phNo")
        age = dict.toStringFrom(key: "age")
        location = dict.toStringFrom(key: "location")
        latLong = dict.toNSArrayFrom(key: "lat_lng")
        hobbies = dict.toStringFrom(key: "hobbies")
        bio = dict.toStringFrom(key: "bio")
        timeZone = dict.toStringFrom(key: "timeZone")
        private_mode = dict.toBoolFrom(key: "private_mode")
        
    }
}

/*"data": {
    "_id": "58d961ad653ba20a9f33e447",
    "updatedAt": "2017-03-27T19:02:05.361Z",
    "createdAt": "2017-03-27T19:02:05.361Z",
    "userName": "perry",
    "name": "Perry",
    "profilePic": "/profilepic/prof_1490640748402.jpg",
    "email": "micheal.perry@yahoo.com",
    "dob": "",
    "gender": "",
    "phNo": "+919856734684",
    "age": "",
    "location": "Kolkata",
    "lat_lng": [
    22.595456,
    88.3838633
    ],
    "hobbies": "",
    "bio": "",
    "timeZone": "+05:30",
    "__v": 0
},
"apiKey": "baJlFZkO5PrJV0xJzQp6Wg=="
}*/
