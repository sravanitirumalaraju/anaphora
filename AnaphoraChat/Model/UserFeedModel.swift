//
//  UserFeedModel.swift
//  ACCO
//
//  Created by Tanmoy on 29/06/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class UserFeedModel: NSObject {
    
    var userLocation = ""
    
    var logTimeStamp = ""
    
    var imageUrl = ""
    
    var message = ""
    
    var userName = ""
    
    var postID = ""
    
    var createdAt = ""
    
    var  id = ""
    
    var likesModel = NSArray()
    
    var commentsModel = NSArray()
    
    override init() {
        super.init()
    }
    
    convenience init(_ dictionary: NSDictionary) {
        self.init()
        
        self.userLocation = dictionary.toStringFrom(key: "userLocation")
        
        self.logTimeStamp = dictionary.toStringFrom(key: "logTimeStamp")
        
        self.imageUrl = dictionary.toStringFrom(key: "imageUrl")
        
        self.message = dictionary.toStringFrom(key: "message")
        
        self.userName = dictionary.toStringFrom(key: "userName")
        
        self.postID = dictionary.toStringFrom(key: "postID")
        
        self.createdAt = dictionary.toStringFrom(key: "createdAt")
        
        self.id = dictionary.toStringFrom(key: "id")
        
        self.likesModel = dictionary.toNSArrayFrom(key: "likes")
        
        self.commentsModel = dictionary.toNSArrayFrom(key: "comments")
        
    }

}


/*
 
 "data": [
 {
 "__v": 0,
 "reported": "0",
 "userLocation": "Bhawanipur, Kolkata",
 "logTimeStamp": "1491949801794",
 "imageUrl": "/feedpic/2354353654.jpg",
 "message": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur a molestie nisi, vel ullamcorper quam. In in nulla dolor. Pellentesque nulla felis, pellentesque sit amet vestibulum ut, dapibus quis odio.",
 "userName": "perma",
 "postID": "POST1491949801794",
 "createdAt": "2017-04-11T22:30:01.803Z",
 "updatedAt": "2017-04-11T22:30:01.803Z",
 "_id": "58ed58e9e33ede07306cc402",
 "likes": [
 {
 "_id": "58ed5c3c2c0a5f086ab0c461",
 "updatedAt": "2017-04-11T22:44:12.655Z",
 "createdAt": "2017-04-11T22:44:12.655Z",
 "postID": "58ed58e9e33ede07306cc402",
 "userName": "perma",
 "likedOn": "1491950652653",
 "__v": 0
 },
 {
 "_id": "58ed5c432c0a5f086ab0c462",
 "updatedAt": "2017-04-11T22:44:19.607Z",
 "createdAt": "2017-04-11T22:44:19.607Z",
 "postID": "58ed58e9e33ede07306cc402",
 "userName": "pierce",
 "likedOn": "1491950659600",
 "__v": 0
 }
 ],
 "comments": [
 {
 "_id": "58ed59ff09fcdd077502dc98",
 "updatedAt": "2017-04-11T22:34:39.788Z",
 "createdAt": "2017-04-11T22:34:39.788Z",
 "postID": "58ed58e9e33ede07306cc402",
 "userName": "pierce",
 "message": "Sed varius id mi tempor vulputate",
 "logTimeStamp": "1491950079786",
 "__v": 0
 },
 {
 "_id": "58ed5a2009fcdd077502dc9a",
 "updatedAt": "2017-04-11T22:35:12.029Z",
 "createdAt": "2017-04-11T22:35:12.029Z",
 "postID": "58ed58e9e33ede07306cc402",
 "userName": "perry",
 "message": "Sed varius id mi tempor vulputate",
 "logTimeStamp": "1491950112028",
 "__v": 0
 }
 ]
 }
 ]
 }
 
 */
