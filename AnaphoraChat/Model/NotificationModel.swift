//
//  NotificationModel.swift
//  ACCO
//
//  Created by Omnibuz Labs on 01/11/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class NotificationModel: NSObject {
    var id = ""
    var updatedAt = ""
    var createdAt = ""
    var alertID = ""
    var userNameReceiver = ""
    var title = ""
    var message = ""
    var imageUrl = ""
    var related_user = ""
    var related_data = ""
    var epochTimeStampSent = ""
    var epochTimeStampRead = ""
    var readStatus = ""
    var __v = ""
    
    override init() {
        super.init()
    }
    
    convenience init(_ dictionary : NSDictionary) {
        self.init()
        
        
        self.id = dictionary.toStringFrom(key: "_id")
        self.updatedAt = dictionary.toStringFrom(key: "updatedAt")
        self.createdAt = dictionary.toStringFrom(key: "createdAt")
        self.alertID = dictionary.toStringFrom(key: "alertID")
        self.userNameReceiver = dictionary.toStringFrom(key: "userNameReceiver")
        self.title = dictionary.toStringFrom(key: "title")
        self.message = dictionary.toStringFrom(key: "message")
        self.imageUrl = dictionary.toStringFrom(key: "imageUrl")
        self.related_user = dictionary.toStringFrom(key: "related_user")
        self.related_data = dictionary.toStringFrom(key: "related_data")
        self.epochTimeStampSent = dictionary.toStringFrom(key: "epochTimeStampSent")
        self.epochTimeStampRead = dictionary.toStringFrom(key: "epochTimeStampRead")
        self.readStatus = dictionary.toStringFrom(key: "readStatus")
        self.__v = dictionary.toStringFrom(key: "__v")
    }

}
