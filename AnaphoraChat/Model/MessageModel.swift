//
//  MessageModel.swift
//  ACCO
//
//  Created by Omnibuz Labs on 01/12/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class MessageModel: NSObject {
    
    var __v = ""
    var updatedAt = ""
    var createdAt = ""
    var conversationsID = ""
    var conversationGroupID = ""
    var sender = ""
    var message_text = ""
    var message_file = ""
    var message_type = ""
    var epochTimeStamp = ""
    var _id = ""
    var usersConvNotRead = NSArray()
    var usersDeletedConv = NSArray()
    var users = NSArray()
    
    override init() {
        super.init()
    }
    
    convenience init(dictionary: NSDictionary) {
        self.init()
        
        self.__v = dictionary.toStringFrom(key: "__v")
        
        self.updatedAt = dictionary.toStringFrom(key: "updatedAt")
        
        self.createdAt = dictionary.toStringFrom(key: "createdAt")
        
        self.conversationsID = dictionary.toStringFrom(key: "conversationsID")
        
        self.conversationGroupID = dictionary.toStringFrom(key: "conversationGroupID")
        
        self.sender = dictionary.toStringFrom(key: "sender")
        
        self.message_text = dictionary.toStringFrom(key: "message_text")
        
        self.message_file = dictionary.toStringFrom(key: "message_file")
        
        self.epochTimeStamp = dictionary.toStringFrom(key: "epochTimeStamp")
        
        self._id = dictionary.toStringFrom(key: "_id")
        
        self.usersConvNotRead = dictionary.toNSArrayFrom(key: "usersConvNotRead")
        
        self.usersDeletedConv = dictionary.toNSArrayFrom(key: "usersDeletedConv")
        
        self.users = dictionary.toNSArrayFrom(key: "users")
        
        
    }

}
