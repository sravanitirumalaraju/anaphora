//
//  ExploreModel.swift
//  ACCO
//
//  Created by Tanmoy Khanra on 21/07/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class ExploreModel: NSObject {
    
    var userName = ""
    
    var locationName = ""
    
    var userDetails = UserModel()
    
    var userFeed = NSArray()
    
    var connection = Bool()
    
    override init() {
        super.init()
    }
    
    convenience init(_ dictionary: NSDictionary) {
        self.init()
        
        self.userName = dictionary.toStringFrom(key: "userName")
        
        self.locationName = dictionary.toStringFrom(key: "locationName")
        
        self.userDetails = UserModel(dict: dictionary["user_details"] as! NSDictionary)
        
        self.userFeed = dictionary.toNSArrayFrom(key: "user_feed")
        
        self.connection = dictionary.toBoolFrom(key: "connection")
        
    }


}
