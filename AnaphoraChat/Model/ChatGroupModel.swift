//
//  ChatGroupModel.swift
//  ACCO
//
//  Created by Omnibuz Labs on 23/10/17.
//  Copyright © 2017 Omnibuz. All rights reserved.
//

import UIKit

class ChatGroupModel: NSObject {
    var id = ""
    var updatedAt = ""
    var createdAt = ""
    var conversationGroupID = ""
    var title = ""
    var imageURL = ""
    var creatorUserName = ""
    var creatorIP = ""
    var creatorLocation = ""
    var creatorEpochTimeStamp = ""
    var isArchived = Bool()
    var type = ""
    var __v = ""
    var usersDeletedConvGroup = NSArray()
    var usersRemoved = NSArray()
    var users = NSArray()
    var last_message = ""
    var last_message_time = ""
    var user_details = NSDictionary()
    override init() {
        super.init()
    }
    
    convenience init(_ dictionary: NSDictionary) {
        self.init()
        
        self.id = dictionary.toStringFrom(key: "_id")
        
        self.updatedAt = dictionary.toStringFrom(key: "updatedAt")
        
        self.createdAt = dictionary.toStringFrom(key: "createdAt")
        
        self.conversationGroupID = dictionary.toStringFrom(key: "conversationGroupID")
        
        self.title = dictionary.toStringFrom(key: "title")
        
        self.imageURL = dictionary.toStringFrom(key: "imageURL")
        
        self.creatorUserName = dictionary.toStringFrom(key: "creatorUserName")
        
        self.creatorIP = dictionary.toStringFrom(key: "creatorIP")
        
        self.creatorLocation = dictionary.toStringFrom(key: "creatorLocation")
        
        self.creatorEpochTimeStamp = dictionary.toStringFrom(key: "creatorEpochTimeStamp")
        
        self.isArchived = dictionary.toBoolFrom(key: "isArchived")
        
        self.type = dictionary.toStringFrom(key: "type")
        
        self.__v = dictionary.toStringFrom(key: "__v")
        
        self.usersDeletedConvGroup = dictionary.toNSArrayFrom(key: "usersDeletedConvGroup")
        
        self.usersRemoved = dictionary.toNSArrayFrom(key: "usersRemoved")
        
        self.users = dictionary.toNSArrayFrom(key: "users")
        
        self.last_message = dictionary.toStringFrom(key: "last_message")

        self.last_message_time = dictionary.toStringFrom(key: "last_message_time")

        self.user_details = dictionary["user_details"] as! NSDictionary
        
        //self.user_details = dictionary.toNSDictionaryFrom(key: "user_details")
        
        
        
    }

}
