//
//  RegisterModel.swift
//  DNADemo
//
//  Created by subhajit halder on 24/01/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit

class RegisterModel: NSObject {
    
    var userName = ""
    var name = ""
    var profilePic = ""
    var email = ""
    var dob = ""
    var gender = ""
    var phNo = ""
    var age = ""
    var hobbies = ""
    var bio = ""
    var password = ""
    var location = ""
    var latLong = ""
    var timeZone = ""
    var private_mode = ""
}

/*
 userName,
 name,
 profilePic,
 email,
 dob,
 gender,
 phNo,
 age: "Age",
 location,
 lat_lng,
 hobbies:"Hobbies",
 bio:"Bio"
 
 */

