//
//  AppServiceManager.swift
//  DNADemo
//
//  Created by subhajit halder on 18/01/17.
//  Copyright © 2017 SubhajitHalder. All rights reserved.
//

import UIKit

class AppServiceManager: NSObject {

    typealias ServiceCompletionHandler = (_ object: Any?,_ message: String?,_ error: Error?) -> Void
    
    public class func loginWith( username: String!, password: String!, handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/auth/signin"
        //userName,password
        let httpBody = "userName=\(username!)&password=\(password!)"
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.sendParamswithCompletion({(object, error) in
            
            
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                
                if success {
                    
                    let token =  data["apiKey"] as! String
                    
                    let model = UserModel(dict: data["data"] as! NSDictionary)
                    
                    UserDefaults.standard.set(model.userName, forKey: "userName")
                    UserDefaults.standard.set(model.gender, forKey: "userGender")
                    UserDefaults.standard.set(token, forKey: "token")
                    
                    handler(model, message, nil)
                }
                else {
                    handler(nil, message, nil)
                }
                
            }
            else {
                handler(nil, nil, error)
            }
        })
    }
    
    

    //
    public class func registerUser( model: RegisterModel, handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/auth/signup"
        
        let httpBody = "userName=\(model.userName)&name=\(model.name)&profilePic=\(model.profilePic)&email=\(model.email)&dob=\(model.dob)&gender=\(model.gender)&phNo=\(model.phNo)&location=\(model.location)&lat_lng=\(model.latLong)&password=\(model.password)&hobbies=\(model.hobbies)&bio=\(model.bio)"
      print(model.location)
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.sendParamswithCompletion({(object, error) in
            
            
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success {
                    
                    let token =  data["apiKey"] as! String
                    
                    let model = UserModel(dict: data["data"] as! NSDictionary)
                    
                    UserDefaults.standard.set(model.userName, forKey: "userName")
                    UserDefaults.standard.set(model.gender, forKey: "userGender")
                    UserDefaults.standard.set(token, forKey: "token")


                    handler(model, message, nil)
                }
                else {
                    handler(nil, message, nil)
                }
                
            }
            else {
                handler(nil, nil, error)
            }
        })
    }
    
    
    
    public class func saveImage(withImage image: UIImage = UIImage(), type: String, handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/file/\(type)"
        
        let params = [ "picType": type]
        let data = UIImageJPEGRepresentation(image, 0.75)
        
        let restClient = SHRestClient(withUrl: url, Body: "", Method: "POST", Headers: nil)
        
        restClient.saveData(withParams: data!, withFileName: "myImage.jpg", fileKey: "", otherData: params as NSDictionary, withCompletion: {(object, error) in
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success {
//                    let dict = data["data"] as! NSDictionary
                    handler(data["data"], message, nil)
                }
                else {
                    handler(nil, message, nil)
                }
                
            }
            else {
                handler(nil, nil, error)
            }

        })
        
    }
    
    public class func getUserDetails(username: String, handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/profile/details/\(username)"
        let httpBody = ""
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "GET", Headers: nil)
        restClient.sendParamswithCompletion({(object, error) in
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                print(data)
                if success  {
                    if data["data"] is NSDictionary {
                        let model = UserModel(dict: data["data"] as! NSDictionary)
                        handler(model, message, nil)
                    }
                }
                else {
                    handler(nil, message, nil)
                }
            }
            else {
                handler(nil, nil, error)
            }
        })

    }
    
    //MARK: UPDATE USER DETAILS
    
    public class func updateUser( model: RegisterModel, handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/profile/update"
        
        let httpBody = "name=\(model.name)&profilePic=\(model.profilePic)&email=\(model.email)&dob=\(model.dob)&gender=\(model.gender)&phNo=\(model.phNo)&location=\(model.location)&lat_lng=\(model.latLong)&hobbies=\(model.hobbies)&bio=\(model.bio)&private_mode=\(model.private_mode)&timeZone=\(TimeZone.current.abbreviation()!)"
       print(model.location)
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "PUT", Headers: nil)
        restClient.sendParamswithCompletion({(object, error) in
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                if success {
                    let model = UserModel(dict: data["data"] as! NSDictionary)
                    handler(model, message, nil)
                }
                else {
                    handler(nil, message, nil)
                }
            }
            else {
                handler(nil, nil, error)
            }
        })
    }
    
    
    //MARK: Connection Feed
    
    
    public class func getConnectionFeed( time: String, handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/feed/connections/\(time)"
        
        let httpBody = ""
        
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "GET", Headers: nil)
        
        restClient.sendParamswithCompletion({(object, error) in
            
            
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success {
                    let list = NSMutableArray()
                    
                    let tempData  =  data["data"] as? NSArray
                    
                    
                    tempData?.enumerateObjects(using: { (obj, index, stop) in
                        
                        let userFeedList = UserFeedModel(obj as! NSDictionary)
                        
                        list.add(userFeedList)
                        
                        
                    })
                    
                    handler(list, message, nil)

                }
                else {
                    handler(nil, message, nil)
                }
                
            }
            else {
                handler(nil, nil, error)
            }
        })
    }
    
    
    
    //MARK: Feed Post 
    
    public class func addFeed( model: FeedPostModel, handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/post/add"
        
        let httpBody = "message=\(model.message)&imageUrl=\(model.imageURL)&userLocation=\(model.location)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

        
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.sendParamswithCompletion({(object, error) in
            
            
            if error == nil {
                
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success {
                    
                    let model = FeedPostCallBackModel(data["data"] as! NSDictionary)
                    
                    handler(model, message, nil)
                    
                }
                else {
                    
                    handler(nil, message, nil)
                }
                
            }
            else {
                
                handler(nil, nil, error)
            }
        })
    }
    
    
    //MARK: Own Feed
    
    
    public class func getOwnFeed(userName: String, time: String, handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/feed/my/\(userName)/\(time)"
        print("\(url)")
        let httpBody = ""
        
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "GET", Headers: nil)
        
        restClient.sendParamswithCompletion({(object, error) in
            
            
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success {
                    let list = NSMutableArray()
                    
                    let tempData  =  data["data"] as? NSArray
                    
                    
                    tempData?.enumerateObjects(using: { (obj, index, stop) in
                        
                        let userFeedList = UserFeedModel(obj as! NSDictionary)
                        
                        list.add(userFeedList)
                        
                        
                    })
                    
                    handler(list, message, nil)
                    
                }
                else {
                    handler(nil, message, nil)
                }
                
            }
            else {
                handler(nil, nil, error)
            }
        })
    }
    
    //MARK: Comment On A Post
    
    public class func commentOn_Post( postID: String, message:String, handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/post/comment/add/"
        
        let httpBody = "message=\(message)&postID=\(postID)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.sendParamswithCompletion({(object, error) in
            
            
            if error == nil {
                
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success {
                    
                    
                    handler(data["data"], message, nil)
                    
                }
                else {
                    
                    handler(nil, message, nil)
                }
                
            }
            else {
                
                handler(nil, nil, error)
            }
        })
    }
    

    //MARK: like unlike 
    
    public class func like_Post( postID: String, handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/post/like"
        
        let httpBody = "postID=\(postID)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.sendParamswithCompletion({(object, error) in
            
            
            if error == nil {
                
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success {
                    
                    
                    handler(data["data"], message, nil)
                    
                }
                else {
                    
                    handler(nil, message, nil)
                }
                
            }
            else {
                
                handler(nil, nil, error)
            }
        })
    }
    
    //MARK: Reset Password
    
    public class func resetPassword( username: String!, password: String!, handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/auth/reset"
        //userName,password
        let httpBody = "userName=\(username!)&password=\(password!)"
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "PUT", Headers: nil)
        
        restClient.sendParamswithCompletion({(object, error) in
            
            
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                
                if success {
                    
                 
                    
                    handler(data["data"], message, nil)
                }
                else {
                    handler(nil, message, nil)
                }
                
            }
            else {
                handler(nil, nil, error)
            }
        })
    }
    
    //MARK: Serach User name

    public class func searchUname( email: String,  handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/auth/email/\(email)"
       
        let httpBody = ""
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "GET", Headers: nil)
        
        restClient.sendParamswithCompletion({(object, error) in
            
            
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                
                if success {
                    
                    handler(data["data"], message, nil)
                }
                else {
                    handler(nil, message, nil)
                }
                
            }
            else {
                handler(nil, nil, error)
            }
        })
    }

   //MARK: Log User 
    
    public class func logUser( lat: String, lng: String , handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/history/location/log/"
        
        let httpBody = "lat=\(lat)&lng=\(lng)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.sendParamswithCompletion({(object, error) in
            
            
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                
                if success {
                    
                    handler(data["data"], message, nil)
                }
                else {
                    handler(nil, message, nil)
                }
                
            }
            else {
                handler(nil, nil, error)
            }
        })
    }

    //MARK: Explore
    
    public class func getExploreFeed( lat: String, lng: String, distance:String, offset:String, limit: String,  handler: @escaping ServiceCompletionHandler) {
        
        let url = "\(kBaseUrl)/api/explore/\(lat)/\(lng)/\(distance)/\(offset)/\(limit)"
        
        let httpBody = ""
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "GET", Headers: nil)
        
        restClient.sendParamswithCompletion({(object, error) in
            
            
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                
                if success {
                    
                    let list = NSMutableArray()
                    
                    let tempData  =  data["data"] as? NSArray
                    
                    
                    tempData?.enumerateObjects(using: { (obj, index, stop) in
                        
                        let exploreModel = ExploreModel(obj as! NSDictionary)
                        
                        list.add(exploreModel)
                        
                        
                    })
                    
                    handler(list, message, nil)
                }
                else {
                    
                    handler(nil, message, nil)
                    
                    
                }
                
            }
            else {
                handler(nil, nil, error)
            }
        })
    }

    
    //MARK: Follow connection
    
    public class func follow_unfollow( username: String,  handler: @escaping ServiceCompletionHandler) {
        let url = "\(kBaseUrl)/api/connection/send"
        let httpBody = "userNameInvited=\(username)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)        
        restClient.sendParamswithCompletion({(object, error) in
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                print(success)
                let message = data["message"] as! String
                if success {
                    let model = FollowModel(data["data"] as! NSDictionary)
                    handler(model, message, nil)
                }
                else {
                    handler(nil, message, nil)
                }
            }
            else {
                handler(nil, nil, error)
            }
        })
    }
    //MARK: GET CHAT GROUP
    public class func chatGroup(handler: @escaping ServiceCompletionHandler){
        let url = "\(kBaseUrl)/api/chat"
        
        let httpBody = ""
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "GET", Headers: nil)
        
        restClient.getJsonData({(object, error) in
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success{
                    let list = NSMutableArray()
                    
                    let tempData = data["data"] as? NSArray
                    
                    tempData?.enumerateObjects(using: { (obj, index, stop) in
                        
                        let chatGroupModel = ChatGroupModel(obj as! NSDictionary)
                        
                        if chatGroupModel.type == "p2p" {
                            
                            list.add(chatGroupModel)
                        }                        
                    })
                    handler(list, message, nil)
                }else{
                    
                    handler(nil, message, nil)
                    
                }
            }else{
                handler(nil,nil,error)
            }
        })
        
    }
    
    //MARK: Create a p2p Chat (chat from profile section)
    public class func createP2PChat(type: String, userName: String, handler: @escaping ServiceCompletionHandler){
        
        let url = "\(kBaseUrl)/api/chat/create"
        
        let httpBody = "type=\(type)&userName=\(userName)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.getJsonData({(object, error) in
            if error == nil{
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success {
                    
                    handler(data["data"], message, nil)
                    
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, error)
            }
        })
        
    }
    
    
    //MARK: Create a Random group Chat(group / anon [2 person] / p2p [2 person])
    public class func createRandomChat(type: String, user_gender: String, chatroom_opt_gender: String, lat: String, lng: String, handler: @escaping ServiceCompletionHandler){
        
        let url = "\(kBaseUrl)/api/chat/random"
        
        let httpBody = "type=\(type)&user_gender=\(user_gender)&chatroom_opt_gender=\(chatroom_opt_gender)&lat=\(lat)&lng=\(lng)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.getJsonData({(object, error) in
            if error == nil{
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success {
                    handler(data["data"], message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, error)
            }
        })
        
    }
    
    
    //MARK: Find In Chat
    public class func findChat(userName: String, handler: @escaping ServiceCompletionHandler){
        
        let url = "\(kBaseUrl)/api/feed/search"
        
        let httpBody = "search_text=\(userName)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.getJsonData({(object, error) in
            if error == nil{
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success {

                    handler(data["data"] , message, nil)
                    
                }else{
                    
                    handler(nil, message, nil)
                    
                }
            }else{
                
                handler(nil,nil,error)
                
            }
        })
    }
    
    //MARK: Send Message
    public class func sendMessage(conversationGroupID: String, message_text: String ,message_file: String, message_type: String, handler: @escaping ServiceCompletionHandler){
        
        let url = "\(kBaseUrl)/api/chat/message/send"
        
        let httpBody = "conversationGroupID=\(conversationGroupID)&message_text=\(message_text)&message_file=\(message_file)&message_type=\(message_type)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.getJsonData({(object, error) in
            
            if error == nil {
                
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success {
                    
                    handler(data["data"], message, nil)
                
                }else{
                    
                    handler(nil, message, nil)
                
                }
            }else{
               
                handler(nil, nil, error)
            
            }
        })
    }
    
    //MARK: Get All Message
    public class func allMessages(conversationGroupID: String, fromTime: String, handler: @escaping ServiceCompletionHandler){
        
        let url = "\(kBaseUrl)/api/chat/message"
        
        let httpBody = "conversationGroupID=\(conversationGroupID)&fromTime=\(fromTime)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.getJsonData({(object, error) in
            if error == nil{
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success{
                    
                    let list = NSMutableArray()
                    
                    let tempData = data["data"] as! NSArray
                    
                    tempData.enumerateObjects(using: {(object, index, stop) in
                        
                        let messageModel = MessageModel(dictionary: object as! NSDictionary)
                        
                        list.add(messageModel)
                    })
                    handler(list, message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, error)
            }
            
        })
    }
    
    //MARK: Delet Chat Group
    public class func deleteChateGroup(id: String, handler: @escaping ServiceCompletionHandler){
        
        let url = "\(kBaseUrl)/api/chat/delete"
        
        let htttpBody = "_id=\(id)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let restClient = SHRestClient(withUrl: url, Body: htttpBody, Method: "POST", Headers: nil)
        
        restClient.getJsonData({(object, error) in
            if error == nil{
                let data = object?["response"] as! NSDictionary
                let message = data["message"] as! String
                let success = data["success"] as! Bool
                if success {
                    handler(data["data"], message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, error)
            }
        })
        
    }
    
    //MARK: Delete Message
    public class func deleteMessage(id: String, handler: @escaping ServiceCompletionHandler){
        
        let url = "\(kBaseUrl)/api/chat/message/delete"
        
        let httpBody = "_id=\(id)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let restClicen = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClicen.getJsonData({(object, error) in
        
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let message = data["message"] as! String
                let success = data["success"] as! Bool
                if success {
                    handler(data["data"], message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, error)
            }
        })
    }
    
    //MARK: Block User
    public class func blockUser(blockUserName: String, handler: @escaping ServiceCompletionHandler){
        let url = "\(kBaseUrl)/api/connection/block"
        
        let httpBody = "blockUserName=\(blockUserName)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.getJsonData({(object, error) in
            if error == nil{
                let data = object?["response"] as! NSDictionary
                let message = data["message"] as! String
                let success = data["success"] as! Bool
                
                if success {
                    handler(data["data"], message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, error)
            }
        })
    }
    
    //MARK: Leave From Chat
    
    
    //MARK: Report Post
    public class func reportPost(postID: String, handler: @escaping ServiceCompletionHandler){
        let url = "\(kBaseUrl)/api/post/report/"
        
        let httpBody = "post_id=\(postID)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        restClient.getJsonData({(object, error) in
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                if success {
                    handler(data["data"], message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil,nil,error)
            }
        })
    }
    
    //MARK: Delete Post
    public class func deletePost(postID: String, handler: @escaping ServiceCompletionHandler){
        let url = "\(kBaseUrl)/api/post/delete/"
        
        let httpBody = "post_id=\(postID)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        restClient.getJsonData({(object,error) in
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                if success{
                    handler(data["data"],message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil,nil,error)
            }
        })
    }
    
    //MARK: Notification
    
    public class func notificationFetch(handler: @escaping ServiceCompletionHandler){
    
        let url = "\(kBaseUrl)/api/notifications"
        let httpBody = ""
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "GET", Headers: nil)
        
        restClient.getJsonData({(object, error) in
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                if success {
                    let list = NSMutableArray()
                    let tempData = data["data"] as! NSArray
                    
                    tempData.enumerateObjects(using: {(object,index,stop) in
                        let notificationModel = NotificationModel(object as! NSDictionary)
                        
                        list.add(notificationModel)
                    })
                    
                    handler(list, message, nil)
                    
                }else{
                    handler(nil,message,nil)
                }
            }else{
                handler(nil,nil,error)
            }
            
        })
    }
    
    //MARK: User's Follower
    public class func fetchFollower(handler: @escaping ServiceCompletionHandler){
        let url = "\(kBaseUrl)/api/connection/followers"
        let httpBody = ""
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "GET", Headers: nil)
        restClient.getJsonData({(object, error) in
            if error == nil{
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                if success {
                    let list = NSMutableArray()
                    let tempData = data["data"] as! NSArray
                    print(tempData.count)
                    print(tempData)
                    tempData.enumerateObjects(using: {(object,index,stop) in
                        let notificationModel = FollowModel(object as! NSDictionary)
                        list.add(notificationModel)
                    })
                    print(list.count)
                    handler(list, message, nil)
                }else{
                    handler(nil,message,nil)
                }
            }else{
                handler(nil,nil,error)
            }
        })
    }
    
    //MARK: User Following
    public class func fetchFollowing(handler: @escaping ServiceCompletionHandler){
        
        let url = "\(kBaseUrl)/api/connection/following"
        
        let httpBody = ""
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "GET", Headers: nil)
        
        restClient.getJsonData({(object, error) in
            
            if error == nil{
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success {
                    
                    let list = NSMutableArray()
                    let tempData = data["data"] as! NSArray
                    print(tempData.count)
                    print(tempData)
                    tempData.enumerateObjects(using: {(object,index,stop) in
                        let notificationModel = FollowModel(object as! NSDictionary)
                        
                        list.add(notificationModel)
                    })
                    
                    handler(list, message, nil)
                }else{
                    handler(nil,message,nil)
                }
            }else{
                handler(nil,nil,error)
            }
        })
    }
    
    
    //MARK: User Recommendation
    
    public class func fetchUserRecommendation(userName: String,handler: @escaping ServiceCompletionHandler){
        let url = "\(kBaseUrl)/api/recommend/\(userName)"
        
        let httpBody = ""
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "GET", Headers: nil)
        restClient.getJsonData({(object, error) in
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success{
                    let list = NSMutableArray()
                    
                    let tempData = data["data"] as! NSArray
                    
                    tempData.enumerateObjects(using: {(object, index, stop) in
                        let recommendModels = RecommendModel(object as! NSDictionary)
                        
                        list.add(recommendModels)
                        
                    })
                    handler(list, message, nil)
                    
                }else{
                    handler(nil, message, nil)
                }
                
            }else{
                handler(nil, nil, error)
            }
        })
    }
    
    //MARK: My Recommendation
    public class func fetchMyRecommendation(userName: String,handler: @escaping ServiceCompletionHandler){
        let url = "\(kBaseUrl)/api/recommend/my"
        
        let httpBody = "userName=\(userName)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.getJsonData({(object, error) in
            if error == nil {
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                
                if success{
                    let list = NSMutableArray()
                    
                    let tempData = data["data"] as! NSArray
                    
                    tempData.enumerateObjects(using: {(object, index, stop) in
                        let recommendModels = RecommendModel(object as! NSDictionary)
                        
                        list.add(recommendModels)
                        
                    })
                    handler(list, message, nil)
                    
                }else{
                    handler(nil, message, nil)
                }
                
            }else{
                handler(nil, nil, error)
            }
        })
    }
    
    //MARK: Recommend Unrecommend
    
    public class func recommendUnRecommend(userName: String, handler: @escaping ServiceCompletionHandler){
        
        let url = "\(kBaseUrl)/api/recommend"
        
        let httpBody = "userName=\(userName)"
        
        let restClient = SHRestClient(withUrl: url, Body: httpBody, Method: "POST", Headers: nil)
        
        restClient.getJsonData({(object, error) in
            if error == nil{
                let data = object?["response"] as! NSDictionary
                let success = data["success"] as! Bool
                let message = data["message"] as! String
                if success {
                    handler(data["data"], message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, error)
            }
        })
    }

}
